#include <algorithm>
#include <cstdio>
#include <fstream>
#include <string>
#include <vector>

#if !_WIN64
#define x86
#endif

namespace {
	void part1(int* program) {
		int i = 0;
		while (program[i] != 99) {
			const int operand1 = program[program[i + 1]];
			const int operand2 = program[program[i + 2]];
			const int outputPos = program[i + 3];
			if (program[i] == 1)
				program[outputPos] = operand1 + operand2;
			else
				program[outputPos] = operand1 * operand2;
			i += 4;
		}
	}

#ifdef x86
	void part1Asm(int* program) {
		__asm {
			mov ebx, [program]; // Start of program pointer.
			mov ecx, 0; // Program counter
		loopstart:
			mov eax, [ebx + ecx + 4]; // Load operands
			mov eax, [ebx + eax * 4];
			mov edx, [ebx + ecx + 8];
			mov edx, [ebx + edx * 4];
			mov edi, [ebx + ecx + 12]; // Load output location
			cmp [ebx + ecx], 1; // Check operator
			jne opmul;
			add eax, edx; // Do the add.
			jmp advance;
		opmul:
			mul edx; // Do the mul.
		advance:
			mov [ebx + edi * 4], eax; // Write to output.
			add ecx, 16;
			cmp[ebx + ecx], 99; // Check for done.
			jne loopstart;
		}
	}
#endif
}

namespace y2019 {
	void day02() {
		// Read input.
		std::ifstream ss{ "2019/2019_day02_input.txt" };
		std::vector<int> program;

		std::string line;
		while (!ss.eof()) {
			getline(ss, line, ',');
			program.push_back(std::atoi(line.c_str()));
		}

		program[1] = 12;
		program[2] = 2;

		auto program_copy = program;
		part1(program_copy.data());
		printf("expected answer = %d\n", program_copy[0]);

		program_copy = program;
#ifdef x86
		part1Asm(program_copy.data());
		printf("asm answer = %d\n", program_copy[0]);
#endif
	}
}
