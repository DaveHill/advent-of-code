#include <algorithm>
#include <cstdio>
#include <fstream>
#include <string>
#include <vector>

#if !_WIN64
#define x86
#endif

namespace {
	int calculateFuel(const int* mass, int n) {
		int acc = 0;
		for (int i = 0; i < n; ++i)
			acc += mass[i] / 3 - 2;
		return acc;
	}

#ifdef x86
	int calculateFuelAsm(const int* mass, int n) {
		__asm {
			mov ecx, n; // Initialize counter (countdown)
			mov esi, DWORD PTR[mass]; // Data pointer
			mov edi, 3; // Load the 3 in the fuel equation fuel = mass / 3 - 2
			mov ebx, 0; // Initialize accumulator
		loopstart:
			mov eax, [esi]; // Load input data to eax
			add esi, 4; // Increment data pointer
			xor edx, edx; // Clear high-part edx to be used in division
			div edi; // Divide by 3
			add eax, -2; // Subtract 2
			add ebx, eax; // Accumulate result
			dec ecx; // Decrement loop counter
			jnz loopstart; // Loop
			mov eax, ebx; // Move accumulator to output
		}
	}
#endif

	int calculateFuelPart2(const int* mass, int n) {
		int acc = 0;
		for (int i = 0; i < n; ++i) {
			int fuel = mass[i] / 3 - 2;
			while (fuel > 0) {
				acc += fuel;
				fuel = fuel / 3 - 2;
			}
		}
		return acc;
	}

#ifdef x86
	int calculateFuelPart2Asm(const int* mass, int n) {
		__asm {
			mov ecx, n; // Initialize counter
			mov esi, DWORD PTR[mass]; // Data pointer
			mov edi, 3;
			mov ebx, 0; // Accumulator.
		outerloop:
			mov eax, [esi]; // Load input value
			add esi, 4; // Increment data pointer
			xor edx, edx; // Clear high part for division
			div edi;
			add eax, -2;
		innerloop:
			add ebx, eax; // Accumulate
			xor edx, edx;
			div edi;
			add eax, -2;
			test eax, eax;
			jg innerloop; // Loop while fuel > 0
			dec ecx;
			jnz outerloop; // Loop for all data
			mov eax, ebx; // Move accumulator to output
		}
	}
#endif

	void part1(const int* mass, int n) {
#ifdef x86
		printf("part 1: fuel = %d\n", calculateFuelAsm(mass, n));
#endif
		printf("part 1: expected fuel = %d\n", calculateFuel(mass, n));
	}

	void part2(const int* mass, int n) {
#ifdef x86
		printf("part 2: fuel = %d\n", calculateFuelPart2Asm(mass, n));
#endif
		printf("part 2: expected fuel = %d\n", calculateFuelPart2(mass, n));
	}
}

namespace y2019 {
	void day01() {
		// Read input.
		std::ifstream ss{ "2019/2019_day01_input.txt" };
		std::vector<int> mass_list;

		std::string line;
		while (!ss.eof()) {
			getline(ss, line);
			mass_list.push_back(std::atoi(line.c_str()));
		}

		part1(mass_list.data(), mass_list.size());
		part2(mass_list.data(), mass_list.size());
	}
}
