#include <vector>
#include <string>
#include <iostream>

using namespace std;

namespace {
	auto dirs = "L1,L3,L5,L3,R1,L4,L5,R1,R3,L5,R1,L3,L2,L3,R2,R2,L3,L3,R1,L2,R1,L3,L2,R4,R2,L5,R4,L5,R4,L2,R3,L2,R4,R1,L5,L4,R1,L2,R3,R1,R2,L4,R1,L2,R3,L2,L3,R5,L192,R4,L5,R4,L1,R4,L4,R2,L5,R45,L2,L5,R4,R5,L3,R5,R77,R2,R5,L5,R1,R4,L4,L4,R2,L4,L1,R191,R1,L1,L2,L2,L4,L3,R1,L3,R1,R5,R3,L1,L4,L2,L3,L1,L1,R5,L4,R1,L3,R1,L2,R1,R4,R5,L4,L2,R4,R5,L1,L2,R3,L4,R2,R2,R3,L2,L3,L5,R3,R1,L4,L3,R4,R2,R2,R2,R1,L4,R4,R1,R2,R1,L2,L2,R4,L1,L2,R3,L3,L5,L4,R4,L3,L1,L5,L3,L5,R5,L5,L4,L2,R1,L2,L4,L2,L4,L1,R4,R4,R5,R1,L4,R2,L4,L2,L4,R2,L4,L1,L2,R1,R4,R3,R2,R2,R5,L1,L2"s;

	enum class Heading { N, E, S, W };

	Heading turn(Heading current, bool turnLeft)
	{
		switch (current)
		{
		case Heading::N: return turnLeft ? Heading::W : Heading::E;
		case Heading::S: return turnLeft ? Heading::E : Heading::W;
		case Heading::E: return turnLeft ? Heading::N : Heading::S;
		case Heading::W: return turnLeft ? Heading::S : Heading::N;
		default: std::terminate();
		}
	}

	std::vector<std::string> split(const std::string& str, const std::string& delim) {
		size_t start = 0;
		size_t end = str.find(delim);
		std::vector<std::string> result;
		while (end != std::string::npos) {
			result.push_back(str.substr(start, end - start));
			start = end + delim.length();
			end = str.find(delim, start);
		}
		result.push_back(str.substr(start, end));
		return result;
	}
}

namespace y2016 {
	void day01()
	{
		vector<string> instructions = split(dirs, ",");

		Heading heading = Heading::N;

		struct Coord { int x = 0; int y = 0; };
		Coord location;

		vector<Coord> visitedLocations;
		bool completedSecondPart = false;

		for (auto s : instructions)
		{
			// Parse direction and distance.
			bool turnLeft = s[0] == 'L';
			int amount = atoi(string(s.begin() + 1, s.end()).c_str());

			// Turn.
			heading = turn(heading, turnLeft);

			// Move.
			for (int i = 0; i < amount; ++i)
			{
				switch (heading)
				{
				case Heading::N: location.y += 1; break;
				case Heading::S: location.y -= 1; break;
				case Heading::E: location.x += 1; break;
				case Heading::W: location.x -= 1; break;
				}

				if (!completedSecondPart)
				{
					const auto prev = std::find_if(visitedLocations.begin(), visitedLocations.end(), [&](const auto& rhs) { return location.x == rhs.x && location.y == rhs.y; });
					if (prev != visitedLocations.end())
					{
						cout << "Already visited " << prev->x << ", " << prev->y << " (distance = " << std::abs(prev->x) + std::abs(prev->y) << ")" << endl;
						completedSecondPart = true;
					}

					visitedLocations.push_back(location);
				}
			}
		}

		// Manhattan distance = |x| + |y|
		cout << "Final destination distance = " << std::abs(location.x) + std::abs(location.y) << endl;
	}
}