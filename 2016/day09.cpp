#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <cstdio>
#include <cassert>

using namespace std;

namespace
{
    uint64_t length(const string& s, bool part2)
    {
        size_t openParen = 0;
        uint64_t decodedLength = 0;
        bool inParens = false;

        for (size_t i = 0; i < s.length(); ++i)
        {
            if (s[i] == '(')
            {
                openParen = i;
                inParens = true;
            }
            else if (inParens && s[i] == ')')
            {
                int numChars, numRepeats;
                sscanf_s(s.substr(openParen, i - openParen).c_str(), "(%dx%d)", &numChars, &numRepeats);

                // Add in the length of the repeated chars, and skip forward.
                if (!part2)
                {
                    decodedLength += numChars * numRepeats;
                }
                else
                {
                    decodedLength += numRepeats * length(s.substr(i + 1, numChars), true);
                }

                i += numChars;

                inParens = false;
            }
            else if (!inParens)
            {
                ++decodedLength;
            }
        }

        return decodedLength;
    }
}

namespace y2016 {
	void day09()
	{
		ifstream fs{ "2016/day09_input.txt" };
		string s((istreambuf_iterator<char>(fs)), istreambuf_iterator<char>());

		// Remove newlines.
		s.erase(remove(begin(s), end(s), '\n'), end(s));

		cout << length(s, false) << endl;
		cout << length(s, true) << endl;
	}
}