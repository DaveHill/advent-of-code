#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

void part(bool part2)
{
    ifstream ss{ "2016/day06_input.txt" };

    vector<map<char, int>> charCounts(8);

    for (string line; getline(ss, line); )
        for (int i = 0; i < 8; ++i)
            ++charCounts[i][line[i]];

    const auto& comparitor = [](const auto& lhs, const auto& rhs) { return lhs.second < rhs.second; };

    for (int i = 0; i < 8; ++i)
        if (!part2)
            cout << max_element(begin(charCounts[i]), end(charCounts[i]), comparitor)->first;
        else
            cout << min_element(begin(charCounts[i]), end(charCounts[i]), comparitor)->first;

    cout << endl;
}

namespace y2016 {
	void day06()
	{
		part(false);
		part(true);
	}
}