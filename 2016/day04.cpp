#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

namespace y2016 {
	void day04()
	{
		ifstream f{ "2016/day04_input.txt" };

		int totalSectorIds = 0;

		for (string line; getline(f, line); )
		{
			auto lastHyphen = line.find_last_of('-');
			auto openBracket = line.find_first_of('[');

			vector<pair<char, int>> charCounts;
			for (auto it = line.begin(); it != line.begin() + lastHyphen; ++it)
			{
				if (*it != '-')
				{
					auto countIt = find_if(begin(charCounts), end(charCounts), [&it](const auto& p) { return p.first == *it; });
					if (countIt != end(charCounts))
						countIt->second++;
					else
						charCounts.emplace_back(*it, 1);
				}
			}

			sort(begin(charCounts), end(charCounts), [](const auto& lhs, const auto& rhs)
				{
					// Sort by descending count, then ascending letter.
					return lhs.second == rhs.second ? lhs.first < rhs.first : lhs.second > rhs.second;
				});

			string checksum;
			checksum.reserve(5);
			transform(begin(charCounts), begin(charCounts) + 5, back_inserter(checksum), [](const auto& p) { return p.first; });

			int sectorId = atoi(string{ line.begin() + lastHyphen + 1, line.begin() + openBracket }.c_str());

			if (checksum == string{ line.begin() + openBracket + 1, line.end() - 1 })
				totalSectorIds += sectorId;

			string decrypted;
			transform(begin(line), begin(line) + lastHyphen, back_inserter(decrypted), [sectorId](const char c)
				{
					return c == '-' ? ' ' : (char)(((int)(c - 'a') + sectorId) % 26 + 'a');
				});

			if (decrypted.find("north") != string::npos)
				cout << sectorId << ":\t" << decrypted << endl;
		}

		cout << totalSectorIds << endl;
	}
}