#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

namespace
{
    void part1()
    {
        ifstream ss{ "2016/day03_input.txt" };
        int numValid = 0;
        for (string line; getline(ss, line); )
        {
            const int a = atoi(string{ line.begin(), line.begin() + 5 }.c_str());
            const int b = atoi(string{ line.begin() + 5, line.begin() + 10 }.c_str());
            const int c = atoi(string{ line.begin() + 10, line.end() }.c_str());

            const int maxSide = std::max(a, std::max(b, c));
            const int sum = a + b + c;
            if (maxSide * 2 < sum)
                numValid++;
        }

        cout << numValid << endl;
    }

    void part2()
    {
        ifstream ss{ "2016/day03_input.txt" };
        int numValid = 0;

        while (!ss.eof())
        {
            vector<string> lines(3);
            getline(ss, lines[0]);
            getline(ss, lines[1]);
            getline(ss, lines[2]);

            for (int offset = 0; offset < 15; offset += 5)
            {
                const int a = atoi(string{ lines[0].begin() + offset, lines[0].begin() + offset + 5 }.c_str());
                const int b = atoi(string{ lines[1].begin() + offset, lines[1].begin() + offset + 5 }.c_str());
                const int c = atoi(string{ lines[2].begin() + offset, lines[2].begin() + offset + 5 }.c_str());

                const int maxSide = std::max(a, std::max(b, c));
                const int sum = a + b + c;
                if (maxSide * 2 < sum)
                    numValid++;
            }
        }

        cout << numValid << endl;
    }
}

namespace y2016 {
	void day03()
	{
		part1();
		part2();
	}
}