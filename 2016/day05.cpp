#include <iostream>
#include <string>
#include <algorithm>

#include "../hash-library/md5.h"

using namespace std;

namespace
{
    void part1()
    {
        const string doorId{ "abbhdwsy" };

        string password;

        for (uint64_t i = 0; password.length() < 8; ++i)
        {
            string testString = doorId + to_string(i);
            MD5 md5;
            md5.add(testString.c_str(), testString.length());

            const auto hash = md5.getHash();
            if (hash.find_first_not_of('0') >= 5)
            {
                password.insert(password.end(), hash[5]);
            }
        }

        cout << password << endl;
    }

    void part2()
    {
        const string doorId{ "abbhdwsy" };

        string password = "xxxxxxxx";

        for (uint64_t i = 0; ; ++i)
        {
            string testString = doorId + to_string(i);
            MD5 md5;
            md5.add(testString.c_str(), testString.length());

            const auto hash = md5.getHash();
            if (hash.find_first_not_of('0') >= 5 && hash[5] >= '0' && hash[5] < '8' && password[hash[5] - '0'] == 'x')
            {
                password[hash[5] - '0'] = hash[6];
                cout << password << endl;

                if (password.find('x') == string::npos)
                {
                    break;
                }
            }
        }
    }
}

namespace y2016 {
	void day05()
	{
		part1();
		part2();
	}
}