#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

namespace
{
    int w = 50;
    int h = 6;

    int coord(int x, int y)
    {
        return y * w + x;
    }
}

namespace y2016 {
	void day08()
	{
		ifstream fs{ "2016/day08_input.txt" };

		vector<char> screen(w * h);
		fill(begin(screen), end(screen), ' ');

		for (string line; getline(fs, line); )
		{
			if (line.find("rect") == 0)
			{
				int rectX, rectY;
				sscanf_s(line.data(), "rect %dx%d", &rectX, &rectY);

				for (int x = 0; x < rectX; ++x)
				{
					for (int y = 0; y < rectY; ++y)
					{
						screen[coord(x, y)] = '#';
					}
				}
			}

			if (line.find("rotate row") == 0)
			{
				int row, amount;
				sscanf_s(line.data(), "rotate row y=%d by %d", &row, &amount);
				rotate(
					next(begin(screen), row * w),
					next(begin(screen), row * w + w - amount),
					next(begin(screen), (row + 1) * w)
				);
			}

			if (line.find("rotate column") == 0)
			{
				int col, amount;
				sscanf_s(line.data(), "rotate column x=%d by %d", &col, &amount);

				for (int i = 0; i < amount % h; ++i)
				{
					// Rotate one step.
					char bottom = screen[coord(col, h - 1)];
					for (int y = h - 1; y >= 1; --y)
					{
						screen[coord(col, y)] = screen[coord(col, y - 1)];
					}
					screen[coord(col, 0)] = bottom;
				}
			}
		}

		cout << count(begin(screen), end(screen), '#') << endl;

		for (int y = 0; y < h; ++y)
		{
			cout << string{ next(begin(screen), y * w), next(begin(screen), (y + 1) * w) } << endl;
		}
	}
}