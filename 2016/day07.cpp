#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

namespace
{
    bool abba(const string& s)
    {
        int state = 0;
        char openingChar = '*';
        char lastChar = '*';
        for (const char c : s)
        {
            switch (state)
            {
            case 0:
                state = 1;
                break;
            case 1:
                if (lastChar != c)
                {
                    state = 2;
                    openingChar = lastChar;
                }
                break;
            case 2:
                if (lastChar == c)
                {
                    state = 3;
                }
                else
                {
                    state = 2;
                    openingChar = lastChar;
                }
                break;
            case 3:
                if (c != openingChar)
                {
                    if (lastChar != c)
                    {
                        state = 2;
                        openingChar = lastChar;
                    }
                    else
                    {
                        state = 1;
                    }
                    openingChar = lastChar;
                }
                else
                {
                    return true;
                }
            };

            lastChar = c;
        }

        return false;
    }

    bool tls(const std::string& line)
    {
        bool looksGood = false;
        bool insideBrackets = false;
        size_t beginSearch = 0;
        size_t endSearch = 0;

        while (beginSearch != string::npos)
        {
            endSearch = line.find_first_of(insideBrackets ? ']' : '[', beginSearch);
            if (abba({ line.begin() + beginSearch, endSearch == string::npos ? line.end() : line.begin() + endSearch }))
            {
                if (!insideBrackets)
                    looksGood = true;
                else
                    return false;
            }

            insideBrackets = !insideBrackets;
            beginSearch = endSearch;
        }

        return looksGood;
    }

    void part1()
    {
        ifstream fs{ "2016/day07_input.txt" };

        int numGood = 0;
        for (string line; getline(fs, line); )
        {
            if (tls(line))
                ++numGood;
        }

        cout << numGood << endl;
    }

    bool ssl(const string& s)
    {
        vector<string> abasInSupernet;
        vector<string> abasInHypernet;
        bool inHypernet = false;
        
        const auto endIt = next(end(s), -3);
        for (size_t i = 0; i < s.size() - 2; ++i)
        {
            if (s[i] == s[i + 2] && s[i] != s[i + 1])
                if (inHypernet)
                    abasInHypernet.push_back(s.substr(i, 3));
                else
                    abasInSupernet.push_back(s.substr(i, 3));

            if (s[i] == '[' || s[i] == ']')
                inHypernet = !inHypernet;
        }

        // bab -> aba in hypernet for comparison.
        transform(begin(abasInHypernet), end(abasInHypernet), begin(abasInHypernet),
            [](string s) { swap(s[0], s[1]); s[2] = s[0]; return s; });

        sort(begin(abasInSupernet), end(abasInSupernet));
        sort(begin(abasInHypernet), end(abasInHypernet));

        vector<string> intersection;
        set_intersection(begin(abasInSupernet), end(abasInSupernet), begin(abasInHypernet), end(abasInHypernet), back_inserter(intersection));

        return !intersection.empty();
    }

    void part2()
    {
        ifstream fs{ "2016/day07_input.txt" };

        int numGood = 0;
        for (string line; getline(fs, line); )
        {
            if (ssl(line))
                ++numGood;
        }

        cout << numGood << endl;
    }
}

namespace y2016 {
	void day07()
	{
		part1();
		part2();
	}
}