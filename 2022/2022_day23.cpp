#include "2022_utils.h"

#include <iterator>
#include <numeric>
#include <unordered_set>

using namespace std;

namespace {
  struct V {
    int x, y;
  };
  bool operator==(const V& l, const V& r) { return l.x == r.x && l.y == r.y; }
  bool operator<(const V& l, const V& r) { return l.x == r.x ? l.y < r.y : l.x < r.x; }
  struct Proposal {
    V from;
    V to;
  };
  struct Consideration {
    vector<V> spaces;
    V move;
  };

  auto part1() {
    auto in = load_strings("2022/2022_day23_input.txt");
    auto in2 = in;

    int h = in.size();
    int w = in[0].size();

    Consideration no_move_cons = {
      .spaces = {
        {-1, -1}, {0, -1}, {1, -1},
        {-1, 0}, {1, 0},
        {-1, 1}, {0, 1}, {1, 1}}
    };

    vector<Consideration> considerations = {
      {.spaces = {{-1, -1}, {0, -1}, {1, -1}}, .move = {0, -1}}, // N
      {.spaces = {{-1, 1}, {0, 1}, {1, 1}}, .move = {0, 1}}, // S
      {.spaces = {{-1, 1}, {-1, 0}, {-1, -1}}, .move = {-1, 0}}, // W
      {.spaces = {{1, 1}, {1, 0}, {1, -1}}, .move = {1, 0}} // E
    };
    int rounds = 10;
    for (int r = 0; r < 10; ++r) {
      vector<Proposal> proposed_moves;
      
      // Build proposals.
      for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
          if (in[y][x] == '#') {
            bool no_move = true;
            for (auto& s : no_move_cons.spaces) {
              V p{ s.x + x, s.y + y };
              if (p.x < w && p.y < h && p.x >= 0 && p.y >= 0) {
                if (in[p.y][p.x] == '#') {
                  no_move = false;
                }
              }
            }
            if (no_move) continue;

            for (int c = 0; c < considerations.size(); ++c) {
              const Consideration& consideration = considerations[(c + r) % considerations.size()];
              bool spaces_free = true;
              for (auto& s : consideration.spaces) {
                V p{ s.x + x, s.y + y };
                if (p.x < w && p.y < h && p.x >= 0 && p.y >= 0) {
                  if (in[p.y][p.x] == '#') {
                    spaces_free = false;
                  }
                }
              }
              if (spaces_free) {
                proposed_moves.push_back(Proposal{
                  .from = {x, y},
                  .to = { consideration.move.x + x, consideration.move.y + y } });
                break;
              }
            }
          }
        }
      }

      // Sort proposals by destination.
      sort(proposed_moves.begin(), proposed_moves.end(),
        [](const auto& p1, const auto& p2) { return p1.to < p2.to; });

      // Remove any with duplicate destination
      for (int i = 0; i < (int)proposed_moves.size() - 1;) {
        if (proposed_moves[i].to == proposed_moves[i + 1].to) {
          proposed_moves.erase(proposed_moves.begin() + i, proposed_moves.begin() + i + 2);
        }
        else {
          ++i;
        }
      }

      // Expand grid.
      for (auto& p : proposed_moves) {
        if (p.to.x < 0) {
          ++w;
          for (auto& pp : proposed_moves) {
            ++pp.from.x;
            ++pp.to.x;
          }
          for (auto& line : in) {
            line.insert(line.begin(), '.');
          }
        }
        if (p.to.x >= w) {
          ++w;
          for (auto& line : in) {
            line.insert(line.end(), '.');
          }
        }
        if (p.to.y < 0) {
          ++h;
          for (auto& pp : proposed_moves) {
            ++pp.from.y;
            ++pp.to.y;
          }
          in.insert(in.begin(), string(w, '.'));
        }
        if (p.to.y >= h) {
          ++h;
          in.insert(in.end(), string(w, '.'));
        }
      }

      // Make moves
      for (auto& p : proposed_moves) {
        in[p.from.y][p.from.x] = '.';
        in[p.to.y][p.to.x] = '#';
      }
    }

    // Find extents of elves.
    V min{9999, 9999};
    V max{-9999,-9999};
    for (int y = 0; y < h; ++y) {
      for (int x = 0; x < w; ++x) {
        if (in[y][x] == '#') {
          min.x = std::min(min.x, x);
          max.x = std::max(max.x, x);
          min.y = std::min(min.y, y);
          max.y = std::max(max.y, y);
        }
      }
    }

    // Count spaces.
    int spaces = 0;
    for (int y = min.y; y <= max.y; ++y) {
      for (int x = min.x; x <= max.x; ++x) {
        if (in[y][x] == '.') {
          ++spaces;
        }
      }
    }

    return spaces;
  }

  auto part2() {
    auto in = load_strings("2022/2022_day23_input.txt");

    int h = in.size();
    int w = in[0].size();

    Consideration no_move_cons = {
      .spaces = {
        {-1, -1}, {0, -1}, {1, -1},
        {-1, 0}, {1, 0},
        {-1, 1}, {0, 1}, {1, 1}}
    };

    vector<Consideration> considerations = {
      {.spaces = {{-1, -1}, {0, -1}, {1, -1}}, .move = {0, -1}}, // N
      {.spaces = {{-1, 1}, {0, 1}, {1, 1}}, .move = {0, 1}}, // S
      {.spaces = {{-1, 1}, {-1, 0}, {-1, -1}}, .move = {-1, 0}}, // W
      {.spaces = {{1, 1}, {1, 0}, {1, -1}}, .move = {1, 0}} // E
    };

    for (int r = 0; ; ++r) {
      int made_move = false;
      vector<Proposal> proposed_moves;

      // Build proposals.
      for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
          if (in[y][x] == '#') {
            bool no_move = true;
            for (auto& s : no_move_cons.spaces) {
              V p{ s.x + x, s.y + y };
              if (p.x < w && p.y < h && p.x >= 0 && p.y >= 0) {
                if (in[p.y][p.x] == '#') {
                  no_move = false;
                }
              }
            }
            if (no_move) continue;

            for (int c = 0; c < considerations.size(); ++c) {
              const Consideration& consideration = considerations[(c + r) % considerations.size()];
              bool spaces_free = true;
              for (auto& s : consideration.spaces) {
                V p{ s.x + x, s.y + y };
                if (p.x < w && p.y < h && p.x >= 0 && p.y >= 0) {
                  if (in[p.y][p.x] == '#') {
                    spaces_free = false;
                  }
                }
              }
              if (spaces_free) {
                proposed_moves.push_back(Proposal{
                  .from = {x, y},
                  .to = { consideration.move.x + x, consideration.move.y + y } });
                break;
              }
            }
          }
        }
      }

      // Sort proposals by destination.
      sort(proposed_moves.begin(), proposed_moves.end(),
        [](const auto& p1, const auto& p2) { return p1.to < p2.to; });

      // Remove any with duplicate destination.
      for (int i = 0; i < (int)proposed_moves.size() - 1;) {
        if (proposed_moves[i].to == proposed_moves[i + 1].to) {
          proposed_moves.erase(proposed_moves.begin() + i, proposed_moves.begin() + i + 2);
        }
        else {
          ++i;
        }
      }

      // Expand grid.
      for (auto& p : proposed_moves) {
        if (p.to.x < 0) {
          ++w;
          for (auto& pp : proposed_moves) {
            ++pp.from.x;
            ++pp.to.x;
          }
          for (auto& line : in) {
            line.insert(line.begin(), '.');
          }
        }
        if (p.to.x >= w) {
          ++w;
          for (auto& line : in) {
            line.insert(line.end(), '.');
          }
        }
        if (p.to.y < 0) {
          ++h;
          for (auto& pp : proposed_moves) {
            ++pp.from.y;
            ++pp.to.y;
          }
          in.insert(in.begin(), string(w, '.'));
        }
        if (p.to.y >= h) {
          ++h;
          in.insert(in.end(), string(w, '.'));
        }
      }

      // Make moves
      for (auto& p : proposed_moves) {
        in[p.from.y][p.from.x] = '.';
        in[p.to.y][p.to.x] = '#';
        made_move = true;
      }
      
      if (!made_move) {
        return r + 1;
      }
    }

    return 0;
  }
}

namespace y2022 {
  void day23() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
