#include "2022_utils.h"

#include <deque>

using namespace std;

namespace {
  struct Monkey {
    deque<int64_t> items;
    function<int64_t(int64_t)> op;
    int64_t next[2];
    int64_t divisor = 1;
    int64_t inspect_count = 0;
  };

  vector<Monkey> load_input() {
    auto input = load_strings("2022/2022_day11_input.txt");

    vector<Monkey> monkeys;
    for (auto& s : input) {
      if (starts_with(s, "Monkey")) {
        monkeys.push_back({});
      }
      else if (starts_with(s, "  Starting items: ")) {
        auto all_items = split(s, ":")[1];
        for (auto item : split(all_items, ",")) {
          monkeys.back().items.push_back(stoi(item));
        }
      }
    }

    // Check if we're executing the example.
    if (monkeys.size() == 4) {
      monkeys[0].op = [](int64_t old) { return old * 19; };
      monkeys[1].op = [](int64_t old) { return old + 6; };
      monkeys[2].op = [](int64_t old) { return old * old; };
      monkeys[3].op = [](int64_t old) { return old + 3; };

      monkeys[0].divisor = 23;
      monkeys[1].divisor = 19;
      monkeys[2].divisor = 13;
      monkeys[3].divisor = 17;

      monkeys[0].next[0] = 2;
      monkeys[0].next[1] = 3;
      monkeys[1].next[0] = 2;
      monkeys[1].next[1] = 0;
      monkeys[2].next[0] = 1;
      monkeys[2].next[1] = 3;
      monkeys[3].next[0] = 0;
      monkeys[3].next[1] = 1;
    }
    else {
      monkeys[0].op = [](int64_t old) { return old * 7; };
      monkeys[1].op = [](int64_t old) { return old + 4; };
      monkeys[2].op = [](int64_t old) { return old * 5; };
      monkeys[3].op = [](int64_t old) { return old * old; };
      monkeys[4].op = [](int64_t old) { return old + 1; };
      monkeys[5].op = [](int64_t old) { return old + 8; };
      monkeys[6].op = [](int64_t old) { return old + 2; };
      monkeys[7].op = [](int64_t old) { return old + 5; };

      monkeys[0].divisor = 19;
      monkeys[1].divisor = 3;
      monkeys[2].divisor = 13;
      monkeys[3].divisor = 17;
      monkeys[4].divisor = 2;
      monkeys[5].divisor = 11;
      monkeys[6].divisor = 5;
      monkeys[7].divisor = 7;

      monkeys[0].next[0] = 6;
      monkeys[0].next[1] = 4;
      monkeys[1].next[0] = 7;
      monkeys[1].next[1] = 5;
      monkeys[2].next[0] = 5;
      monkeys[2].next[1] = 1;
      monkeys[3].next[0] = 0;
      monkeys[3].next[1] = 4;
      monkeys[4].next[0] = 6;
      monkeys[4].next[1] = 2;
      monkeys[5].next[0] = 7;
      monkeys[5].next[1] = 3;
      monkeys[6].next[0] = 2;
      monkeys[6].next[1] = 1;
      monkeys[7].next[0] = 3;
      monkeys[7].next[1] = 0;
    }
    return monkeys;
  }

  auto part1() {
    vector<Monkey> monkeys = load_input();

    for (int round = 0; round < 20; ++round) {
      for (auto& m : monkeys) {
        while (!m.items.empty()) {
          int64_t item = m.items.front();
          item = m.op(item);
          item /= 3;
          m.items.pop_front();
          ++m.inspect_count;
          monkeys[m.next[item % m.divisor != 0]].items.push_back(item);
        }
      }
    }

    sort(begin(monkeys), end(monkeys),
      [](const auto& l, const auto& r) { return l.inspect_count < r.inspect_count; });

    return monkeys[monkeys.size() - 1].inspect_count * monkeys[monkeys.size() - 2].inspect_count;
  }

  auto part2() {
    vector<Monkey> monkeys = load_input();

    int64_t modulo = 1;
    for (auto& m : monkeys) {
      modulo *= m.divisor;
    }

    for (int round = 0; round < 10000; ++round) {
      for (auto& m : monkeys) {
        while (!m.items.empty()) {
          int64_t item = m.items.front();
          item = m.op(item);
          item %= modulo;
          m.items.pop_front();
          ++m.inspect_count;
          monkeys[m.next[item % m.divisor != 0]].items.push_back(item);
        }
      }
    }

    sort(begin(monkeys), end(monkeys),
      [](const auto& l, const auto& r) { return l.inspect_count < r.inspect_count; });

    return monkeys[monkeys.size() - 1].inspect_count * monkeys[monkeys.size() - 2].inspect_count;
  }
}

namespace y2022 {
  void day11() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
