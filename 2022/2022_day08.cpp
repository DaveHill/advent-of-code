#include "2022_utils.h"

using namespace std;

namespace {
  auto part1() {
    auto s = load_strings("2022/2022_day08_input.txt");

    int64_t result = 0;
    int h = s.size();
    int w = s[0].size();
    for (int y = 0; y < h; ++y) {
      for (int x = 0; x < w; ++x) {
        int blocked_count = 0;
        for (int i = x - 1; i >= 0; --i) {
          if (s[y][i] >= s[y][x]) {
            ++blocked_count;
            break;
          }
        }
        for (int i = x + 1; i < w; ++i) {
          if (s[y][i] >= s[y][x]) {
            ++blocked_count;
            break;
          }
        }
        for (int j = y - 1; j >= 0; --j) {
          if (s[j][x] >= s[y][x]) {
            ++blocked_count;
            break;
          }
        }
        for (int j = y + 1; j < h; ++j) {
          if (s[j][x] >= s[y][x]) {
            ++blocked_count;
            break;
          }
        }
        if (blocked_count < 4) {
          ++result;
        }
      }
    }

    return result;
  }

  auto part2() {
    auto s = load_strings("2022/2022_day08_input.txt");

    int64_t result = 0;
    int h = s.size();
    int w = s[0].size();
    int max_scenic = 0;
    for (int y = 0; y < h; ++y) {
      for (int x = 0; x < w; ++x) {
        int scenic[4] = { 0 };
        int blocked_count = 0;
        for (int i = x - 1; i >= 0; --i) {
          ++scenic[0];
          if (s[y][i] >= s[y][x]) {
            break;
          }
        }
        for (int i = x + 1; i < w; ++i) {
          ++scenic[1];
          if (s[y][i] >= s[y][x]) {
            break;
          }
        }
        for (int j = y - 1; j >= 0; --j) {
          ++scenic[2];
          if (s[j][x] >= s[y][x]) {
            break;
          }
        }
        for (int j = y + 1; j < h; ++j) {
          ++scenic[3];
          if (s[j][x] >= s[y][x]) {
            break;
          }
        }
        max_scenic = max(max_scenic, scenic[0] * scenic[1] * scenic[2] * scenic[3]);
      }
    }

    return max_scenic;
  }
}

namespace y2022 {
  void day08() {
    Profile p("day 8");
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
