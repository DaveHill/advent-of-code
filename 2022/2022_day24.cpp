#include "2022_utils.h"

#include <unordered_set>

using namespace std;

namespace {
  struct V {
    int x, y;
  };
  using Cell = bitset<5>;
  using Grid = vector<vector<Cell>>;
  struct State {
    shared_ptr<Grid> grid;
    V pos;
    int best_end_estimation;
    int moves = 0;
    int lap = 1;
  };
  
  size_t id(const State& s) {
    return (size_t)s.pos.x << 40 | (size_t)s.pos.y << 32 | (size_t)s.moves << 16 | (size_t)s.lap;
  }

  auto work(int num_laps) {
    auto in = load_strings("2022/2022_day24_input.txt");
    // 0 wall, 1 east, 2 south, 3 west, 4 north
    Grid grid;
    for (auto& l : in) {
      grid.emplace_back();
      for (char c : l) {
        if (c == '#') grid.back().push_back(Cell{}.set(0));
        if (c == '>') grid.back().push_back(Cell{}.set(1));
        if (c == 'v') grid.back().push_back(Cell{}.set(2));
        if (c == '<') grid.back().push_back(Cell{}.set(3));
        if (c == '^') grid.back().push_back(Cell{}.set(4));
        if (c == '.') grid.back().push_back(Cell{});
      }
    }
    V start{};
    V end{ .y = (int)grid.size() - 1 };
    while (grid[start.y][start.x].any()) ++start.x;
    while (grid[end.y][end.x].any()) ++end.x;

    Grid empty_grid = grid;
    for (auto& r : empty_grid) {
      for (auto& c : r) {
        c.set(1, false);
        c.set(2, false);
        c.set(3, false);
        c.set(4, false);
      }
    }

    auto distance_to_end = [&](const State& s) {
      V target = (s.lap == 2 ? start : end);
      return abs(s.pos.x - target.x) + abs(s.pos.y - target.y)
        + ((end.x - start.x) + (end.y - start.y)) * (num_laps - s.lap);
    };
    auto best_cmp = [&](const State& l, const State& r) {
      return l.best_end_estimation < r.best_end_estimation;
    };

    const int h = grid.size();
    const int w = grid[0].size();

    // Wrap in grid bounds.
    auto wrap_x = [&](int x) { return (x + w - 3) % (w - 2) + 1; };
    auto wrap_y = [&](int y) { return (y + h - 3) % (h - 2) + 1; };

    deque<State> next; // Keep sorted by distance to end.
    next.push_back(State{
      .grid = make_shared<Grid>(grid),
      .pos = start,
    });
    next.back().best_end_estimation = distance_to_end(next.back());
    int min_moves = numeric_limits<int>::max();
    unordered_set<size_t> visited;
    while (!next.empty()) {
      State curr = move(next.front());
      next.pop_front();
      const Grid& curr_grid = *curr.grid;

      static int i = 0;
      if (++i % 10000 == 0) {
        printf("(%d, %d) -> distance = %d (lap %d)\n", curr.pos.x, curr.pos.y, distance_to_end(curr), curr.lap);
      }

      constexpr bool print_grid = false;
      if (print_grid) {
        printf("\n");
        for (auto& r : curr_grid) {
          for (auto& c : r) {
            if (c.test(0)) printf("#");
            else if (c.count() == 1) {
              if (c.test(1)) printf(">");
              if (c.test(2)) printf("v");
              if (c.test(3)) printf("<");
              if (c.test(4)) printf(">");
            }
            else if (c.count() > 1) printf("%d", c.count());
            else printf(".");
          }
          printf("\n");
        }
      }

      // Check for completion.
      if (distance_to_end(curr) == 0) {
        // Add once since we're adjacent to the end pos.
        min_moves = min(min_moves, curr.moves);
        continue;
      }

      // Skip visited states.
      if (auto [it, inserted] = visited.insert(id(curr)); !inserted) {
        continue;
      }

      // Abandon this search if we can't possibly do better.
      if (curr.moves + distance_to_end(curr) >= min_moves) {
        continue;
      }

      // Advance to next grid state.
      State next_state{
        // Start with copy of the empty grid.
        .grid = make_shared<Grid>(empty_grid),
        .moves = curr.moves + 1,
        .lap = curr.lap,
      };

      // Check for next lap.
      if ((curr.lap == 1 && curr.pos.x == end.x && curr.pos.y == end.y)
        || (curr.lap == 2 && curr.pos.x == start.x && curr.pos.y == start.y)) {
        ++next_state.lap;
      }

      Grid& next_grid = *next_state.grid;

      for (int y = 1; y < h - 1; ++y) {
        for (int x = 1; x < w - 1; ++x) {
          // Move blizzards.
          next_grid[y][x].set(1, curr_grid[y][wrap_x(x - 1)].test(1));
          next_grid[y][x].set(2, curr_grid[wrap_y(y - 1)][x].test(2));
          next_grid[y][x].set(3, curr_grid[y][wrap_x(x + 1)].test(3));
          next_grid[y][x].set(4, curr_grid[wrap_y(y + 1)][x].test(4));
        }
      }

      for (const auto& dir : vector<V>{ {1, 0}, {-1, 0}, {0, 1}, {0, -1}, {0, 0} }) {
        V pn{ curr.pos.x + dir.x, curr.pos.y + dir.y };
        if (pn.x > 0 && pn.x < w && pn.y >= 0 && pn.y < h &&
            !next_grid[pn.y][pn.x].any()) {
          State s2 = next_state;
          s2.pos = pn;
          s2.best_end_estimation = s2.moves + distance_to_end(s2);
          auto it = lower_bound(next.begin(), next.end(), s2, best_cmp);
          next.insert(it, s2);
        }
      }
    }

    return min_moves;
  }
}

namespace y2022 {
  void day24() {
    cout << "part 1: " << work(/*laps=*/1) << "\n";
    cout << "part 2: " << work(/*laps=*/3) << "\n";
  }
}
