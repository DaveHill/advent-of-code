#include "2022_utils.h"

#include <iterator>
#include <numeric>
#include <unordered_set>

using namespace std;

namespace {
  auto part1() {
    const auto in_s = load_strings("2022/2022_day20_input.txt");
    vector<int> in;
    int zero_pos;
    for (auto& s : in_s) {
      in.push_back(atoi(s.c_str()));
      if (in.back() == 0) {
        zero_pos = in.size() - 1;
      }
    }

    vector<int> order(in.size());
    iota(order.begin(), order.end(), 0);

    for (int i = 0; i < in.size(); ++i) {
      // Find the index in the order.
      auto it = find(order.begin(), order.end(), i);
      auto moves = in[*it];

      //printf("\nAbout to move the %d\n", in[i]);
      int pos = distance(order.begin(), it);
      while (moves != 0) {
        if (moves > 0) {
          if (pos == in.size() - 1) {
            rotate(order.rbegin(), order.rbegin() + 1, order.rend());
            pos = 0;
          }
          swap(order[pos], order[pos + 1]);
          ++pos;
          if (pos == in.size() - 1) {
            rotate(order.rbegin(), order.rbegin() + 1, order.rend());
            pos = 0;
          }
          --moves;
        }
        else {
          if (pos == 0) {
            rotate(order.begin(), order.begin() + 1, order.end());
            pos = in.size() - 1;
          }
          swap(order[pos], order[pos - 1]);
          --pos;
          if (pos == 0) {
            rotate(order.begin(), order.begin() + 1, order.end());
            pos = in.size() - 1;
          }
          ++moves;
        }
      }

      //for (auto o : order) {
      //  printf("%d, ", in[o]);
      //}
    }

    int zero_end = distance(order.begin(), find(order.begin(), order.end(), zero_pos));

    int a = in[order[(zero_end + 1000) % in.size()]];
    int b = in[order[(zero_end + 2000) % in.size()]];
    int c = in[order[(zero_end + 3000) % in.size()]];

    return a + b + c;
  }

  auto part2() {
    const auto in_s = load_strings("2022/2022_day20_input.txt");
    vector<int64_t> in;

    int zero_pos;
    for (auto& s : in_s) {
      in.push_back(atoi(s.c_str()) * (int64_t)811589153);
      if (in.back() == 0) {
        zero_pos = in.size() - 1;
      }
    }

    vector<int> order(in.size());
    iota(order.begin(), order.end(), 0);

    for (int mix = 0; mix < 10; ++mix) {
      for (int i = 0; i < in.size(); ++i) {
        vector<int64_t> debug;
        for (int i = 0; i < in.size(); ++i) {
          debug.push_back(in[order[i]]);
        }

        // Find the index in the order.
        auto it = find(order.begin(), order.end(), i);

        // Make sure it wraps around at least once.
        int64_t moves_orig = in[*it];
        int64_t moves = moves_orig % ((int64_t)in.size() - 1) + in.size() - 1;

        int pos = distance(order.begin(), it);
        while (moves != 0) {
          if (moves > 0) {
            if (pos == in.size() - 1) {
              rotate(order.rbegin(), order.rbegin() + 1, order.rend());
              pos = 0;
            }
            swap(order[pos], order[pos + 1]);
            ++pos;
            if (pos == in.size() - 1) {
              rotate(order.rbegin(), order.rbegin() + 1, order.rend());
              pos = 0;
            }
            --moves;
          }
          else {
            if (pos == 0) {
              rotate(order.begin(), order.begin() + 1, order.end());
              pos = in.size() - 1;
            }
            swap(order[pos], order[pos - 1]);
            --pos;
            if (pos == 0) {
              rotate(order.begin(), order.begin() + 1, order.end());
              pos = in.size() - 1;
            }
            ++moves;
          }
        }
      }
    }

    int zero_end = distance(order.begin(), find(order.begin(), order.end(), zero_pos));

    int64_t a = in[order[(zero_end + 1000) % in.size()]];
    int64_t b = in[order[(zero_end + 2000) % in.size()]];
    int64_t c = in[order[(zero_end + 3000) % in.size()]];

    return (a + b + c);
  }
}

namespace y2022 {
  void day20() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
