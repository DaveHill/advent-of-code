#include "2022_utils.h"

#include <unordered_set>

using namespace std;

namespace {
  const map<char, int64_t> values = {
    { '0', 0 },
    { '1', 1 },
    { '2', 2 },
    { '-', -1 },
    { '=', -2 },
  };

  const map<int64_t, char> digits = {
    { -1, '-'},
    { -2, '='},
    { 0, '0'},
    { 1, '1'},
    { 2, '2'},
  };

  int64_t snafu_to_int(string snafu) {
    int64_t i = 1;
    int64_t n = 0;
    for (auto it = snafu.rbegin(); it != snafu.rend(); ++it) {
      n += values.at(*it) * i;
      i *= 5;
    }
    return n;
  }

  string int_to_snafu(int64_t n) {
    string snafu;
    while (n > 0) {
      int x = (n + 2) % 5 - 2;
      snafu = digits.at(x) + snafu;
      n /= 5;
      if (x < 0) n += 1;
    }
    return snafu;
  }
}

namespace y2022 {
  void day25() {
    auto in = load_strings("2022/2022_day25_input.txt");

    int64_t sum = 0;
    for (auto& s : in) {
      sum += snafu_to_int(s);
    }

    cout << int_to_snafu(sum) << "\n";
  }
}
