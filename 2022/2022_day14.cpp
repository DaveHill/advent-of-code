#include "2022_utils.h"

#include <deque>

using namespace std;

namespace {
  struct V {
    int x, y;
  };

  bool operator==(const V& l, const V& r) { return l.x == r.x && l.y == r.y; }
  bool operator<(const V& l, const V& r) { return l.x == r.x ? l.y < r.y : l.x < r.x; }
}

namespace std {
  template <>
  struct hash<V>
  {
    std::size_t operator()(const V& k) const {
      return std::hash<int>()(k.x ^ (k.y << 16));
    }
  };
}

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day14_input.txt");
    unordered_map<V, char> map;

    int maxy = 0;
    for (auto& s : input) {
      auto lines = split(s, " -> ");
      V begin;
      sscanf_s(lines[0].c_str(), "%d,%d", &begin.x, &begin.y);
      for (int i = 1; i < lines.size(); ++i) {
        V end;
        sscanf_s(lines[i].c_str(), "%d,%d", &end.x, &end.y);

        // Vertical
        if (begin.x == end.x) {
          for (int y = min(begin.y, end.y); y <= max(begin.y, end.y); ++y) {
            map[V{ begin.x, y }] = '#';
            maxy = max(maxy, y);
          }
        }
        // Horiz
        if (begin.y == end.y) {
          for (int x = min(begin.x, end.x); x <= max(begin.x, end.x); ++x) {
            map[V{ x, begin.y }] = '#';
          }
          maxy = max(maxy, begin.y);
        }
        begin = end;
      }
    }

    int sand_count = 0;
    bool full = false;
    while (!full) {
      ++sand_count;
      V sp(500, 0);

      while (true) {
        // Free falling.
        if (sp.y > maxy) {
          --sand_count;
          full = true;
          break;
        }
        // Look below.
        if (auto it = map.find({ sp.x, sp.y + 1 }); it == map.end()) {
          sp.y += 1;
          continue;
        }
        // Look below-left.
        if (auto it = map.find({ sp.x - 1, sp.y + 1 }); it == map.end()) {
          sp.x -= 1;
          sp.y += 1;
          continue;
        }
        // Look below-right.
        if (auto it = map.find({ sp.x + 1, sp.y + 1 }); it == map.end()) {
          sp.x += 1;
          sp.y += 1;
          continue;
        }
        // Stuck.
        map[sp] = 'o';
        break;
      }
    }

    return sand_count;
  }

  // unordered_map version.
  auto part2a() {
    auto input = load_strings("2022/2022_day14_input.txt");
    unordered_map<V, char> map;

    int maxy = 0;
    for (auto& s : input) {
      auto lines = split(s, " -> ");
      V begin;
      sscanf_s(lines[0].c_str(), "%d,%d", &begin.x, &begin.y);
      for (int i = 1; i < lines.size(); ++i) {
        V end;
        sscanf_s(lines[i].c_str(), "%d,%d", &end.x, &end.y);

        // Vertical
        if (begin.x == end.x) {
          for (int y = min(begin.y, end.y); y <= max(begin.y, end.y); ++y) {
            map[V{ begin.x, y }] = '#';
            maxy = max(maxy, y);
          }
        }
        // Horiz
        if (begin.y == end.y) {
          for (int x = min(begin.x, end.x); x <= max(begin.x, end.x); ++x) {
            map[V{ x, begin.y }] = '#';
          }
          maxy = max(maxy, begin.y);
        }
        begin = end;
      }
    }

    int sand_count = 0;
    bool full = false;
    while (!full) {
      ++sand_count;
      V sp(500, 0);

      while (true) {
        // On ground.
        if (sp.y == maxy + 1) {
          map[sp] = 'o';
          break;
        }
        // Move down.
        if (auto it = map.find({ sp.x, sp.y + 1 }); it == map.end()) {
          sp.y += 1;
          continue;
        }
        // Move down-left.
        if (auto it = map.find({ sp.x - 1, sp.y + 1 }); it == map.end()) {
          sp.x -= 1;
          sp.y += 1;
          continue;
        }
        // Move down-right.
        if (auto it = map.find({ sp.x + 1, sp.y + 1 }); it == map.end()) {
          sp.x += 1;
          sp.y += 1;
          continue;
        }
        // Stuck.
        map[sp] = 'o';

        if (sp.x == 500 && sp.y == 0) {
          full = true;
        }

        break;
      }
    }
    return sand_count;
  }

  // vector<vector<char>> version.
  auto part2b() {
    auto input = load_strings("2022/2022_day14_input.txt");

    int w = 10'000;
    int h = 500;
    vector<vector<char>> map(h, vector<char>(w, '.'));

    int maxy = 0;
    for (auto& s : input) {
      auto lines = split(s, " -> ");
      V begin;
      sscanf_s(lines[0].c_str(), "%d,%d", &begin.x, &begin.y);
      for (int i = 1; i < lines.size(); ++i) {
        V end;
        sscanf_s(lines[i].c_str(), "%d,%d", &end.x, &end.y);

        // Vertical
        if (begin.x == end.x) {
          for (int y = min(begin.y, end.y); y <= max(begin.y, end.y); ++y) {
            map[y][begin.x] = '#';
            maxy = max(maxy, y);
          }
        }
        // Horiz
        if (begin.y == end.y) {
          for (int x = min(begin.x, end.x); x <= max(begin.x, end.x); ++x) {
            map[begin.y][x] = '#';
          }
          maxy = max(maxy, begin.y);
        }
        begin = end;
      }
    }

    int sand_count = 0;
    bool full = false;
    while (!full) {
      ++sand_count;
      V sp(500, 0);

      while (true) {
        // On ground.
        if (sp.y == maxy + 1) {
          map[sp.y][sp.x] = 'o';
          break;
        }
        // Move down.
        if (map[sp.y + 1][sp.x] == '.') {
          sp.y += 1;
          continue;
        }
        // Move down-left.
        if (map[sp.y + 1][sp.x - 1] == '.') {
          sp.x -= 1;
          sp.y += 1;
          continue;
        }
        // Move down-right.
        if (map[sp.y + 1][sp.x + 1] == '.') {
          sp.x += 1;
          sp.y += 1;
          continue;
        }
        // Stuck.
        map[sp.y][sp.x] = 'o';

        if (sp.x == 500 && sp.y == 0) {
          full = true;
        }

        break;
      }
    }

    return sand_count;
  }
}

namespace y2022 {
  void day14() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2a() << "\n";
    cout << "part 2: " << part2b() << "\n";
  }
}
