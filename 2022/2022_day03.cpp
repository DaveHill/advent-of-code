#include "2022_utils.h"

#include <algorithm>

using namespace std;

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day03_input.txt");
    int64_t result = 0;

    for (auto& s : input) {
      std::vector<char> i;
      sort(begin(s), begin(s) + s.size() / 2);
      sort(begin(s) + s.size() / 2, end(s));
      set_intersection(begin(s), begin(s) + s.size() / 2,
        begin(s) + s.size() / 2, s.end(), back_inserter(i));
      char c = i[0];

      if (c >= 'a' && c <= 'z') {
        result += c - 'a' + 1;
      }
      else {
        result += c - 'A' + 27;
      }
    }

    return result;
  }

  auto part2() {
    auto input = load_strings("2022/2022_day03_input.txt");
    int64_t result = 0;

    for (auto& s : input) {
      csort(s);
    }

    for (int i = 0; i < input.size(); i += 3) {
      std::vector<char> intr;
      std::vector<char> intr2;
      set_intersection(input[i].begin(), input[i].end(), input[i + 1].begin(), input[i + 1].end(), back_inserter(intr));
      set_intersection(intr.begin(), intr.end(), input[i + 2].begin(), input[i + 2].end(), back_inserter(intr2));
      char c = intr2[0];
      if (c >= 'a' && c <= 'z') {
        result += c - 'a' + 1;
      }
      else {
        result += c - 'A' + 27;
      }
    }

    return result;
  }
}

namespace y2022 {
  void day03() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
