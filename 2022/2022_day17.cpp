#include "2022_utils.h"

#include <iterator>
#include <numeric>
#include <unordered_set>

using namespace std;

namespace {
  auto part1() {
    auto in = load_strings("2022/2022_day17_input.txt")[0];
    size_t next_input = 0;
    int64_t max_height = 0;
    const int width = 7;

    struct V {
      int x, y;
    };

    struct Rock {
      vector<V> p;
    };

    const vector<Rock> rocks = {
      {.p = {{ 0, 0 }, { 1, 0 }, {2, 0}, {3, 0}} },
      {.p = {{ 1, 2 }, { 0, 1 }, {1, 1}, {2, 1}, {1, 0}} },
      {.p = {{ 0, 0 }, { 1, 0 }, {2, 0}, {2, 1}, {2, 2}} },
      {.p = {{ 0, 0 }, { 0, 1 }, {0, 2}, {0, 3} } },
      {.p = {{ 0, 0 }, { 0, 1 }, {1, 0}, {1, 1} } },
    };

    vector<string> grid;
    grid.push_back("#######");
    for (int i = 0; i < 10; ++i) {
      grid.push_back("_______");
    }

    for (int rock = 0; rock < 2022; ++rock) {
      Rock r = rocks[rock % rocks.size()];

      // Set initial position.
      for (auto& p : r.p) {
        p.x += 2;
        p.y += max_height + 4;
      }

      // Grow grid.
      while (grid.size() < max_height + 10) {
        grid.push_back("_______");
      }

      // Simulate falling.
      while (true) {
        // Jets.
        char input = in[(next_input++) % in.size()];
        if (input == '<') {
          bool blocked = false;
          for (auto& p : r.p) {
            if (p.x == 0 || grid[p.y][p.x - 1] == '#') {
              blocked = true;
              break;
            }
          }
          if (!blocked) {
            for (auto& p : r.p) {
              p.x -= 1;
            }
          }
        }

        if (input == '>') {
          bool blocked = false;
          for (auto& p : r.p) {
            if (p.x >= width - 1 || grid[p.y][p.x + 1] == '#') {
              blocked = true;
              break;
            }
          }
          if (!blocked) {
            for (auto& p : r.p) {
              p.x += 1;
            }
          }
        }

        // Drop.
        {
          bool blocked = false;
          for (auto& p : r.p) {
            if (grid[p.y - 1][p.x] == '#') {
              blocked = true;
              break;
            }
          }

          if (!blocked) {
            for (auto& p : r.p) {
              p.y -= 1;
            }
          }
          else {
            // Stuck.
            for (auto& p : r.p) {
              grid[p.y][p.x] = '#';
              max_height = max((int)max_height, p.y);
            }
            break;
          }
        }
      }
    }

    return max_height;
  }

  struct GameState {
    int next_rock;
    int next_input;
    array<bitset<7>, 20> recent_lines;
  };

  bool operator==(const GameState& l, const GameState& r) {
    return l.next_input == r.next_input
      && l.next_rock == r.next_rock
      && l.recent_lines == r.recent_lines;
  }

  struct GameStats {
    int64_t tower_height;
    int64_t num_rocks_fell;
  };
}

namespace std {
  template <>
  struct hash<GameState>
  {
    std::size_t operator()(const GameState& s) const {
      auto h = std::hash<int>()(s.next_input) 
        ^ std::hash<int>()(s.next_rock << 16);
      for (const auto& l : s.recent_lines) {
        h ^= std::hash<std::bitset<7>>()(l);
      }
      return h;
    }
  };
}

namespace {
  auto part2() {
    auto in = load_strings("2022/2022_day17_input.txt")[0];
    int next_input = 0;
    int next_rock = 0;
    int64_t tower_height = 0;
    const int width = 7;

    struct V {
      int64_t x, y;
    };

    struct Rock {
      vector<V> p;
    };

    const vector<Rock> rocks = {
      {.p = {{ 0, 0 }, { 1, 0 }, {2, 0}, {3, 0}} },
      {.p = {{ 1, 2 }, { 0, 1 }, {1, 1}, {2, 1}, {1, 0}} },
      {.p = {{ 0, 0 }, { 1, 0 }, {2, 0}, {2, 1}, {2, 2}} },
      {.p = {{ 0, 0 }, { 0, 1 }, {0, 2}, {0, 3} } },
      {.p = {{ 0, 0 }, { 0, 1 }, {1, 0}, {1, 1} } },
    };

    vector<bitset<7>> grid;
    grid.emplace_back();
    grid.back().set();
    for (int i = 0; i < 20; ++i) {
      grid.emplace_back();
    }

    int64_t grid_cull_level = 100;
    int64_t grid_cull_lines = 50;
    int64_t removed_lines = 0;
    int64_t quit_rock = -1;

    // Map state to height.
    unordered_map<GameState, GameStats> game_states;

    int64_t num_rocks = 1'000'000'000'000;
    for (int64_t rock = 0; rock < num_rocks; ++rock) {
      // Check whether we've been here before.
      GameState s{
        .next_rock = next_rock,
        .next_input = next_input,
      };
      copy(grid.end() - s.recent_lines.size(), grid.end(), s.recent_lines.begin());
      if (auto prev_state = game_states.find(s); prev_state != game_states.end()) {
        printf("Repeated game state at tower height %lld after %lld rocks.\n", tower_height, rock);

        // Fast forward.
        int64_t height_increase = tower_height - prev_state->second.tower_height;
        int64_t rock_increase = rock - prev_state->second.num_rocks_fell;

        int64_t num_skips = (num_rocks - rock) / rock_increase;

        tower_height += (height_increase * num_skips);
        removed_lines += (height_increase * num_skips);
        rock += (rock_increase * num_skips);
        game_states.clear();

        printf("skipped to rock %lld\n", rock);
      }
      else {
        game_states[s] = { tower_height, rock };
      }

      Rock r = rocks[next_rock++];
      if (next_rock >= rocks.size()) {
        next_rock = 0;
      }

      // Grow grid.
      while (grid.size() < tower_height - removed_lines + 10) {
        grid.emplace_back();
      }

      // Cull old lines.
      if (grid.size() >= grid_cull_level) {
        grid.erase(grid.begin(), grid.begin() + grid_cull_lines);
        removed_lines += grid_cull_lines;
      }

      // Set initial position.
      for (auto& p : r.p) {
        p.x += 2;
        p.y += tower_height - removed_lines + 4;
      }

      // Simulate falling.
      while (true) {
        // Jets.
        char input = in[next_input++];
        if (next_input >= in.size()) {
          next_input = 0;
        }

        if (input == '<') {
          bool blocked = false;
          for (auto& p : r.p) {
            if (p.x == 0 || grid[p.y][p.x - 1]) {
              blocked = true;
              break;
            }
          }
          if (!blocked) {
            for (auto& p : r.p) {
              p.x -= 1;
            }
          }
        }

        if (input == '>') {
          bool blocked = false;
          for (auto& p : r.p) {
            if (p.x >= width - 1 || grid[p.y][p.x + 1]) {
              blocked = true;
              break;
            }
          }
          if (!blocked) {
            for (auto& p : r.p) {
              p.x += 1;
            }
          }
        }

        // Drop.
        {
          bool blocked = false;
          for (auto& p : r.p) {
            if (grid[p.y - 1][p.x]) {
              blocked = true;
              break;
            }
          }

          if (!blocked) {
            for (auto& p : r.p) {
              p.y -= 1;
            }
          }
          else {
            // Stuck.
            for (auto& p : r.p) {
              grid[p.y][p.x] = true;
              tower_height = max(tower_height, p.y + removed_lines);
            }
            break;
          }
        }
      }
    }

    return tower_height;
  }

}

namespace y2022 {
  void day17() {
    {
      Profile p("part 1");
      cout << "part 1: " << part1() << "\n";
    }

    // 1514285714287 too low.
    {
      Profile p("part 2");
      cout << "part 2: " << part2() << "\n";
    }
  }
}
