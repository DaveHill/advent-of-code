#include "2022_utils.h"

using namespace std;

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day05_input.txt");

    // Load stacks.
    vector<vector<char>> stacks(9);
    bool stacks_loaded = false;
    for (auto& s : input) {
      if (s.empty()) {
        stacks_loaded = true;
      }
      if (!stacks_loaded) {
        for (int i = 1; i <= 34; i += 4) {
          if (s[i] >= 'A' && s[i] <= 'Z') {
            stacks[i / 4].insert(begin(stacks[i / 4]), s[i]);
          }
        }
      }


      if (starts_with(s, "move")) {
        int amount, from, to;
        sscanf_s(s.c_str(), "move %d from %d to %d", &amount, &from, &to);

        for (int i = 0; i < amount; ++i) {
          stacks[to - 1].push_back(stacks[from - 1].back());
          stacks[from - 1].pop_back();
        }
      }
    }

    string result;
    for (auto& stack : stacks) {
      result += stack.back();
    }

    return result;
  }

  auto part2() {
    auto input = load_strings("2022/2022_day05_input.txt");

    // Load stacks.
    vector<vector<char>> stacks(9);
    bool stacks_loaded = false;
    for (auto& s : input) {
      if (s.empty()) stacks_loaded = true;
      if (!stacks_loaded) {
        for (int i = 1; i <= 34; i += 4) {
          if (s[i] >= 'A' && s[i] <= 'Z') {
            stacks[i / 4].insert(begin(stacks[i / 4]), s[i]);
          }
        }
      }

      if (starts_with(s, "move")) {
        int amount, from, to;
        sscanf_s(s.c_str(), "move %d from %d to %d", &amount, &from, &to);
        stacks[to - 1].insert(stacks[to - 1].end(), stacks[from - 1].end() - amount, stacks[from - 1].end());
        stacks[from - 1].resize(stacks[from - 1].size() - amount);
      }
    }

    string result;
    for (auto& stack : stacks) {
      result += stack.back();
    }

    return result;
  }
}

namespace y2022 {
  void day05() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
