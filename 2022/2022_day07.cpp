#include "2022_utils.h"

using namespace std;

namespace {
  auto part1() {
    auto in = load_strings("2022/2022_day07_input.txt");

    struct Dir {
      map<string, unique_ptr<Dir>> d;
      Dir* parent = nullptr;
      int64_t size = 0;
    };

    unique_ptr<Dir> root = make_unique<Dir>();
    Dir* cwd = root.get();
    set<Dir*> small_dirs;

    for (auto& s : in) {
      if (starts_with(s, "$ cd")) {
        string next = s.substr(5);
        if (next == "..") {
          cwd = cwd->parent;
        }
        else {
          cwd = cwd->d[next].get();
        }
      }
      else if (starts_with(s, "dir")) {
        auto dir = make_unique<Dir>();
        dir->parent = cwd;
        small_dirs.insert(dir.get());
        cwd->d[s.substr(4)] = move(dir);
      }
      else  if (starts_with(s, "$ ls")) {
        continue;
      }
      else {
        int size;
        char filename[128];
        sscanf_s(s.c_str(), "%d %s", &size, filename, 128);

        // Add size to current dir and all parents.
        Dir* d = cwd;
        while (d) {
          d->size += size;
          if (d->size >= 100000) {
            small_dirs.erase(d);
          }
          d = d->parent;
        }
      }
    }

    int64_t result = 0;
    for (auto& d : small_dirs) {
      result += d->size;
    }

    return result;
  }

  auto part2() {
    auto in = load_strings("2022/2022_day07_input.txt");

    struct Dir {
      map<string, unique_ptr<Dir>> d;
      Dir* parent = nullptr;
      int64_t size = 0;
    };

    unique_ptr<Dir> root = make_unique<Dir>();
    Dir* cwd = root.get();
    set<Dir*> all_dirs;

    int64_t result = 0;
    for (auto& s : in) {
      if (starts_with(s, "$ cd")) {
        string next = s.substr(5);
        if (next == "..") {
          cwd = cwd->parent;
        }
        else {
          cwd = cwd->d[next].get();
        }
      }
      else if (starts_with(s, "dir")) {
        auto dir = make_unique<Dir>();
        dir->parent = cwd;
        all_dirs.insert(dir.get());
        cwd->d[s.substr(4)] = move(dir);
      }
      else  if (starts_with(s, "$ ls")) {
        continue;
      }
      else {
        int size;
        char filename[128];
        sscanf_s(s.c_str(), "%d %s", &size, filename, 128);

        // Add size to current dir and all parents.
        Dir* d = cwd;
        while (d) {
          d->size += size;
          d = d->parent;
        }
      }
    }

    int64_t used_size = root->size;
    int64_t current_free = (70000000 - used_size);
    int64_t need_to_free = 30000000 - current_free;

    Dir* smallest_dir = root.get();
    for (auto& d : all_dirs) {
      if (d->size < smallest_dir->size && d->size >= need_to_free) {
        smallest_dir = d;
      }
    }

    return smallest_dir->size;
  }
}

namespace y2022 {
  void day07() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
