#include "2022_utils.h"

#include <deque>

using namespace std;

namespace {
  struct Packet {
    int value = -1;
    vector<Packet> sub;
  };

  Packet parse(const string& s) {
    Packet root;
    deque<Packet*> stack;
    stack.push_back(&root);
    for (int i = 1; i < s.size(); ++i) {
      if (s[i] == '[') {
        stack.back()->sub.emplace_back();
        stack.push_back(&stack.back()->sub.back());
      }
      if (s[i] == ']') {
        stack.pop_back();
      }
      if (s[i] >= '0' && s[i] <= '9') {
        // Stupid double-digit input...
        int x = s[i] - '0';
        if (s[i + 1] == '0') {
          x = 10;
          ++i;
        }
        stack.back()->sub.push_back({.value = x});
      }
    }
    return root;
  }

  int compare(Packet& l, Packet& r) {
    if (l.value >= 0 && r.value >= 0) {
      return l.value < r.value ? -1 :
        l.value > r.value ? 1 : 0;
    }

    if (l.value >= 0 && r.value < 0) {
      Packet l2{ .sub = {{.value = l.value}} };
      return compare(l2, r);
    }
    if (r.value >= 0 && l.value < 0) {
      Packet r2{ .sub = {{.value = r.value}} };
      return compare(l, r2);
    }

    for (int i = 0; ; ++i) {
      if (i == l.sub.size()) {
        return (i == r.sub.size() ? 0 : -1);
      }
      if (i == r.sub.size()) {
        return (i == l.sub.size() ? 0 : 1);
      }

      int c = compare(l.sub[i], r.sub[i]);
      if (c != 0) {
        return c;
      }
    }
  }

  auto part1() {
    auto input = load_strings("2022/2022_day13_input.txt");
    int result = 0;

    for (int i = 0; i < input.size(); i += 3) {
      Packet l1 = parse(input[i]);
      Packet r1 = parse(input[i + 1]);

      if (compare(l1, r1) == -1) {
        result += (i /3) + 1;
      }
    }

    return result;
  }

  auto part2() {
    auto input = load_strings("2022/2022_day13_input.txt");

    vector<Packet> packets;
    for (int i = 0; i < input.size(); i += 3) {
      Packet l1 = parse(input[i]);
      Packet r1 = parse(input[i + 1]);
      packets.push_back(l1);
      packets.push_back(r1);
    }

    Packet divider1 = parse("[[2]]");
    Packet divider2 = parse("[[6]]");

    packets.push_back(divider1);
    packets.push_back(divider2);

    sort(packets.begin(), packets.end(), [](auto& l, auto& r) {
      return compare(l, r) == -1;
    });

    int divider1_pos = 0;
    int divider2_pos = 0;
    for (int i = 0; i < packets.size(); ++i) {
      if (!compare(packets[i], divider1)) {
        divider1_pos = i + 1;
      }
      if (!compare(packets[i], divider2)) {
        divider2_pos = i + 1;
      }
    }

    return divider1_pos * divider2_pos;
  }
}

namespace y2022 {
  void day13() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
