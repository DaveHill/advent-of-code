#include "2022_utils.h"

#include <deque>
using namespace std;

namespace {
  struct V {
    int x, y;
  };

  bool operator==(const V& l, const V& r) { return l.x == r.x && l.y == r.y; }
}

namespace std {

  template <>
  struct hash<V>
  {
    std::size_t operator()(const V& k) const
    {
      return k.x ^ (k.y << 16);
    }
  };

}

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day12_input.txt");
    V start_pos;
    V end_pos;
    int h = input.size();
    int w = input[0].size();
    for (int y = 0; y < h; ++y) {
      for (int x = 0; x < w; ++x) {
        if (input[y][x] == 'S') {
          start_pos = { x, y };
        }
        if (input[y][x] == 'E') {
          end_pos = { x, y };
        }
      }
    }

    struct State {
      V pos;
      int steps;
    };

    deque<State> s;
    s.push_back({
      .pos = start_pos,
      .steps = 0,
      });

    unordered_map<V, int> min_steps;

    while (!s.empty()) {
      State curr = s.front();
      s.pop_front();

      if (input[curr.pos.y][curr.pos.x] == 'E') {
        continue;
      }

      for (V dv : { V{ -1, 0 }, V{ 1, 0 }, V{ 0, -1 }, V{ 0, 1 }}) {
        V next_pos = { curr.pos.x + dv.x, curr.pos.y + dv.y };
        if (next_pos.x < 0 || next_pos.x >= w || next_pos.y < 0 || next_pos.y >= h) {
          continue;
        }

        char curr_height = input[curr.pos.y][curr.pos.x];
        char next_height = input[next_pos.y][next_pos.x];
        if (curr_height == 'S') curr_height = 'a';
        if (next_height == 'E') next_height = 'z';
        if (next_height - curr_height > 1) {
          continue;
        }

        int next_steps = curr.steps + 1;

        // Abandon this path if we've reached the cell faster before.
        auto best = min_steps.find(next_pos);
        if (best != end(min_steps) && next_steps >= best->second) {
          continue;
        }
        min_steps[next_pos] = next_steps;

        s.push_back({ .pos = next_pos, .steps = next_steps, });
      }
    }

    return min_steps[end_pos];
  }

  auto part2() {
    auto input = load_strings("2022/2022_day12_input.txt");
    V start_pos;
    V end_pos;
    int h = input.size();
    int w = input[0].size();
    for (int y = 0; y < h; ++y) {
      for (int x = 0; x < w; ++x) {
        if (input[y][x] == 'S') {
          start_pos = { x, y };
        }
        if (input[y][x] == 'E') {
          end_pos = { x, y };
        }
      }
    }

    struct State {
      V pos;
      int steps;
    };

    deque<State> s;
    s.push_back({
      .pos = end_pos,
      .steps = 0,
      });

    unordered_map<V, int> min_steps;

    while (!s.empty()) {
      State curr = s.front();
      s.pop_front();

      if (input[curr.pos.y][curr.pos.x] == 'a') {
        continue;
      }

      for (V dv : { V{ -1, 0 }, V{ 1, 0 }, V{ 0, -1 }, V{ 0, 1 }}) {
        V next_pos = { curr.pos.x + dv.x, curr.pos.y + dv.y };
        if (next_pos.x < 0 || next_pos.x >= w || next_pos.y < 0 || next_pos.y >= h) {
          continue;
        }

        char curr_height = input[curr.pos.y][curr.pos.x];
        char next_height = input[next_pos.y][next_pos.x];
        if (curr_height == 'S') curr_height = 'a';
        if (next_height == 'E') next_height = 'z';
        if (curr_height - next_height > 1) {
          continue;
        }

        int next_steps = curr.steps + 1;

        // Abandon this path if we've reached the cell faster before.
        auto best = min_steps.find(next_pos);
        if (best != end(min_steps) && next_steps >= best->second) {
          continue;
        }
        min_steps[next_pos] = next_steps;

        s.push_back({ .pos = next_pos, .steps = next_steps, });
      }
    }

    int mins = 9999;
    for (auto& [pos, steps] : min_steps) {
      if (input[pos.y][pos.x] == 'a' && steps < mins) {
        mins = steps;
      }
    }

    return mins;
  }
}

namespace y2022 {
  void day12() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
