#include "2022_utils.h"

#include <iterator>
#include <numeric>
#include <unordered_set>

using namespace std;

namespace {
  struct Cube { int x = 0, y = 0, z = 0; };

  auto part1() {
    const auto in_s = load_strings("2022/2022_day18_input.txt");
    vector<Cube> cubes;
    for (auto& s : in_s) {
      Cube c;
      sscanf_s(s.c_str(), "%d,%d,%d", &c.x, &c.y, &c.z);
      cubes.push_back(c);
    }

    int adj = 0;
    for (int i = 0; i < cubes.size(); ++i) {
      for (int j = i + 1; j < cubes.size(); ++j) {
        int dx = abs(cubes[i].x - cubes[j].x);
        int dy = abs(cubes[i].y - cubes[j].y);
        int dz = abs(cubes[i].z - cubes[j].z);
      
        if (dx + dy + dz == 1 && dx * dy * dz == 0) {
          ++adj;
        }
      }
    }

    return cubes.size() * 6 - adj * 2;
  }

  int id(Cube c) {
    return c.x * 32 * 32 + c.y * 32 + c.z;
  }

  auto part2() {
    const auto in_s = load_strings("2022/2022_day18_input.txt");
    unordered_set<int> cubes;
    int cmin = 99, cmax = -1;
    for (auto& s : in_s) {
      Cube c;
      sscanf_s(s.c_str(), "%d,%d,%d", &c.x, &c.y, &c.z);
      cubes.insert(id(c));
      cmin = min(min(min(cmin, c.x), c.y), c.z);
      cmax = max(max(max(cmax, c.x), c.y), c.z);
    }

    // Gap to get around sides.
    ++cmax;
    --cmin;

    unordered_set<int> visited;
    int surface_area = 0;

    deque<Cube> next;
    next.emplace_back();

    while (!next.empty()) {
      Cube c = next.front();
      next.pop_front();

      // Count surface cubes.
      if (cubes.contains(id(c))) {
        ++surface_area;
        continue;
      }

      // Ignore out of bounds.
      if (c.x < cmin || c.x > cmax || c.y < cmin || c.y > cmax || c.z < cmin || c.z > cmax) {
        continue;
      }

      // Ignore previously visited.
      if (visited.contains(id(c))) {
        continue;
      }
      visited.insert(id(c));

      // Flood fill.
      for (Cube& dir : vector<Cube>{ {1, 0, 0}, { 0, 1, 0 }, { 0, 0, 1 } }) {
        for (int sign : {-1, 1}) {
          next.push_back(Cube{ c.x + dir.x * sign, c.y + dir.y * sign, c.z + dir.z * sign });
        }
      }
    }

    return surface_area;
  }
}

namespace y2022 {
  void day18() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
