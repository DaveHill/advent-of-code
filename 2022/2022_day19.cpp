#include "2022_utils.h"

#include <iterator>
#include <numeric>
#include <unordered_set>

using namespace std;

namespace {
  struct Blueprint {
    int id;
    int ore_robot_ore_cost = 0;
    int clay_robot_ore_cost = 0;
    int obsidian_robot_ore_cost = 0;
    int obsidian_robot_clay_cost = 0;
    int geode_robot_ore_cost = 0;
    int geode_robot_obsidian_cost = 0;
  };

  struct State {
    int ore = 0;
    int clay = 0;
    int obsidian = 0;
    int geodes = 0;
    int ore_robots = 0;
    int clay_robots = 0;
    int obsidian_robots = 0;
    int geode_robots = 0;
    int minutes_passed = 0;
  };

  template <typename T, typename... Rest>
  void hash_combine(std::size_t& seed, const T& v, const Rest&... rest)
  {
    seed ^= std::hash<T>{}(v)+0x9e3779b9 + (seed << 6) + (seed >> 2);
    (hash_combine(seed, rest), ...);
  }

  bool operator==(const State& l, const State& r) {
    return l.ore == r.ore && l.clay == r.clay && l.obsidian == r.obsidian
      && l.ore_robots == r.ore_robots && l.clay_robots == r.clay_robots
      && l.geode_robots == r.geode_robots && l.geodes == r.geodes;
  }
}

namespace std {
  template<>
  struct hash<State> {
    size_t operator()(const State& s) const {
      size_t h = std::hash<int>{}(s.ore);
      hash_combine(h,
        s.clay,
        s.obsidian,
        s.ore_robots,
        s.clay_robots,
        s.obsidian_robots,
        s.geode_robots,
        s.geodes);
      return h;
    }
  };
};

namespace {
  int MaximizeGeodes(const Blueprint& bp, int minutes) {
    int max_geodes = 0;

    const int max_ore_cost = max(max(max(
      bp.ore_robot_ore_cost,
      bp.clay_robot_ore_cost),
      bp.obsidian_robot_ore_cost),
      bp.geode_robot_ore_cost);
    const int max_clay_cost = bp.obsidian_robot_clay_cost;
    const int max_obsidian_cost = bp.geode_robot_obsidian_cost;

    // Best num minutes to get these resources.
    unordered_map<State, int> best;

    // Advance the state.
    auto advance = [](State s) {
      s.minutes_passed++;
      s.ore += s.ore_robots;
      s.clay += s.clay_robots;
      s.obsidian += s.obsidian_robots;
      s.geodes += s.geode_robots;
      return s;
    };

    deque<State> next;
    next.push_back(State{ .ore_robots = 1 });

    int max_minute = 0;

    while (!next.empty()) {
      State s = next.front();
      next.pop_front();

      if (s.minutes_passed == minutes) {
        if (s.geodes > max_geodes) {
          max_geodes = s.geodes;
        }
        continue;
      }

      if (s.minutes_passed > max_minute) {
        printf("minute %d\n", s.minutes_passed);
        max_minute = s.minutes_passed;
      }

      // Check whether we've been here before.
      if (auto [it, inserted] = best.emplace(s, s.minutes_passed); !inserted) {
        if (s.minutes_passed < it->second) {
          // Improved on this state, update best.
          it->second = s.minutes_passed;
        }
        else {
          // Didn't do better, abandon.
          continue;
        }
      }

      // Build a geode robot.
      if (s.ore >= bp.geode_robot_ore_cost
        && s.obsidian >= bp.geode_robot_obsidian_cost) {
        State s2 = advance(s);
        s2.ore -= bp.geode_robot_ore_cost;
        s2.obsidian -= bp.geode_robot_obsidian_cost;
        s2.geode_robots += 1;
        next.push_back(s2);

        // This is always the best thing to do, don't explore further.
        continue;
      }

      // Build an ore robot if more ore is needed.
      if (s.ore < max_ore_cost * 2 && s.ore >= bp.ore_robot_ore_cost) {
        State s2 = advance(s);
        s2.ore -= bp.ore_robot_ore_cost;
        s2.ore_robots += 1;
        next.push_back(s2);
      }

      // Build a clay robot if more clay is needed.
      if (s.clay < max_clay_cost * 2 && s.ore >= bp.clay_robot_ore_cost) {
        State s2 = advance(s);
        s2.ore -= bp.clay_robot_ore_cost;
        s2.clay_robots += 1;
        next.push_back(s2);
      }

      // Build an obsidian robot.
      if (s.obsidian < max_obsidian_cost * 2
        && s.ore >= bp.obsidian_robot_ore_cost
        && s.clay >= bp.obsidian_robot_clay_cost) {
        State s2 = advance(s);
        s2.ore -= bp.obsidian_robot_ore_cost;
        s2.clay -= bp.obsidian_robot_clay_cost;
        s2.obsidian_robots += 1;
        next.push_back(s2);
      }

      // Wait for more resources if something couldn't be built.
      if (s.ore < max_ore_cost
        || (s.clay_robots > 0 && s.clay < max_clay_cost)
        || (s.obsidian_robots > 0 && s.obsidian < max_obsidian_cost)) {
        next.push_back(advance(s));
      }
    }

    return max_geodes;
  }

  auto part1() {
    const auto in_s = load_strings("2022/2022_day19_input.txt");
    vector<Blueprint> blueprints;
    for (auto& s : in_s) {
      Blueprint bp;
      sscanf_s(s.c_str(),
        "Blueprint %d: Each ore robot costs %d ore. "
        "Each clay robot costs %d ore. "
        "Each obsidian robot costs %d ore and %d clay. "
        "Each geode robot costs %d ore and %d obsidian.",
        &bp.id,
        &bp.ore_robot_ore_cost, &bp.clay_robot_ore_cost, &bp.obsidian_robot_ore_cost,
        &bp.obsidian_robot_clay_cost, &bp.geode_robot_ore_cost, &bp.geode_robot_obsidian_cost);
      blueprints.push_back(bp);
    }

    int total_quality = 0;
    const Blueprint* best_blueprint = nullptr;
    for (const auto& bp : blueprints) {
      int max_geodes = MaximizeGeodes(bp, 24);
      int quality = bp.id * max_geodes;
      printf("ID %d x %d geodes = %d quality\n", bp.id, max_geodes, quality);
      total_quality += quality;
    }

    printf("total quality = %d\n", total_quality);
    return total_quality;
  }

  auto part2() {
    const auto in_s = load_strings("2022/2022_day19_input.txt");
    vector<Blueprint> blueprints;
    for (auto& s : in_s) {
      Blueprint bp;
      sscanf_s(s.c_str(),
        "Blueprint %d: Each ore robot costs %d ore. "
        "Each clay robot costs %d ore. "
        "Each obsidian robot costs %d ore and %d clay. "
        "Each geode robot costs %d ore and %d obsidian.",
        &bp.id,
        &bp.ore_robot_ore_cost, &bp.clay_robot_ore_cost, &bp.obsidian_robot_ore_cost,
        &bp.obsidian_robot_clay_cost, &bp.geode_robot_ore_cost, &bp.geode_robot_obsidian_cost);
      blueprints.push_back(bp);
      if (blueprints.size() == 3)
        break;
    }

    std::vector<int64_t> results;
    const Blueprint* best_blueprint = nullptr;
    for (const auto& bp : blueprints) {
      int max_geodes = MaximizeGeodes(bp, 32);
      results.push_back(max_geodes);
      printf("ID %d: %d geodes\n", bp.id, max_geodes);
    }

    int64_t mult = accumulate(results.begin(), results.end(),
      1, multiplies<int64_t>());
    printf("accum = %lld\n", mult);
    return mult;
  }
}

namespace y2022 {
  void day19() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
