#include "2022_utils.h"

using namespace std;

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day02_input.txt");
    int64_t result = 0;

    for (auto& s : input) {
      if (s[2] == 'X') {
        result += 1;
        if (s[0] == 'C') result += 6;
        if (s[0] == 'A') result += 3;
      }
      else if (s[2] == 'Y') {
        result += 2;
        if (s[0] == 'A') result += 6;
        if (s[0] == 'B') result += 3;
      }
      else if (s[2] == 'Z') {
        result += 3;
        if (s[0] == 'B') result += 6;
        if (s[0] == 'C') result += 3;
      }
    }

    return result;
  }

  auto part2() {
    auto input = load_strings("2022/2022_day02_input.txt");

    // Replace the 'response' sign.
    for (auto& s : input) {
      if (s[2] == 'X') {
        if (s[0] == 'A') s[2] = 'Z';
        if (s[0] == 'B') s[2] = 'X';
        if (s[0] == 'C') s[2] = 'Y';
      }
      else if (s[2] == 'Y') {
        if (s[0] == 'A') s[2] = 'X';
        if (s[0] == 'B') s[2] = 'Y';
        if (s[0] == 'C') s[2] = 'Z';
      }
      else if (s[2] == 'Z') {
        if (s[0] == 'A') s[2] = 'Y';
        if (s[0] == 'B') s[2] = 'Z';
        if (s[0] == 'C') s[2] = 'X';
      }
    }

    // Re-use the scoring from before.
    int64_t result = 0;
    for (auto& s : input) {
      if (s[2] == 'X') {
        result += 1;
        if (s[0] == 'C') result += 6;
        if (s[0] == 'A') result += 3;
      }
      else if (s[2] == 'Y') {
        result += 2;
        if (s[0] == 'A') result += 6;
        if (s[0] == 'B') result += 3;
      }
      else if (s[2] == 'Z') {
        result += 3;
        if (s[0] == 'B') result += 6;
        if (s[0] == 'C') result += 3;
      }
    }

    return result;
  }
}

namespace y2022 {
  void day02() {
    cout << "part 1: " << part1() << "\npart 2: " << part2() << "\n";
  }
}
