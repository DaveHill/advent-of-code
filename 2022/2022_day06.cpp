#include "2022_utils.h"

using namespace std;

namespace {
  auto part1() {
    auto s = load_strings("2022/2022_day06_input.txt")[0];

    for (int i = 0;; ++i) {
      if (s[i] != s[i + 1] && s[i] != s[i + 2] && s[i] != s[i + 3]
        && s[i + 1] != s[i + 2] && s[i + 1] != s[i + 3] && s[i + 2] != s[i + 3]) {
        return i + 4;
      }
    }

    return 0;
  }

  auto part2() {
    auto s = load_strings("2022/2022_day06_input.txt")[0];

    for (int i = 14;; ++i) {
      vector<char> s2(s.begin() + i - 14, s.begin() + i);
      csort(s2);
      if (adjacent_find(s2.begin(), s2.end()) == s2.end()) {
        return i;
      }
    }

    return 0;
  }
}

namespace y2022 {
  void day06() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
