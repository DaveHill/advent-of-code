#include "2022_utils.h"

#include <algorithm>

using namespace std;

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day04_input.txt");
    int64_t result = 0;

    for (auto& s : input) {
      int lo1, hi1, lo2, hi2;
      sscanf_s(s.c_str(), "%d-%d,%d-%d", &lo1, &hi1, &lo2, &hi2);
      if ((lo1 >= lo2 && hi1 <= hi2) || (lo2 >= lo1 && hi2 <= hi1))
        result++;
    }

    return result;
  }

  auto part2() {
    auto input = load_strings("2022/2022_day04_input.txt");
    int64_t result = 0;

    for (auto& s : input) {
      int lo1, hi1, lo2, hi2;
      sscanf_s(s.c_str(), "%d-%d,%d-%d", &lo1, &hi1, &lo2, &hi2);
      if (!(lo1 > hi2 || hi1 < lo2))
        result++;
    }

    return result;
  }
}

namespace y2022 {
  void day04() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
