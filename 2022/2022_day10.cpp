#include "2022_utils.h"

using namespace std;

namespace {
  auto part1() {
    auto input = load_strings("2022/2022_day10_input.txt");
    int64_t result = 0;

    int signals_to_sum[] = { 20, 60, 100, 140, 180, 220 };
    int x = 1;
    int cycle = 1;

    auto increment_cycle = [&] {
      if (ccontains(signals_to_sum, cycle)) {
        result += x * cycle;
      }
      ++cycle;
    };

    for (auto& s : input) {
      int operand;
      if (sscanf_s(s.c_str(), "addx %d", &operand)) {
        increment_cycle();
        increment_cycle();
        x += operand;
      }
      if (s == "noop") {
        increment_cycle();
      }
    }

    return result;
  }

  auto part2() {
    auto input = load_strings("2022/2022_day10_input.txt");

    int signals_to_sum[] = { 20, 60, 100, 140, 180, 220 };
    int x = 1;
    int cycle = 1;
    string pixels;

    auto increment_cycle = [&] {
      if (abs(x - (cycle % 40 - 1)) <= 1) {
        pixels += '#';
      }
      else {
        pixels += '.';
      }
      ++cycle;
    };

    for (auto& s : input) {
      int operand;
      if (sscanf_s(s.c_str(), "addx %d", &operand)) {
        increment_cycle();
        increment_cycle();
        x += operand;
      }
      if (s == "noop") {
        increment_cycle();
      }
    }

    for (int i = 0; i < pixels.size(); ++i) {
      if (i % 40 == 0) {
        printf("\n");
      }
      printf("%c", pixels[i]);
    }

    return 0;
  }
}

namespace y2022 {
  void day10() {
    cout << "part 1: " << part1() << "\n";
    cout << "part 2: " << part2() << "\n";
  }
}
