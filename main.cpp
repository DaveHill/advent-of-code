#include <iostream>
#include <functional>
#include <string>
#include <vector>

using namespace std;

namespace y2016 {
	void day01();
	void day02();
	void day03();
	void day04();
	void day05();
	void day06();
	void day07();
	void day08();
	void day09();
}

void do2016()
{
	using namespace y2016;
	cout << " *** 2016 ***\n";
	cout << "-- Day 01 --\n";
	day01();

	cout << "-- Day 02 --\n";
	day02();

	cout << "-- Day 03 --\n";
	day03();

	cout << "-- Day 04 --\n";
	day04();

	cout << "-- Day 05 --\n";
	 day05();

	cout << "-- Day 06 --\n";
	day06();

	cout << "-- Day 07 --\n";
	day07();

	cout << "-- Day 08 --\n";
	day08();

	cout << "-- Day 09 --\n";
	day09();
}

namespace y2019 {
	void day01();
	void day02();
}

void do2019() {
	using namespace y2019;

	printf("-- Day 01 --\n");
	day01();

	printf("-- Day 02 --\n");
	day02();
}

namespace y2020 {
	void day01();
	void day02();
	void day03();
	void day04();
	void day05();
	void day06();
	void day07();
	void day08();
	void day09();
	void day10();
	void day11();
	void day12();
	void day13();
	void day14();
	void day15();
	void day16();
	void day17();
	void day18();
	void day19();
	void day20();
}

void do2020() {
	using namespace y2020;

	printf(R"(                                       
 .-----.   .----.   .-----.   .----.   
/ ,-.   \ /  ..  \ / ,-.   \ /  ..  \  
'-'  |  |.  /  \  .'-'  |  |.  /  \  . 
   .'  / |  |  '  |   .'  / |  |  '  | 
 .'  /__ '  \  /  ' .'  /__ '  \  /  ' 
|       | \  `'  / |       | \  `'  /  
`-------'  `---''  `-------'  `---''   
	)" "\n");
	printf("-- day 01 --\n");
	day01();

	printf("-- day 02 --\n");
	day02();

	printf("-- day 03 --\n");
	day03();

	printf("-- day 04 --\n");
	day04();

	printf("-- day 05 --\n");
	day05();

	printf("-- day 06 --\n");
	day06();

	printf("-- day 07 --\n");
	day07();

	printf("-- day 08 --\n");
	day08();

	printf("-- day 09 --\n");
	day09();

	printf("-- Day 10 --\n");
	day10();

	printf("-- Day 11 --\n");
	day11();

	printf("-- Day 12 --\n");
	day12();

	printf("-- Day 13 --\n");
	day13();

	printf("-- Day 14 --\n");
	day14();

	printf("-- Day 15 --\n");
	day15();

	printf("-- Day 16 --\n");
	day16();

	printf("-- Day 17 --\n");
	day17();

	printf("-- Day 18 --\n");
	day18();

	printf("-- Day 19 --\n");
	day19();

	printf("-- Day 20 --\n");
	day20();
}

namespace y2021 {
	void day01();
	void day02();
	void day03();
	void day04();
	void day05();
	void day06();
	void day07();
	void day08();
	void day09();
	void day10();
	void day11();
	void day12();
	void day13();
	void day14();
	void day15();
	void day16();
	void day17();
	void day18();
	void day19();
	void day20();
	void day21();
	void day22();
	void day23();
	void day24();
	void day25();
}

void do2021() {
	using namespace y2021;

	printf(R"(
 _______  _______  _______  __   
/ ___   )(  __   )/ ___   )/  \  
\/   )  || (  )  |\/   )  |\/) ) 
    /   )| | /   |    /   )  | | 
  _/   / | (/ /) |  _/   /   | | 
 /   _/  |   / | | /   _/    | | 
(   (__/\|  (__) |(   (__/\__) (_
\_______/(_______)\_______/\____/
	)" "\n");
	printf("-- day 01 --\n");
	day01();

	printf("-- day 02 --\n");
	day02();

	printf("-- day 03 --\n");
	day03();

	printf("-- day 04 --\n");
	day04();

	printf("-- day 05 --\n");
	day05();

	printf("-- day 06 --\n");
	day06();

	printf("-- day 07 --\n");
	day07();

	printf("-- day 08 --\n");
	day08();

	printf("-- day 09 --\n");
	day09();

	printf("-- day 10 --\n");
	day10();

	printf("-- day 11 --\n");
	day11();

	printf("-- day 12 --\n");
	day12();

	printf("-- day 13 --\n");
	day13();

	printf("-- day 14 --\n");
	day14();

	printf("-- day 15 --\n");
	day15();

	printf("-- day 16 --\n");
	day16();

	printf("-- day 17 --\n");
	day17();

	printf("-- day 18 --\n");
	day18();

	printf("-- day 19 --\n");
	day19();

	printf("-- day 20 --\n");
	day20();

	printf("-- day 21 --\n");
	day21();

	printf("-- day 22 --\n");
	day22();

	printf("-- day 23 --\n");
	day23();

	printf("-- day 24 --\n");
	day24();

	printf("-- day 25 --\n");
	day25();
}

namespace y2022 {
	void day01();
	void day02();
	void day03();
	void day04();
	void day05();
	void day06();
	void day07();
	void day08();
	void day09();
	void day10();
	void day11();
	void day12();
	void day13();
	void day14();
	void day15();
	void day16();
	void day17();
	void day18();
	void day19();
	void day20();
	void day21();
	void day22();
	void day23();
	void day24();
	void day25();

	void do2022(bool all) {
		printf(R"(
    _          _             _          _       
  /\ \       / /\          /\ \       /\ \      
 /  \ \     / /  \        /  \ \     /  \ \     
/ /\ \ \   / / /\ \      / /\ \ \   / /\ \ \    
\/_/\ \ \ / / /\ \ \     \/_/\ \ \  \/_/\ \ \   
    / / //_/ /  \ \ \        / / /      / / /   
   / / / \ \ \   \ \ \      / / /      / / /    
  / / /  _\ \ \   \ \ \    / / /  _   / / /  _  
 / / /_/\_\\ \ \___\ \ \  / / /_/\_\ / / /_/\_\ 
/ /_____/ / \ \/____\ \ \/ /_____/ // /_____/ / 
\________/   \_________\/\________/ \________/        
)");

		auto days = vector{
			day01,
			day02,
			day03,
			day04,
			day05,
			day06,
			day07,
			day08,
			day09,
			day10,
			day11,
			day12,
			day13,
			day14,
			day15,
			day16,
			day17,
			day18,
			day19,
			day20,
			day21,
			day22,
			day23,
			day24,
			day25,
		};

		int start_day = (all ? 1 : days.size());
		for (int day = start_day; day <= days.size(); ++day) {
			printf("-- day %d --\n", day);
			days[day - 1]();
		}
	}
}

int main() {
	//do2016();
	//do2019();
	//do2020();
	//do2021();
	y2022::do2022(/*all=*/false);

	system("pause");
}
