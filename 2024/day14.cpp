#include "2024_utils.h"

namespace {

void part1() {
  auto in = load_strings("day14_input.txt");
  i64 result = 0;
  Vector2i dim = in.size() == 500 ? Vector2i{101, 103} : Vector2i{11,7};

  struct Robot {
    Vector2i p, v;
  };
  vector<Robot> robots;

  for (auto& line: in) {
    robots.emplace_back();
    auto& r = robots.back();
    sscanf_s(line.c_str(), "p=%d,%d v=%d,%d", &r.p[0], &r.p[1], &r.v[0], &r.v[1]);
  }

  for (int i = 0; i < 100; ++i) {
    for (auto& r : robots) {
      r.p += r.v + dim;
      r.p.x() %= dim.x();
      r.p.y() %= dim.y();
    }
  }

  i64 q1 = 0, q2 = 0, q3 = 0, q4 = 0;
  for (auto& r : robots) {
    if (r.p.x() < dim.x() / 2 && r.p.y() < dim.y() / 2) ++q1;
    if (r.p.x() < dim.x() / 2 && r.p.y() > dim.y() / 2) ++q2;
    if (r.p.x() > dim.x() / 2 && r.p.y() < dim.y() / 2) ++q3;
    if (r.p.x() > dim.x() / 2 && r.p.y() > dim.y() / 2) ++q4;
  }

  cout << std::format("part 1 result = {}\n", q1 * q2 * q3 * q4);
}

void part2() {
  auto in = load_strings("day14_input.txt");
  i64 result = 0;
  Vector2i dim = in.size() == 500 ? Vector2i{101, 103} : Vector2i{11,7};

  struct Robot {
    Vector2i p, v;
  };
  vector<Robot> robots;

  for (auto& line: in) {
    robots.emplace_back();
    auto& r = robots.back();
    sscanf_s(line.c_str(), "p=%d,%d v=%d,%d", &r.p[0], &r.p[1], &r.v[0], &r.v[1]);
  }

  int iterations = 0;
  vector<string> img(dim.y());
  float min_average_dist_from_center = std::numeric_limits<float>::max();
  while (true) {
    for (auto& r : img) {
      r.assign(dim.x(), '.');
    }

    ++iterations;
    float total_dist_from_center = 0.0f;
    for (auto& r : robots) {
      r.p += r.v + dim;
      r.p.x() %= dim.x();
      r.p.y() %= dim.y();

      total_dist_from_center += (r.p - dim / 2).cast<float>().norm();

      auto& px = img[r.p.y()][r.p.x()];
      if (px == '.') px = '1';
      else ++px;
    }
    float average_dist_from_center = total_dist_from_center / robots.size();
    if (average_dist_from_center < min_average_dist_from_center) {
      cout << "New min at itr " << iterations << " (" << average_dist_from_center << ")\n";
      min_average_dist_from_center = average_dist_from_center;
    }

    // Render.
    if (iterations >= 7080) {
      for (auto& r : img) {
        cout << r << "\n";
      }

      cout << "iteration " << iterations << "\n";
      char ans;
      std::cin >> ans;
      if (ans == 'y') break;
    }
  }

  cout << std::format("part 2 result = {}\n", iterations);
}

}  // namespace

void day14() {
  part1();
  part2();
}
