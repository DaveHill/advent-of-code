#include "2024_utils.h"

namespace {

void part1() {
  auto in = load_strings("day09_input.txt")[0];
  i64 result = 0;

  vector<int> disk;
  disk.reserve(100'000);
  for (int i = 0; i < in.length(); ++i) {
    int size = in[i] - '0';
    for (int j = 0; j < size; ++j) {
      disk.push_back((i % 2 == 0) ? i / 2 : -1);
    }
  }

  i64 next_free = 0;
  i64 next_move = disk.size() - 1;

  while (true) {
    while (next_move > 0 && disk[next_move] == -1) --next_move;
    while (next_free < disk.size() && disk[next_free] != -1) ++next_free;
    if (next_move <= next_free) break;  // done
    swap(disk[next_free], disk[next_move]);
  }

  for (int i = 0; i < disk.size(); ++i) {
    if (disk[i] == -1) break;
    result += i * disk[i];
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto in = load_strings("day09_input.txt")[0];
  i64 result = 0;

  vector<pair<int, int>> disk;
  disk.reserve(in.size());
  for (int i = 0; i < in.length(); ++i) {
    int size = in[i] - '0';
    disk.emplace_back((i % 2 == 0) ? i / 2 : -1, size);
  }

  for (int next_move = disk.size() - 1; next_move >= 0; --next_move) {
    if (disk[next_move].first == -1) continue;
    
    // Find a free spot.
    for (int i = 0; i < next_move; ++i) {
      int move_size = disk[next_move].second;
      int free_size = disk[i].second;
      if (disk[i].first == -1 && free_size >= move_size) {
        if (free_size > move_size) {
          // Split the free space into two blocks.
          disk[i].second = move_size;
          disk.emplace(disk.begin() + i + 1, -1, free_size - move_size);
          ++next_move;
        }

        swap(disk[i], disk[next_move]);
        break;
      }
    }
  }

  int id = 0;
  for (auto& [file, size] : disk) {
    if (file != -1) {
      for (int i = id; i < id + size; ++i)
        result += file * i;
    }
    id += size;
  }

  cout << std::format("part 1 result = {}\n", result);
}

}  // namespace

void day09() {
  part1();
  part2();
}
