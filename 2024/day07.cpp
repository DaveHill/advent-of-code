#include "2024_utils.h"
#include <span>

namespace {

void part1_it() {
  auto input = load_strings("day07_input.txt");
  i64 result = 0;

  for (auto& line: input) {
    i64 target;
    sscanf_s(line.c_str(), "%llu", &target);
    auto operands = split_int(split(line, ":")[1], " ");

    // Treat 0 as add and 1 as mul, in each position.
    // Treat first operator as add.
    for (i64 ops = 1; ops < (1ull << operands.size()); ops += 1) {
      i64 total = operands[0];
      for (int i = 1; i < operands.size(); ++i) {
        if (ops & 1ull << i)
          total *= operands[i];
        else
          total += operands[i];
      }
      if (total == target) {
        result += target;
        break;
      }
    }
  }

  cout << std::format("part 1 result = {}\n", result);
}

bool valid(i64 target, i64 current, span<i64> operands) {
  if (operands.empty()) return current == target;
  else return
    valid(target, current + operands[0], operands.subspan(1)) ||
    valid(target, current * operands[0], operands.subspan(1));
}

void part1() {
  auto input = load_strings("day07_input.txt");
  i64 result = 0;

  for (auto& line: input) {
    i64 target;
    sscanf_s(line.c_str(), "%llu", &target);
    auto operands = split_int(split(line, ":")[1], " ");

    if (valid(target, 0, operands)) {
      result += target;
    }
  }

  cout << format("part 1 result = {}\n", result);      
}

bool valid2(i64 target, i64 current, span<i64> operands) {
  if (operands.empty())
    return current == target;
  else return
    valid2(target, current + operands[0], operands.subspan(1)) ||
    valid2(target, current * operands[0], operands.subspan(1)) ||
    valid2(target, stoll(to_string(current) + to_string(operands[0])), operands.subspan(1));
}

void part2() {
  auto input = load_strings("day07_input.txt");
  i64 result = 0;

  for (auto& line : input) {
    i64 target;
    sscanf_s(line.c_str(), "%llu", &target);
    auto operands = split_int(split(line, ":")[1], " ");
    
    if (valid2(target, 0, operands))
      result += target;
  }

  cout << format("part 2 result = {}\n", result);      
}

}  // namespace

void day07() {
  part1_it();
  part1();
  part2();
}
