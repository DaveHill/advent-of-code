#include "2024_utils.h"
#include <unordered_set>

namespace {

void part1() {
  auto input = load_strings("day06_input.txt");
  i64 result = 0;
  
  i64 r = 0, c;
  for (i64 row = 0; r == 0 && row < input.size(); ++row) {
    for (i64 col = 0; r == 0 && col < input.size(); ++col) {
      if (input[row][col] == '^') {
        r = row;
        c = col;
      }
    }
  }

  set<pair<i64, i64>> positions;

  // Start direction: up.
  int dr = -1;
  int dc = 0;
  while (true) {
    positions.emplace(pair{r,c});

    // Check going out of bounds.
    if (r + dr < 0 || r + dr >= input.size() || c + dc < 0 || c + dc >= input[0].size()) {
      break;
    }

    while (input[r + dr][c + dc] == '#') {
      // Turn right.
      int dc_bak = dc;
      dc = dr * -1;
      dr = dc_bak;    
    }

    // Move.
    r += dr;
    c += dc;
  }

  cout << std::format("part 1 result = {}\n", positions.size());
}

void part2() {
  auto input = load_strings("day06_input.txt");
  i64 result = 0;
  
  i64 r = 0, c;
  for (i64 row = 0; r == 0 && row < input.size(); ++row) {
    for (i64 col = 0; r == 0 && col < input.size(); ++col) {
      if (input[row][col] == '^') {
        r = row;
        c = col;
      }
    }
  }

  int ri = r, ci = c;

  set<pair<i64, i64>> positions;

  // Start direction: up.
  int dr = -1;
  int dc = 0;
  while (true) {
    positions.emplace(pair{r,c});

    // Check going out of bounds.
    if (r + dr < 0 || r + dr >= input.size() || c + dc < 0 || c + dc >= input[0].size()) {
      break;
    }

    while (input[r + dr][c + dc] == '#') {
      // Turn right.
      int dc_bak = dc;
      dc = dr * -1;
      dr = dc_bak;    
    }

    // Move.
    r += dr;
    c += dc;
  }

  i64 loops = 0;
  for (auto& obstruction : positions) {
    if (obstruction == pair{ri, ci}) {
      // Ignore starting position.
      continue;
    }

    // Reset simulation.
    dr = -1;
    dc = 0;
    r = ri;
    c = ci;
    set<tuple<i64, i64, i64, i64>> prev_states; // r, c, dr, dc
    while (true) {
      // If we've been here before it's a loop.
      if (!prev_states.emplace(r, c, dr, dc).second) {
        ++loops;
        break;
      }

      // Check out of bounds.
      if (r + dr < 0 || r + dr >= input.size() || c + dc < 0 || c + dc >= input[0].size()) {
        break;
      }

      while (input[r + dr][c + dc] == '#' || pair{r + dr, c + dc} == obstruction) {
        // Turn right.
        int dc_bak = dc;
        dc = dr * -1;
        dr = dc_bak;
        prev_states.emplace(r, c, dr, dc);
      }

      // Move.
      r += dr;
      c += dc;
    }
  }

  cout << std::format("part 2 result = {}\n", loops);
}

}  // namespace

void day06() {
  part1();
  part2();
}
