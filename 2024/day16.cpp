#include "2024_utils.h"
#include <queue>

namespace {

void part1() {
  auto in = load_strings("day16_input.txt");
  i64 result = 0;

  Vector2i start;
  Vector2i dir(1, 0);
  Vector2i end;
  set<Vector2i> walls;
  map<pair<Vector2i, Vector2i>, int> cheapest;

  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in[0].size(); ++x) {
      switch (in[y][x]) {
      case '#': walls.emplace(x, y); break;
      case 'S': start = {x, y}; break;
      case 'E': end = {x, y}; break;
      }
    }
  }

  struct State16 {
    Vector2i pos;
    Vector2i dir;
    int cost = 0;
  };

  auto cmp = [&](const State16& l, const State16& r) { 
    return l.cost + (l.pos - end).cwiseAbs().sum() > r.cost + (r.pos - end).cwiseAbs().sum(); };
  priority_queue<State16, vector<State16>, decltype(cmp)> q(cmp);
  q.push({.pos = start, .dir = {1, 0}});

  int best_end = std::numeric_limits<int>::max();

  while (!q.empty()) {
    State16 s = q.top();
    q.pop();

    if (s.pos == end) best_end = min(best_end, s.cost);

    if (walls.contains(s.pos)) continue;
    auto c = cheapest.find({s.pos, s.dir});
    if (c != cheapest.end() && c->second <= s.cost) {
      continue;
    }
    cheapest[{s.pos, s.dir}] = s.cost;

    // Generate next.
    if (s.dir.x() == 0) {
      q.push({ .pos = s.pos, .dir = {1, 0}, .cost = s.cost + 1000 });
      q.push({ .pos = s.pos, .dir = {-1, 0}, .cost = s.cost + 1000 });
    } else {
      q.push({ .pos = s.pos, .dir = {0, 1}, .cost = s.cost + 1000 });
      q.push({ .pos = s.pos, .dir = {0, -1}, .cost = s.cost + 1000 });    
    }
    q.push({ .pos = s.pos + s.dir, .dir = s.dir, .cost = s.cost + 1 });
  }

  cout << std::format("part 1 result = {}\n", best_end);
}

void part2() {
  auto in = load_strings("day16_input.txt");
  i64 result = 0;

  Vector2i start;
  Vector2i dir(1, 0);
  Vector2i end;
  set<Vector2i> walls;
  map<pair<Vector2i, Vector2i>, int> cheapest;

  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in[0].size(); ++x) {
      switch (in[y][x]) {
      case '#': walls.emplace(x, y); break;
      case 'S': start = {x, y}; break;
      case 'E': end = {x, y}; break;
      }
    }
  }

  struct State16 {
    Vector2i pos;
    Vector2i dir;
    int cost = 0;
    set<Vector2i> path;
  };

  set<Vector2i> all_best_spots;

  auto cmp = [&](const State16& l, const State16& r) { 
    return l.cost + (l.pos - end).cwiseAbs().sum() > r.cost + (r.pos - end).cwiseAbs().sum(); };
  priority_queue<State16, vector<State16>, decltype(cmp)> q(cmp);
  q.push({.pos = start, .dir = {1, 0}, .path = {start}});

  int best_end = std::numeric_limits<int>::max();

  while (!q.empty()) {
    State16 s = q.top();
    q.pop();

    if (s.pos == end) {
      if (s.cost > best_end) continue;
      if (s.cost < best_end) {
        best_end = s.cost;
        all_best_spots.clear();
      }
      all_best_spots.insert(s.path.begin(), s.path.end());
    }

    if (walls.contains(s.pos)) continue;
    auto c = cheapest.find({s.pos, s.dir});
    if (c != cheapest.end() && c->second < s.cost) {
      continue;
    }
    cheapest[{s.pos, s.dir}] = s.cost;

    // Generate next.
    if (s.dir.x() == 0) {
      q.push({ .pos = s.pos, .dir = {1, 0}, .cost = s.cost + 1000, .path = s.path });
      q.push({ .pos = s.pos, .dir = {-1, 0}, .cost = s.cost + 1000, .path = s.path });
    } else {
      q.push({ .pos = s.pos, .dir = {0, 1}, .cost = s.cost + 1000, .path = s.path });
      q.push({ .pos = s.pos, .dir = {0, -1}, .cost = s.cost + 1000, .path = s.path });    
    }

    s.path.insert(s.pos + s.dir);
    q.push({ .pos = s.pos + s.dir, .dir = s.dir, .cost = s.cost + 1, .path = s.path });
  }

  cout << std::format("part 2 result = {}\n", all_best_spots.size());
}

}  // namespace

void day16() {
  part1();
  part2();
}
