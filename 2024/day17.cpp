#include "2024_utils.h"
#include <cassert>

namespace {

constexpr int adv = 0, bxl = 1, bst = 2, jnz = 3, bxc = 4,
  out = 5, bdv = 6, cdv = 7;

string run(i64 a, vector<i64>& program) {
  i64 b = 0, c = 0;
  auto combo = [&](i64 x) { 
    if (x <= 3) return x;
    if (x == 4) return a;
    if (x == 5) return b;
    if (x == 6) return c;
    assert(false);
  };

  string result;
  for (int pc = 0; pc < program.size(); pc += 2) {
    i64 v = program[pc + 1];
    switch (program[pc]) {
    case adv:
      a = a / pow(2, combo(v));
      break;
    case bdv:
      b = a / pow(2, combo(v));
      break;
    case cdv:
      c = a / pow(2, combo(v));
      break;
    case bxl:
      b = b ^ v;
      break;
    case bst:
      b = combo(v) % 8;
      break;
    case jnz:
      if (a != 0) pc = v - 2;
      break;
    case bxc:
      b = b ^ c;
      break;
    case out:
      auto val = to_string(combo(v) % 8);
      result = result.empty() ?  val : result + "," + val;
      break;
    };
  }
  return result;
}

void part1() {
  auto in = load_strings("day17_input.txt");
  i64 a, b, c;
  for (auto& line: in) {
    sscanf(line.c_str(), "Register A: %lld", &a);
    sscanf(line.c_str(), "Register B: %lld", &b);
    sscanf(line.c_str(), "Register C: %lld", &c);
  }
  auto program = split_int(split(in.back(), ": ")[1], ",");
  cout << std::format("part 1 result = {}\n", run(a, program));
}

void part2() {
  auto in = load_strings("day17_input.txt");
  auto program_str = split(in.back(), ": ")[1];
  auto program = split_int(program_str, ",");

  // Found by hand in debugger.
  i64 v = 0b101'011'010'010'011'101'000'001'011'100'000'011'110'000'001'111;
  assert(run(v, program) == program_str);
  cout << std::format("part 2 result = {}\n", v);
}

}  // namespace

void day17() {
  part1();
  part2();
}
