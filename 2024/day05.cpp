#include "2024_utils.h"

namespace {

void part1() {
  auto input = load_strings("day05_input.txt");
  i64 result = 0;

  struct Rule {
    i64 before, after;
  };
  vector<Rule> rules;
  bool rules_parsed = false;

  for (auto& line: input) {
    if (line == "") {
      rules_parsed = true;
      continue;
    }

    if (!rules_parsed) {
      Rule r;
      sscanf_s(line.c_str(), "%llu|%llu", &r.before, &r.after);
      rules.push_back(r);
      continue;
    }

    auto pages = split_int(line, ",");

    bool ok = true;
    // Reverse iterate, check no rules broken.
    for (auto it = pages.rbegin(); it < pages.rend(); ++it) {
      for (auto it2 = it + 1; it2 < pages.rend(); ++it2) {
        for (auto& r : rules) {
          if (*it == r.before && *it2 == r.after) {
            // Rule broken!
            ok = false;
          }
        }
      }
    }

    if (ok) result += pages[pages.size() / 2];
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day05_input.txt");
  i64 result = 0;

  struct Rule {
    i64 before, after;
  };
  vector<Rule> rules;
  bool rules_parsed = false;

  for (auto& line: input) {
    if (line == "") {
      rules_parsed = true;
      continue;
    }

    if (!rules_parsed) {
      Rule r;
      sscanf_s(line.c_str(), "%llu|%llu", &r.before, &r.after);
      rules.push_back(r);
      continue;
    }

    auto pages = split_int(line, ",");

    bool ok = true;
    // Reverse iterate, check no rules broken.
    for (auto it = pages.rbegin(); ok && it < pages.rend(); ++it) {
      for (auto it2 = it + 1; ok && it2 < pages.rend(); ++it2) {
        for (auto& r : rules) {
          if (*it == r.before && *it2 == r.after) {
            // Rule broken!
            ok = false;

            // Fix it.
            ranges::sort(pages, [&](i64 lhs, i64 rhs) {
              for (auto& r : rules) {
                if (lhs == r.before && rhs == r.after) return true;
              }
              return false;
            });
            result += pages[pages.size() / 2];
            break;
          }
        }
      }
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day05() {
  part1();
  part2();
}
