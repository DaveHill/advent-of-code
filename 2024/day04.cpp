#include "2024_utils.h"

namespace {

void part1() {
  auto input = load_strings("day04_input.txt");
  i64 result = 0;

  for (int r = 0; r < input.size(); ++r) {
    for (int c = 0; c < input[0].size(); ++c) {
      if (input[r][c] == 'X') {
        // Search all directions for MAS.
        for (int dc : {-1, 0, 1}) {
          for (int dr : {-1, 0, 1}) {
            if (dc == 0 && dr == 0) continue;
            // Check bounds.
            if (dr < 0 && r < 3) continue;
            if (dr > 0 && r >= input.size() - 3) continue;
            if (dc < 0 && c < 3) continue;
            if (dc > 0 && c >= input[0].size() - 3) continue;

            if (input[r + dr][c + dc] == 'M'
              && input[r + dr * 2][c + dc * 2] == 'A'
              && input[r + dr * 3][c + dc * 3] == 'S'){ 
              ++result;
            }
          }
        }
      }
    }
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day04_input.txt");
  i64 result = 0;

  for (int r = 1; r < input.size() - 1; ++r) {
    for (int c = 1; c < input[0].size() - 1; ++c) {
      if (input[r][c] == 'A') {
        bool x = false;

        // Offsets for the 2 M's.
        int dr1 = -1, dc1 = -1, dr2, dc2;
        for (int i = 0; i < 4; ++i) {
          // Second M is rotated one place.
          dr2 = -1 * dc1;
          dc2 = dr1;

          // Check the two M's, and the two S's should be opposite them.
          if (
            input[r + dr1][c + dc1] == 'M' && input[r - dr1][c - dc1] == 'S' &&
            input[r + dr2][c + dc2] == 'M' && input[r - dr2][c - dc2] == 'S') {
            x = true;
          }
          // Move the first M to where the second M was.
          dr1 = dr2;
          dc1 = dc2;
        }

        if (x) ++result;
      }
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day04() {
  part1();
  part2();
}
