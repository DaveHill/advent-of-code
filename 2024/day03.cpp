#include "2024_utils.h"

#include <regex>

namespace {

void part1() {
  auto input = load_string("day03_input.txt");
  i64 result = 0;

  auto regex_str = R"((mul\([0-9]+,[0-9]+\)))";
  regex r(regex_str);
  smatch matches;
  regex_search(input, matches, r);

  sregex_iterator begin(input.begin(), input.end(), r), end;

  for (auto it = begin; it != end; ++it) {
    i64 x, y;
    sscanf_s(it->str().c_str(), "mul(%llu,%llu)", &x, &y);
    result += x * y;
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_string("day03_input.txt");
  i64 result = 0;

  auto regex_str = R"((mul\([0-9]+,[0-9]+\)|do\(\)|don't\(\)))";
  regex r(regex_str);
  smatch matches;
  regex_search(input, matches, r);

  sregex_iterator begin(input.begin(), input.end(), r), end;

  bool on = true;
  for (auto it = begin; it != end; ++it) {
    if (it->str() == "do()") on = true;
    else if (it->str() == "don't()") on = false;
    else if (on) {
      i64 x, y;
      sscanf_s(it->str().c_str(), "mul(%llu,%llu)", &x, &y);
      result += x * y;
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day03() {
  part1();
  part2();
}
