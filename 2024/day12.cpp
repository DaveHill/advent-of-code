#include "2024_utils.h"

namespace {

void part1() {
  auto in = load_strings("day12_input.txt");
  i64 result = 0;

  set<Vector2i> complete;
  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in[0].size(); ++x) {
      char name = in[y][x];

      // Find all plots in the region.
      set<Vector2i> plots;
      deque<Vector2i> q = {{x, y}};
      while (!q.empty()) {
        Vector2i p = q.front();
        q.pop_front();
        if (complete.contains(p)) continue;

        // Ignore if out of bounds or name doesn't match.
        if ((p.array() < 0).any() || (p.array() >= in[0].size()).any()
          || in[p.y()][p.x()] != name) {
          continue;
        }

        // Add plot to the region.
        complete.insert(p);
        plots.insert(p);

        // Search neighbors.
        for (auto& d : vector<Vector2i>{{0, -1}, {-1, 0}, {1, 0}, {0, 1}}) {
          auto neighbor = p + d;
          q.push_back(neighbor);
        }
      }

      // Sum the plots.
      for (auto& p : plots) {
        for (auto& d : vector<Vector2i>{{0, -1}, {-1, 0}, {1, 0}, {0, 1}}) {
          if (!plots.contains(p + d)) {
             result += plots.size();
          }
        }
      }
    }
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto in = load_strings("day12_input.txt");
  i64 result = 0;

  set<Vector2i> complete;
  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in[0].size(); ++x) {
      char name = in[y][x];

      // Find all plots in the region.
      set<Vector2i> plots;
      deque<Vector2i> q = {{x, y}};
      while (!q.empty()) {
        Vector2i p = q.front();
        q.pop_front();
        if (complete.contains(p)) continue;

        // Ignore if out of bounds or name doesn't match.
        if ((p.array() < 0).any() || (p.array() >= in[0].size()).any()
          || in[p.y()][p.x()] != name) {
          continue;
        }

        // Add plot to the region.
        complete.insert(p);
        plots.insert(p);

        // Search neighbors.
        for (auto& d : vector<Vector2i>{{0, -1}, {-1, 0}, {1, 0}, {0, 1}}) {
          auto neighbor = p + d;
          q.push_back(neighbor);
        }
      }

      // Check which direction a side exists.
      map<Vector2i, set<Vector2i>> sides;
      for (auto& p : plots) {
        for (auto& d : vector<Vector2i>{{0, -1}, {-1, 0}, {1, 0}, {0, 1}}) {
          if (!plots.contains(p + d)) sides[p].insert(d);
        }
      }

      // Only count the side for the top-left most plot.
      for (auto& p : plots) {
        for (auto side : sides[p]) {
          bool neighbor_has = false;
          for (auto& d : vector<Vector2i>{{0, -1}, {-1, 0}}) {
            if (sides[p + d].contains(side)) {
              neighbor_has = true;
            }
          }
          if (!neighbor_has)
            result += plots.size();
        }
      }
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day12() {
  part1();
  part2();
}
