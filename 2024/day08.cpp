#include "2024_utils.h"
#include <Eigen/Core>
using Eigen::Vector2i;

namespace {

void part1() {
  auto in = load_strings("day08_input.txt");
  map<char, vector<Vector2i>> all_antennas;

  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in.size(); ++x) {
      if (in[y][x] != '.') {
        all_antennas[in[y][x]].emplace_back(x, y);
      }
    }
  }
  auto in_bounds = [&](Vector2i& pos) {
    return
      pos.x() >= 0 && pos.x() < in[0].size() &&
      pos.y() >= 0 && pos.y() < in.size();
  };

  set<pair<int, int>> antinodes;
  for (auto& [type, antennas] : all_antennas) {
    for (int i = 0; i < antennas.size(); ++i) {
      auto& antenna_a = antennas[i];
      for (int j = i + 1; j < antennas.size(); ++j) {
        auto& antenna_b = antennas[j];
        // b - a + b
        // = 2b - a
        Vector2i antinode1 = 2 * antenna_b - antenna_a;
        Vector2i antinode2 = 2 * antenna_a - antenna_b;
        if (in_bounds(antinode1)) antinodes.emplace(antinode1.x(), antinode1.y());
        if (in_bounds(antinode2)) antinodes.emplace(antinode2.x(), antinode2.y());
      }
    }
  }

  cout << std::format("part 1 result = {}\n", antinodes.size());
}

void part2() {
  auto in = load_strings("day08_input.txt");
  map<char, vector<Vector2i>> all_antennas;
  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in[0].size(); ++x) {
      if (in[y][x] != '.') {
        all_antennas[in[y][x]].emplace_back(x, y);
      }
    }
  }

  auto in_bounds = [&](Vector2i& pos) {
    return
      pos.x() >= 0 && pos.x() < in[0].size() &&
      pos.y() >= 0 && pos.y() < in.size();
  };

  set<pair<int, int>> antinodes;
  for (auto& [freq, antennas] : all_antennas) {
    for (int i = 0; i < antennas.size(); ++i) {
      auto antenna_a = antennas[i];
      for (int j = i + 1; j < antennas.size(); ++j) {
        auto antenna_b = antennas[j];
        auto delta = antenna_a - antenna_b;
        // Positive direction.
        auto antinode = antenna_a;
        while (in_bounds(antinode)) {
          antinodes.emplace(antinode.x(), antinode.y());
          antinode += delta;
        }
        // Negative direction.
        antinode = antenna_b;
        while (in_bounds(antinode)) {
          antinodes.emplace(antinode.x(), antinode.y());
          antinode -= delta;
        }
      }
    }
  }

  cout << std::format("part 1 result = {}\n", antinodes.size());
}

}  // namespace

void day08() {
  part1();
  part2();
}
