#include "2024_utils.h"

namespace {

void part1() {
  auto in = load_strings("day18_input.txt");
  i64 result = 0;
  int w{},h{};
  
  int bytes = in.size() == 25 ? 12 : 1024;

  set<Vector2i> corrupt;
  for (int i = 0; i < bytes; ++i) {
    auto l = split_int(in[i], ",");
    w = max<int>(w, l[0] + 1);
    h = max<int>(w, l[1] + 1);
    corrupt.emplace(l[0], l[1]);
  }

  struct State {
    Vector2i p{};
    int cost = 0;
  };

  map<Vector2i, int> cost;
  deque<State> q;
  q.emplace_back();
  int cheapest = numeric_limits<int>::max();

  while (!q.empty()) {
    State s = q.front();
    q.pop_front();

    if (s.p == Vector2i(w - 1, h - 1)) {
      cheapest = min(cheapest, s.cost);
    }

    if ((s.p.array() < 0).any() || (s.p.array() >= w).any()) {
      continue;
    }
    if (corrupt.contains(s.p))
      continue;
    auto it = cost.find(s.p);
    if (it != cost.end() && it->second <= s.cost)
      continue;
    cost[s.p] = s.cost;


    for (auto d : {Vector2i{0, -1}, {0, 1}, {-1, 0}, {1, 0}}) {
      State s2 = s;
      s2.p += d;
      ++s2.cost;
      q.push_back(s2);
    }
  }

  cout << std::format("part 1 result = {}\n", cheapest);
}

void part2() {
  auto in = load_strings("day18_input.txt");
  i64 result = 0;
  int w{},h{};
  
  int bytes = in.size() == 25 ? 12 : 1024;

  for (int bytes = 1024; bytes < in.size(); ++bytes) {
    set<Vector2i> corrupt;
    for (int i = 0; i < bytes; ++i) {
      auto l = split_int(in[i], ",");
      w = max<int>(w, l[0] + 1);
      h = max<int>(w, l[1] + 1);
      corrupt.emplace(l[0], l[1]);
    }

    struct State {
      Vector2i p{};
      int cost = 0;
    };

    map<Vector2i, int> cost;
    deque<State> q;
    q.emplace_back();
    int cheapest = numeric_limits<int>::max();

    while (!q.empty()) {
      State s = q.front();
      q.pop_front();

      if (s.p == Vector2i(w - 1, h - 1)) {
        cheapest = min(cheapest, s.cost);
      }

      if ((s.p.array() < 0).any() || (s.p.array() >= w).any()) {
        continue;
      }
      if (corrupt.contains(s.p))
        continue;
      auto it = cost.find(s.p);
      if (it != cost.end() && it->second <= s.cost)
        continue;
      cost[s.p] = s.cost;


      for (auto d : {Vector2i{0, -1}, {0, 1}, {-1, 0}, {1, 0}}) {
        State s2 = s;
        s2.p += d;
        ++s2.cost;
        q.push_back(s2);
      }
    }

    if (cheapest == numeric_limits<int>::max()) {
      cout << std::format("part 2 result = {}\n", in[bytes -1]);
      break;
    }
  }
}

}  // namespace

void day18() {
  part1();
  part2();
}
