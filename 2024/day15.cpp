#include "2024_utils.h"

#include "stb_image_write.h"

namespace {

void part1() {
  auto in = load_strings("day15_input.txt");
  i64 result = 0;

  Vector2i r;
  set<Vector2i> boxes;
  set<Vector2i> walls;

  int y = 0;
  for (; y < in.size(); ++y) {
    if (in[y].empty()) break;
    for (int x = 0; x < in[0].size(); ++x) {
      if (in[y][x] == '#') walls.emplace(x, y);
      if (in[y][x] == 'O') boxes.emplace(x, y);
      if (in[y][x] == '@') r = {x, y};
    }
  }

  function<bool(Vector2i, Vector2i)> move_box;
  move_box = [&](Vector2i p, Vector2i d) {
      if (walls.contains(p + d)) return false;
      auto other_box = boxes.find(p + d);
      if (other_box == boxes.end() || move_box(*other_box, d)) {
        boxes.erase(p);
        boxes.insert(p + d);
        return true;
      }
      return false;
    };

  for (; y < in.size(); ++y) {
    for (char dir : in[y]) {
      Vector2i d;
      switch (dir) {
      case '>': d = {1, 0}; break;
      case '<': d = {-1, 0}; break;
      case '^': d = {0, -1}; break;
      case 'v': d = {0, 1}; break;
      };

      // Move robot.
      if (walls.contains(r + d)) continue;
      auto box = boxes.find(r + d);
      if (box == boxes.end() || move_box(*box, d)) {
        r += d;
      }
    }
  }

  for (auto& box : boxes) {
    result += box.dot(Vector2i{1, 100});
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto in = load_strings("day15_input.txt");
  i64 result = 0;

  Vector2i r;
  set<Vector2i> boxes;
  set<Vector2i> walls;

  int y = 0;
  for (; y < in.size(); ++y) {
    if (in[y].empty()) break;
    for (int x = 0; x < in[0].size(); ++x) {
      if (in[y][x] == '#') walls.emplace(x * 2, y);
      if (in[y][x] == '#') walls.emplace(x * 2 + 1, y);
      if (in[y][x] == 'O') boxes.emplace(x * 2, y);
      if (in[y][x] == '@') r = {x * 2, y};
    }
  }

  auto find_push = [&](Vector2i p, Vector2i d) {
    if (d.x() != 0) {
      return vector{ boxes.find(p + d * 2)};
    } else {
      return vector{
        boxes.find(p + d),
        boxes.find(p + d + Vector2i(-1, 0)),
        boxes.find(p + d + Vector2i(1, 0)),
      };
    }
   };
  function<bool(Vector2i, Vector2i)> can_move_box;
  can_move_box = [&](Vector2i p, Vector2i d) {
      if (walls.contains(p + d)) return false;
      if ((d.x() == 1 || d.y() != 0) && walls.contains(p + d + Vector2i{1, 0})) return false;
      return ranges::all_of(find_push(p, d),
        [&](auto b){ return b == boxes.end() || can_move_box(*b, d); });
    };

  function<bool(Vector2i, Vector2i)> move_box;
  move_box = [&](Vector2i p, Vector2i d) {
      for (auto& b : find_push(p, d)) {
        if (b != boxes.end())
          move_box(*b, d);
      }
      boxes.erase(p);
      boxes.insert(p + d);
      return true;
  };

  int w = in[0].size() * 2;
  int h = y;

  int i = 0;
  for (; y < in.size(); ++y) {
    for (char dir : in[y]) {
      // Draw state.
      bool interactive = false;
      if (interactive) {
        cout << "Move " << dir << "\n";
        vector<string> lines(h, string(w, '.'));
        for (auto& box : boxes) {
          lines[box.y()][box.x()] = '[';
          lines[box.y()][box.x() + 1] = ']';
        }
        for (auto& wall : walls) {
          lines[wall.y()][wall.x()] = '#';
        }
        lines[r.y()][r.x()] = '@';
        for (auto& line : lines) {
          cout << line << "\n";
        }
        system("pause");
      }

      bool write_pngs = true;
      if (write_pngs && i < 600) {
        struct Pix { uint8_t r, g, b; };
        vector<Pix> img(w * h, Pix{ 255, 255, 255 });
        for (auto& box : boxes) {
          img[box.y() * w + box.x()] = Pix{ 155, 103, 60 };
          img[box.y() * w + box.x() + 1] = Pix{ 155, 103, 60 };
        }
        for (auto& wall : walls) {
          img[wall.y() * w + wall.x()] = Pix{ 50, 50, 50 };
        }
        img[r.y() * w + r.x()] = Pix{ 0, 255, 0 };
        stbi_write_png(format("C:\\tmp\\aoc\\2024_day15\\{:05}.png", i++).c_str(), w, h, 3, (void*)img.data(), w * sizeof(Pix));
      }

      Vector2i d;
      switch (dir) {
      case '>': d = {1, 0}; break;
      case '<': d = {-1, 0}; break;
      case '^': d = {0, -1}; break;
      case 'v': d = {0, 1}; break;
      };

      if (walls.contains(r + d)) continue;
      auto box = boxes.find(r + d);
      if (box == boxes.end()) box = boxes.find(r + d + Vector2i{-1, 0});
      if (box == boxes.end() || can_move_box(*box, d)) {
        if (box != boxes.end()) {
          move_box(*box, d);
        }
        r += d;
      }
    }
  }

  for (auto& box : boxes) {
    result += box.dot(Vector2i{1, 100});
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day15() {
  part1();
  part2();
}
