#include "2024_utils.h"
#include <ranges>
using namespace std;

namespace {

void run(int part, int iter) {
  auto in_txt = load_strings("day11_input.txt");
  vector<i64> in = split_int(in_txt[0], " ");

  // val, count
  unordered_map<i64, i64> m;
  for (auto v : in) m.emplace(v, 1);

  for (int i = 0; i < iter; ++i) {
    unordered_map<i64, i64> n;
    for (auto& [val, count] : m) {
      if (val == 0) {
        n[1] += count;
      } else {
        auto s = to_string(val);
        if (s.size() % 2 == 0) {
          n[stoll(s.substr(0, s.size() / 2))] += count;
          n[stoll(s.substr(s.size() / 2))] += count;
        } else {
          n[val * 2024] += count;
        }
      }
    }
    swap(m, n);
  }

  i64 result = ranges::fold_left(m | views::values, 0, plus<i64>());
  cout << format("part {} result = {}\n", part, result);
}

}  // namespace

void day11() {
  run(1, 25);
  run(2, 75);
}
