#include "2024_utils.h"

namespace {

void part1() {
  auto input = load_strings("day01_input.txt");
  vector<i64> v1, v2;
  for (auto& line: input) {
    auto vals = split(line, " ");
    v1.push_back(stol(vals[0]));
    v2.push_back(stol(vals[1]));
  }
  ranges::sort(v1);
  ranges::sort(v2);
  i64 result = 0;
  for (int i = 0; i < input.size(); ++i) {
    result += abs(v1[i] - v2[i]);
  }
  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day01_input.txt");
  vector<i64> v1, v2;
  for (auto& line: input) {
    auto vals = split(line, " ");
    v1.push_back(stol(vals[0]));
    v2.push_back(stol(vals[1]));
  }
  ranges::sort(v1);
  ranges::sort(v2);
  i64 result = 0;
  for (auto& v : v1) {
    auto [b, e] = ranges::equal_range(v2, v);
    result += v * distance(b, e);
  }
  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day01() {
  part1();
  part2();
}
