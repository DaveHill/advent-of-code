#include "2024_utils.h"
#include <queue>
#include <cmath>
#include <Eigen/Dense>

namespace {

void part1() {
  auto in = load_strings("day13_input.txt");
  i64 result = 0;

  Vector2i a;
  Vector2i b;
  Vector2i prize;

  struct State {
    Vector2i pos;
    i64 cost = 0;
    i64 a_push = 0;
    i64 b_push = 0;
  };

  for (auto& line: in) {
    if (sscanf_s(line.c_str(), "Button A: X+%d, Y+%d", &a[0], &a[1]) == 2) continue;
    if (sscanf_s(line.c_str(), "Button B: X+%d, Y+%d", &b[0], &b[1]) == 2) continue;
    if (sscanf_s(line.c_str(), "Prize: X=%d, Y=%d", &prize[0], &prize[1]) != 2) {
      continue;
    }

    // Process it.
    auto cmp = [&](const State& l, const State& r){ 
      return (l.pos - prize).cwiseAbs().sum() < (r.pos - prize).cwiseAbs().sum();
    };
    priority_queue<State, vector<State>, decltype(cmp)> q(cmp);
    q.push({.pos = {0, 0}});
    map<Vector2i, i64> pos_cheapest;
    i64 cheapest = std::numeric_limits<i64>::max();

    while (!q.empty()) {
      State s = q.top();
      q.pop();
      
      if (s.cost > cheapest) continue;
      if (s.a_push > 100 || s.b_push > 100) continue;
      if ((s.pos.array() > prize.array()).any()) continue;

      i64 dist = (s.pos - prize).cwiseAbs().sum();
      if (dist == 0) {
        cheapest = std::min(cheapest, s.cost);
      }

      auto prev = pos_cheapest.find(s.pos);
      if (prev != pos_cheapest.end() && prev->second <= s.cost) {
        continue;
      }
      pos_cheapest[s.pos] = s.cost;

      q.push({ .pos = s.pos + a, .cost = s.cost + 3, .a_push = s.a_push + 1, .b_push = s.b_push });
      q.push({ .pos = s.pos + b, .cost = s.cost + 1, .a_push = s.a_push, .b_push = s.b_push + 1 });
    }

    if (cheapest < std::numeric_limits<i64>::max()) {
      result += cheapest;
    }
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto in = load_strings("day13_input.txt");
  i64 result = 0;

  using Vector2l = Eigen::Matrix<long long, 2, 1>;

  Vector2l a;
  Vector2l b;
  Vector2l prize;

  for (auto& line: in) {
    if (sscanf_s(line.c_str(), "Button A: X+%lld, Y+%lld", &a[0], &a[1]) == 2) continue;
    if (sscanf_s(line.c_str(), "Button B: X+%lld, Y+%lld", &b[0], &b[1]) == 2) continue;
    if (sscanf_s(line.c_str(), "Prize: X=%lld, Y=%lld", &prize[0], &prize[1]) != 2) {
      continue;
    }

    prize += Vector2l(10000000000000, 10000000000000);

    i64 a_num = b.y()  * prize.x() - b.x() * prize.y();
    i64 a_den = b.y() * a.x() - b.x() * a.y();
    i64 b_num = a.y()  * prize.x() - a.x() * prize.y();
    i64 b_den = a.y() * b.x() - a.x() * b.y();

    if (a_num % a_den == 0 && b_num % b_den == 0) {
      result += 3 * a_num / a_den +  b_num / b_den;
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

void part2a() {
  auto in = load_strings("day13_input.txt");
  i64 result = 0;

  for (auto& line: in) {
    Eigen::Matrix2d m;
    Eigen::Vector2d p;
    if (sscanf_s(line.c_str(), "Button A: X+%lf, Y+%lf", &m(0, 0), &m(1, 0)) == 2) continue;
    if (sscanf_s(line.c_str(), "Button B: X+%lf, Y+%lf", &m(0, 1), &m(1, 1)) == 2) continue;
    if (sscanf_s(line.c_str(), "Prize: X=%lf, Y=%lf", &p[0], &p[1]) != 2) {
      continue;
    }
    p.array() += 10000000000000.0;

    Eigen::Vector2d s = m.colPivHouseholderQr().solve(p);
    if (m * s.unaryExpr([](double d){ return round(d);}) == p) {
      result += 3 * s.x() + s.y();
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day13() {
  part1();
  part2a();
}
