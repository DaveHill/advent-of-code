#include "2024_utils.h"
#include <Eigen/Core>
using Eigen::Vector2i;

namespace {

void part1() {
  auto in = load_strings("day10_input.txt");
  i64 result = 0;

  // start, current
  deque<pair<Vector2i, Vector2i>> states;
  set<tuple<int, int, int, int>> cache;
  
  // Find trail heads.
  for (int r = 0; r < in.size(); ++r) {
    for (int c = 0; c < in[0].size(); ++c) {
      if (in[r][c] == '0') {
        states.emplace_back(Vector2i{c, r}, Vector2i{c, r});
      }
    }
  }

  while (!states.empty()) {
    auto& [start, current] = states.front();
    states.pop_front();
    if (!cache.emplace(start.x(), start.y(), current.x(), current.y()).second) continue;

    if (in[current.y()][current.x()] == '9') {
      ++result;
      continue;
    }

    for (auto& d : vector<Vector2i>{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}) {
      auto next = current + d;
      if (
        (next.array() >= 0).all() &&
        (next.array() < Vector2i{in[0].size(), in.size()}.array()).all() &&
        in[next.y()][next.x()] == in[current.y()][current.x()] + 1) {
          states.emplace_back(start, next);
      }
    }
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto in = load_strings("day10_input.txt");
  i64 result = 0;

  // start, curr
  deque<tuple<Vector2i, Vector2i>> states;
  
  for (int y = 0; y < in.size(); ++y) {
    for (int x = 0; x < in[0].size(); ++x) {
      if (in[y][x] == '0') {
        states.emplace_back(Vector2i{x, y}, Vector2i{x, y});
      }
    }
  }

  while (!states.empty()) {
    auto [start, curr] = states.front();
    states.pop_front();

    if (in[curr.y()][curr.x()] == '9') {
      ++result;
      continue;
    }

    for (auto& d : vector<Vector2i>{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}) {
      auto next = curr + d;
      if (next.y() >= 0 && next.y() < in.size() && next.x() >= 0 && next.x() < in[0].size()
        && in[next.y()][next.x()] == in[curr.y()][curr.x()] + 1) {
        states.emplace_back(start, next);
      }
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day10() {
  part1();
  part2();
}
