#include "2024_utils.h"

namespace {

void part1() {
  auto in = load_strings("dayXX_input.txt");
  i64 result = 0;

  for (auto& line: in) {
    
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {

}

}  // namespace

void dayXX() {
  part1();
  part2();
}
