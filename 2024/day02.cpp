#include "2024_utils.h"

namespace {

i64 sign(i64 v) {
  return (0 < v) - (v < 0);
}

void part1() {
  auto input = load_strings("day02_input.txt");
  i64 result = 0;

  for (auto& line: input) {
    i64 last_diff = 0;
    bool ok = true;
    auto vals = split_int(line, " ");
    for (int i = 1; i < vals.size(); ++i) {
      i64 diff = vals[i] - vals[i - 1];
      ok &= abs(diff) >= 1 && abs(diff) <= 3 && (last_diff == 0 || sign(diff) == sign(last_diff));
      last_diff = diff;
    }

    if (ok) ++result;
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day02_input.txt");
  i64 result = 0;

  for (auto& line: input) {
    i64 last_diff = 0;
    bool ok = true;
    auto vals = split_int(line, " ");

    for (int i = 1; i < vals.size(); ++i) {
      i64 diff = vals[i] - vals[i - 1];
      ok &= abs(diff) >= 1 && abs(diff) <= 3 && (last_diff == 0 || sign(diff) == sign(last_diff));
      last_diff = diff;
    }

    if (ok) {
      ++result;
    } else {
      for (int remove = 0; remove < vals.size(); ++remove) {
        auto v2 = vals;
        v2.erase(v2.begin() + remove);
        last_diff = 0;
        ok = true;
        for (int i = 1; i < v2.size(); ++i) {
          i64 diff = v2[i] - v2[i - 1];
          ok &= abs(diff) >= 1 && abs(diff) <= 3 && (last_diff == 0 || sign(diff) == sign(last_diff));
          last_diff = diff;
        }
        if (ok) {
          ++result;
          break;
        }
      }
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day02() {
  part1();
  part2();
}
