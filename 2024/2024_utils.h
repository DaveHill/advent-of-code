#pragma once

// A bunch of handy includes.
#include <algorithm>
#include <array>
#include <bitset>
#include <cassert>
#include <chrono>
#include <cstdio>
#include <deque>
#include <format>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <utility>
#include <vector>

#include <Eigen/Core>
using Eigen::Vector2i;
using i64 = int64_t;

inline std::string load_string(const char* filename) {
  std::ifstream file_stream(filename);
  assert(file_stream.is_open());
  std::stringstream buffer;
  buffer << file_stream.rdbuf();
  return buffer.str();
}

inline std::vector<int64_t> load_ints(const char* filename,
                                      const char delim = '\n') {
  std::ifstream file_stream{filename};
  assert(file_stream.is_open());
  std::string line;
  std::vector<int64_t> data;
  while (std::getline(file_stream, line, delim)) {
    data.push_back(std::stoull(line));
  }
  return data;
}

inline std::vector<std::string> load_strings(const char* filename) {
  std::ifstream file_stream{filename};
  assert(file_stream.is_open());
  std::string line;
  std::vector<std::string> data;
  while (std::getline(file_stream, line)) {
    data.push_back(line);
  }
  return data;
}

inline std::vector<std::string> split(const std::string& str,
                                      const std::string& delim) {
  size_t start = 0;
  size_t end = str.find(delim);
  std::vector<std::string> result;
  while (end != std::string::npos) {
    auto part = str.substr(start, end - start);
    if (!part.empty()) result.push_back(part);
    start = end + delim.length();
    end = str.find(delim, start);
  }
  auto last = str.substr(start, end);
  if (!last.empty()) result.push_back(last);
  return result;
}

inline std::vector<i64> split_int(const std::string& str, const std::string& delim) {
  std::vector<std::string> v = split(str, delim);
  std::vector<i64> vi;
  vi.reserve(v.size());
  for (auto& s : v) {
    vi.push_back(stol(s));
  }
  return vi;
}

template <typename T>
inline bool ccontains(auto& c, const T& v) {
  return std::find(std::begin(c), std::end(c), v) != std::end(c);
}

inline void csort(auto& c) { std::sort(c.begin(), c.end()); }
inline void rcsort(auto& c) { std::sort(c.rbegin(), c.rend()); }

class Profile {
 public:
  explicit Profile(std::string name) : name_(name) {
    begin_ = std::chrono::steady_clock::now();
  }

  ~Profile() {
    auto end = std::chrono::steady_clock::now();
    printf("%s: %lldus\n", name_.c_str(), (end - begin_).count() / 1'000);
  }

 private:
  std::string name_;
  std::chrono::time_point<std::chrono::steady_clock> begin_;
};

// Spaceship operator for Vector2i.
namespace Eigen{
inline auto operator<=>(const Vector2i& l, const Vector2i& r){
  return std::tie(l.x(), l.y()) <=> std::tie(r.x(), r.y());
}
}

using namespace std;
