#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <bitset>

#include "utils.h"

namespace {
	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day16_input.txt");
		int64_t error_rate = 0;

		std::vector<std::pair<int, int>> valid_ranges;

		bool read_ranges = true;
		bool sum_errors = false;
		for (auto& line : input) {
			if (line == "") {
				read_ranges = false;
				continue;
			}

			if (read_ranges) {
				std::string ranges = line.substr(line.find(":") + 2);
				int lo1, hi1, lo2, hi2;
				sscanf_s(ranges.c_str(), "%d-%d or %d-%d", &lo1, &hi1, &lo2, &hi2);
				valid_ranges.emplace_back(lo1, hi1);
				valid_ranges.emplace_back(lo2, hi2);
			}

			if (starts_with(line, "nearby tickets:")) {
				sum_errors = true;
				continue;
			}

			if (sum_errors) {
				auto values = split(line, ",");
				for (std::string& value_str : values) {
					int value = std::stoi(value_str);

					bool error = true;
					for (auto& [lo, hi] : valid_ranges) {
						if (value >= lo && value <= hi) {
							error = false;
						}
					}

					if (error)
						error_rate += value;
				}
			}
		}

		printf("part 1: %llu\n", error_rate);
	}

	void part2() {
		struct Field {
			std::string name;
			int lo1 = 0;
			int hi1 = 0;
			int lo2 = 0;
			int hi2 = 0;
			std::bitset<20> possible_positions = std::bitset<20>().set();
		};

		std::vector<std::string> input = load_strings("2020/2020_day16_input.txt");

		// Parse fields.
		std::vector<Field> fields;
		bool sum_errors = false;
		for (auto& line : input) {
			if (line == "") {
				break;
			}

			Field f;
			f.name = line.substr(0, line.find(":"));
			std::string ranges = line.substr(line.find(":") + 2);
			sscanf_s(ranges.c_str(), "%d-%d or %d-%d", &f.lo1, &f.hi1, &f.lo2, &f.hi2);
			fields.push_back(f);
		}

		// Read all the tickets, store possible orderings.
		bool read_tickets = false;
		for (auto& line : input) {
			if (starts_with(line, "nearby tickets:")) {
				read_tickets = true;
				continue;
			}

			if (read_tickets) {
				auto values = split(line, ",");
				auto updated_fields = fields;
				bool valid_ticket = true;

				for (int i = 0; i < values.size(); ++i) {
					int value = std::stoi(values[i]);

					// Mark any impossible field locations, discard invalid tickets.
					bool any_matched = false;
					for (auto& field : updated_fields) {
						if ((value < field.lo1 || value > field.hi1)
							&& (value < field.lo2 || value > field.hi2)) {
							field.possible_positions[i] = false;
						}
						else {
							any_matched = true;
						}
					}
					
					if (!any_matched)
						valid_ticket = false;
				}

				if (valid_ticket) {
					fields = updated_fields;
				}
			}
		}

		// Reduce down possible field positions.
		bool stable = false;
		while (!stable) {
			stable = true;
			for (int i = 0; i < fields.size(); ++i) {
				if (fields[i].possible_positions.count() == 1) {
					for (int j = 0; j < fields.size(); ++j) {
						if (i != j) {
							if ((fields[j].possible_positions & fields[i].possible_positions).any()) {
								fields[j].possible_positions &= ~fields[i].possible_positions;
								stable = false;
							}
						}
					}
				}
			}
		}
		
		// Parse my ticket data.
		bool read_my_ticket = false;
		std::vector<int> my_ticket;
		for (auto& line : input) {
			if (starts_with(line, "your ticket")) {
				read_my_ticket = true;
				continue;
			}

			if (read_my_ticket) {
				for (std::string& s : split(line, ",")) {
					my_ticket.push_back(std::stoi(s));
				}
				break;
			}
		}

		int64_t product = 1;
		for (auto& field : fields) {
			if (starts_with(field.name, "departure")) {
				int field_ticket_location = 0;
				while (!field.possible_positions[field_ticket_location])
					field_ticket_location++;
				product *= my_ticket[field_ticket_location];
			}
		}

		printf("part 2: %llu\n", product);
	}
}

namespace y2020 {
	void day16() {
		part1();
		part2();
	}
}
