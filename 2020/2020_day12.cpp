#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>

#include "utils.h"

namespace {
	void turn_left(int& dir_x, int& dir_y) {
		std::swap(dir_x, dir_y);
		dir_x = -dir_x;
	}

	void turn_right(int& dir_x, int& dir_y) {
		std::swap(dir_x, dir_y);
		dir_y = -dir_y;
	}

	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day12_input.txt");
		
		int x = 0;
		int y = 0;

		// Start facing east.
		int dir_x = 1;
		int dir_y = 0;

		for (const auto& line : input) {
			char instruction;
			int distance;
			sscanf_s(line.c_str(), "%c%d", &instruction, 1, &distance);

			switch (instruction) {
			case 'F':
				x += dir_x * distance;
				y += dir_y * distance;
				break;
			case 'N':
				y += distance;
				break;
			case 'S':
				y -= distance;
				break;
			case 'E':
				x += distance;
				break;
			case 'W':
				x -= distance;
				break;
			case 'L':
				for (int i = 0; i < distance / 90; ++i)
					turn_left(dir_x, dir_y);
				break;
			case 'R':
				for (int i = 0; i < distance / 90; ++i) 
					turn_right(dir_x, dir_y);
				break;
			default:
				break;
			}
		}

		printf("part 1: %d\n", std::abs(x) + std::abs(y));
	}

	void part2() {			
		std::vector<std::string> input = load_strings("2020/2020_day12_input.txt");

		int x = 0;
		int y = 0;
		int waypoint_x = 10;
		int waypoint_y = 1;

		for (const auto& line : input) {
			char instruction;
			int distance;
			sscanf_s(line.c_str(), "%c%d", &instruction, 1, &distance);

			switch (instruction) {
			case 'F':
				x += waypoint_x * distance;
				y += waypoint_y * distance;
				break;
			case 'N':
				waypoint_y += distance;
				break;
			case 'S':
				waypoint_y -= distance;
				break;
			case 'E':
				waypoint_x += distance;
				break;
			case 'W':
				waypoint_x -= distance;
				break;
			case 'L':
				for (int i = 0; i < distance / 90; ++i)
					turn_left(waypoint_x, waypoint_y);
				break;
			case 'R':
				for (int i = 0; i < distance / 90; ++i)
					turn_right(waypoint_x, waypoint_y);
				break;
			default:
				break;
			}
		}

		printf("part 2: %d\n", std::abs(x) + std::abs(y));
	}
}

namespace y2020 {
	void day12() {
		part1();
		part2();
	}
}
