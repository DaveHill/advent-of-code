#include <cstdio>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>
#include <set>

namespace {
	std::vector<std::string> load_input(const char* filename) {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<std::string> data;
		while (std::getline(file_stream, line)) {
			data.push_back(line);
		}
		return data;
	}

	void part1() {
		std::vector<std::string> program = load_input("2020/2020_day08_input.txt");
		
		int pc = 0;
		int acc = 0;
		std::set<int> addresses_executed;
		while (addresses_executed.count(pc) == 0) {
			addresses_executed.insert(pc);
			char inst[4];
			int operand;
			sscanf_s(program[pc].c_str(), "%s %d", inst, 4, &operand);

			if (inst == std::string("jmp")) {
				pc += operand;
				continue;
			}
			if (inst == std::string("acc")) {
				acc += operand;
			}
			pc++;
		}

		printf("part 1: %d\n", acc);
	}

	void part2() {
		std::vector<std::string> program_master = load_input("2020/2020_day08_input.txt");

		for (size_t i = 0; i < program_master.size(); ++i) {
			auto program = program_master;
			// Flip a jmp/nop.
			if (program[i].substr(0, 3) == "nop") {
				program[i].replace(0, 3, "jmp");
			}
			else if (program[i].substr(0, 3) == "jmp") {
				program[i].replace(0, 3, "nop");
			}
			else {
				continue;
			}

			size_t pc = 0;
			int acc = 0;
			std::set<int> addresses_executed;
			while (addresses_executed.count(pc) == 0 && pc < program.size()) {
				addresses_executed.insert(pc);
				char inst[4];
				int operand;
				sscanf_s(program[pc].c_str(), "%s %d", inst, 4, &operand);

				if (inst == std::string("jmp")) {
					pc += operand;
					continue;
				}
				if (inst == std::string("acc")) {
					acc += operand;
				}
				pc++;
			}

			if (pc == program.size()) {
				printf("part 2: %d\n", acc);
				return;
			}
		}
	}
}

namespace y2020 {
	void day08() {
		part1();
		part2();
	}
}
