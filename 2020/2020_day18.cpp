#include <algorithm>
#include <cstdio>
#include <vector>
#include <bitset>
#include <functional>
#include <deque>
#include <cassert>

#include "utils.h"

namespace {
	int64_t evaluate(const std::string& equation) {
		int64_t sum = 0;
		std::function<int64_t(int64_t, int64_t)> next_op = std::plus<int64_t>{};
		for (size_t i = 0; i < equation.size(); ++i) {
			const char c = equation[i];

			if (c >= '0' && c <= '9') {
				sum = next_op(sum, c - '0');
			}

			if (c == '*') {
				next_op = std::multiplies<int64_t>{};
			}

			if (c == '+') {
				next_op = std::plus<int64_t>{};
			}

			if (c == '(') {
				// Find matching paren.
				size_t closing_paren = i;
				int num_parens = 1;
				while (num_parens > 0) {
					++closing_paren;
					if (equation[closing_paren] == ')')
						--num_parens;
					if (equation[closing_paren] == '(')
						++num_parens;
				}

				sum = next_op(sum, evaluate(equation.substr(i + 1, closing_paren - i - 1)));
				i = closing_paren + 1;
			}

			if (c == ')') {
				return sum;
			}
		}

		return sum;
	}

	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day18_input.txt");

		int64_t sum = 0;
		for (const auto& line : input) {
			sum += evaluate(line);
		}

		printf("part 1: %lld\n", sum);
	}

	enum class Op {
		Plus,
		Multiply,
	};

	int64_t evaluate2(const std::string& equation) {
		std::deque<Op> operator_stack;
		std::deque<std::function<int64_t()>> operand_stack;

		for (size_t i = 0; i < equation.size(); ++i) {
			const char c = equation[i];

			if (c >= '0' && c <= '9') {
				operand_stack.push_back([c] { return c - '0'; });
			}

			if (c == '*') {
				// Make trees for lower presidence.
				while (!operator_stack.empty() && operator_stack.back() == Op::Plus) {
					auto lhs = operand_stack.back();
					operand_stack.pop_back();
					auto rhs = operand_stack.back();
					operand_stack.pop_back();

					Op op = operator_stack.back();
					operator_stack.pop_back();

					if (op == Op::Plus)
						operand_stack.push_back([=] { return lhs() + rhs(); });
					if (op == Op::Multiply)
						operand_stack.push_back([=] { return lhs() * rhs(); });
				}

				operator_stack.push_back(Op::Multiply);
			}

			if (c == '+') {
				operator_stack.push_back(Op::Plus);
			}

			if (c == '(') {
				// Find matching paren.
				size_t closing_paren = i;
				int num_parens = 1;
				while (num_parens > 0) {
					++closing_paren;
					if (equation[closing_paren] == ')')
						--num_parens;
					if (equation[closing_paren] == '(')
						++num_parens;
				}

				// Evaluate recursively.
				operand_stack.push_back(
					[=] { return evaluate2(equation.substr(i + 1, closing_paren - i - 1)); }
				);

				// Skip ahead.
				i = closing_paren;
			}
		}

		while (!operator_stack.empty()) {
			auto lhs = operand_stack.back();
			operand_stack.pop_back();
			auto rhs = operand_stack.back();
			operand_stack.pop_back();

			Op op = operator_stack.back();
			operator_stack.pop_back();

			if (op == Op::Plus) operand_stack.push_back([=] { return lhs() + rhs(); });
			if (op == Op::Multiply) operand_stack.push_back([=] { return lhs() * rhs(); });
		}

		return operand_stack.front()();
	}

	void part2() {
		std::vector<std::string> input = load_strings("2020/2020_day18_input.txt");

		int64_t sum = 0;
		for (const auto& line : input) {
			sum += evaluate2(line);
		}

		assert(sum == 388966573054664);
		printf("part 2: %lld\n", sum);
	}
}

namespace y2020 {
	void day18() {
		part1();
		part2();
	}
}
