#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <bitset>

#include "utils.h"

namespace {
	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day14_input.txt");
		std::bitset<36> one_mask = 0;
		std::bitset<36> zero_mask = 0;
		zero_mask.flip();

		std::unordered_map<uint64_t, uint64_t> memory;
		for (auto& line : input) {
			if (starts_with(line, "mask")) {
				std::string mask = line.substr(7);

				one_mask = 0;
				zero_mask = 0;
				zero_mask.flip();

				for (int i = 0; i < 36; ++i) {
					if (mask[36 - i - 1] == '0')
						zero_mask[i] = 0;
					if (mask[36 - i - 1] == '1')
						one_mask[i] = 1;
				}

				continue;
			}

			uint64_t address;
			uint64_t value;
			sscanf_s(line.c_str(), "mem[%llu] = %llu", &address, &value);

			memory[address] = (value & zero_mask.to_ullong()) | one_mask.to_ullong();
		}
		
		uint64_t sum = 0;
		for (auto& [address, value] : memory) {
			sum += value;
		}

		printf("part 1: %llu\n", sum);
	}

	void part2() {
		std::vector<std::string> input = load_strings("2020/2020_day14_input.txt");
		std::vector<std::bitset<36>> one_masks;
		std::vector<std::bitset<36>> zero_masks;
		std::unordered_map<uint64_t, uint64_t> memory;

		for (auto& line : input) {
			if (starts_with(line, "mask")) {
				one_masks.assign({ 0 });
				zero_masks.assign({ 0 });
				zero_masks[0].flip();

				std::string mask = line.substr(7);

				// Generate 1s.
				for (int bit = 0; bit < 36; ++bit) {
					if (mask[36 - bit - 1] == '1')
						one_masks[0][bit] = 1;
				}

				// Handle Xs.
				for (int bit = 0; bit < 36; ++bit) {
					if (mask[36 - bit - 1] == 'X') {
						// Double number of masks, half of which have a masked 0, half a 1.
						size_t old_size = one_masks.size();
						for (int mask_id = 0; mask_id < old_size; ++mask_id) {
							one_masks.push_back(one_masks[mask_id]);
							one_masks[mask_id][bit] = 1;

							zero_masks.push_back(zero_masks[mask_id]);
							zero_masks.back()[bit] = 0;
						}
					}
				}

				continue;
			}

			uint64_t address;
			uint64_t value;
			sscanf_s(line.c_str(), "mem[%llu] = %llu", &address, &value);

			for (size_t i = 0; i < one_masks.size(); ++i) {
				auto& one_mask = one_masks[i];
				auto& zero_mask = zero_masks[i];
				uint64_t masked_address = address & zero_mask.to_ullong() | one_mask.to_ullong();
				memory[masked_address] = value;
			}
		}

		uint64_t sum = 0;
		for (auto& [address, value] : memory) {
			sum += value;
		}

		printf("part 2: %llu\n", sum);
	}
}

namespace y2020 {
	void day14() {
		part1();
		part2();
	}
}
