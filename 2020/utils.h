#include <vector>
#include <string>
#include <fstream>

inline std::vector<int64_t> load_ints(const char* filename) {
	std::ifstream file_stream{ filename };
	std::string line;
	std::vector<int64_t> data;
	while (std::getline(file_stream, line, ',')) {
		data.push_back(std::stoull(line));
	}
	return data;
}

inline std::vector<std::string> load_strings(const char* filename) {
	std::ifstream file_stream{ filename };
	std::string line;
	std::vector<std::string> data;
	while (std::getline(file_stream, line)) {
		data.push_back(line);
	}
	return data;
}

inline std::vector<std::string> split(const std::string& str, const std::string& delim) {
	size_t start = 0;
	size_t end = str.find(delim);
	std::vector<std::string> result;
	while (end != std::string::npos) {
		result.push_back(str.substr(start, end - start));
		start = end + delim.length();
		end = str.find(delim, start);
	}
	result.push_back(str.substr(start, end));
	return result;
}

inline bool starts_with(const std::string& haystack, const std::string& needle) {
	return haystack.find(needle) == 0;
}

inline bool contains(const std::string& haystack, const std::string& needle) {
	return haystack.find(needle) != std::string::npos;
}