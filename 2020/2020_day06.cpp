#include <cstdio>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <set>
#include <iterator>

namespace {
	std::vector<std::string> load_input(const char* filename) {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<std::string> data;
		while (std::getline(file_stream, line)) {
			data.push_back(line);
		}
		return data;
	}

	void part1() {
		std::vector<std::string> input = load_input("2020/2020_day06_input.txt");

		std::set<char> answered;
		int total = 0;
		for (const std::string& line : input) {
			if (line == "") {
				total += answered.size();
				answered.clear();
			} else {
				std::copy(line.begin(), line.end(), std::inserter(answered, answered.begin()));
			}
		}

		total += answered.size();
		printf("part 1: %d\n", total);
	}

	void part2() {
		std::vector<std::string> input = load_input("2020/2020_day06_input.txt");

		std::set<char> intersection;
		int total = 0;
		bool first = true;
		for (const std::string& line : input) {
			if (line == "") {
				total += intersection.size();
				intersection.clear();
				first = true;
			}
			else {
				std::set<char> answered;
				std::copy(line.begin(), line.end(), std::inserter(answered, answered.begin()));
				if (first) {
					intersection = answered;
					first = false;
				} else {
					std::set<char> intersection2;
					std::set_intersection(begin(answered), end(answered),
						begin(intersection), end(intersection), inserter(intersection2, intersection2.begin()));
					std::swap(intersection, intersection2);
				}
			}
		}

		total += intersection.size();
		printf("part 2: %d\n", total);
	}
}

namespace y2020 {
	void day06() {
		part1();
		part2();
	}
}
