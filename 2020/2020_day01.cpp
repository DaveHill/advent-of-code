#include <vector>
#include <fstream>
#include <cstdio>
#include <string>

namespace {
	std::vector<int> load_data() {
		std::vector<int> entries;
		std::ifstream fs{ "2020/2020_day01_input.txt" };
		for (std::string line; std::getline(fs, line);) {
			entries.push_back(std::atoi(line.c_str()));
		}
		return entries;
	}

	void part1() {
		auto entries = load_data();

		for (size_t i = 0; i < entries.size(); ++i) {
			for (size_t j = i + 1; j < entries.size(); ++j) {
				if (entries[i] + entries[j] == 2020) {
					printf("part 1: %d\n", entries[i] * entries[j]);
					return;
				}
			}
		}
	}

	void part2() {
		auto entries = load_data();

		for (size_t i = 0; i < entries.size(); ++i) {
			for (size_t j = i + 1; j < entries.size(); ++j) {
				for (size_t k = j + 1; k < entries.size(); ++k) {
					if (entries[i] + entries[j] + entries[k] == 2020) {
						printf("part 2: %d\n", entries[i] * entries[j] * entries[k]);
						return;
					}
				}
			}
		}
	}
}

namespace y2020 {
	void day01() {
		part1();
		part2();
	}
}
