#include <algorithm>
#include <cstdio>
#include <vector>
#include <deque>
#include <bitset>
#include <algorithm>

#include "utils.h"

namespace {
	constexpr int kGridEdge = 12; // sample: 3
	constexpr int kNumTiles = kGridEdge * kGridEdge;
	constexpr int kTileEdge = 10;

	struct Tile {
		int64_t id;
		char data[kTileEdge * kTileEdge];

		char get(int x, int y) const {
			return data[y * 10 + x];
		}

		void set(int x, int y, char c) {
			data[y * 10 + x] = c;
		}
	};

	struct PlacedTile {
		int tile_idx = -1;
		bool mirrored = false;
		int rotations = 0; // Num clockwise rotations.
	};

	struct Grid {
		PlacedTile tiles[kNumTiles];
		std::bitset<kNumTiles> tiles_used;
	};

	PlacedTile next_permutation(const PlacedTile& tile) {
		PlacedTile new_tile = tile;
		if (new_tile.rotations < 3) {
			++new_tile.rotations;
		}
		else {
			new_tile.rotations = 0;
			new_tile.mirrored = !new_tile.mirrored;
		}
		return new_tile;
	}

	bool valid_neighbour(const std::vector<Tile>& tiles, 
			const PlacedTile& tile1, const PlacedTile& tile2, bool check_below) {
		const int kMaxIndex = kTileEdge - 1;

		int t1_x = 0;
		int t1_x_dir = 0;
		int t1_y = 0;
		int t1_y_dir = 0;
		int t2_x = 0;
		int t2_x_dir = 0;
		int t2_y = 0;
		int t2_y_dir = 0;

		if (!check_below) {
			// Tile 1 to the left, tile 2 to the right.
			t1_x = kMaxIndex;
			t1_y_dir= 1;
			t2_y_dir= 1;
		}
		else {
			// Tile 1 above, tile 2 below.
			t1_y = kMaxIndex;
			t1_x_dir = 1;
			t2_x_dir = 1;
		}
	
		// Mirror.
		if (tile1.mirrored) {
			t1_x = kMaxIndex - t1_x;
			t1_x_dir = -t1_x_dir;
		}

		if (tile2.mirrored) {
			t2_x = kMaxIndex - t2_x;
			t2_x_dir = -t2_x_dir;
		}

		// Rotate.
		for (int i = 0; i < tile1.rotations; ++i) {
			std::swap(t1_x, t1_y);
			t1_y = kMaxIndex - t1_y;

			std::swap(t1_x_dir, t1_y_dir);
			t1_y_dir = -t1_y_dir;
		}

		for (int i = 0; i < tile2.rotations; ++i) {
			std::swap(t2_x, t2_y);
			t2_y = kMaxIndex - t2_y;

			std::swap(t2_x_dir, t2_y_dir);
			t2_y_dir = -t2_y_dir;
		}

		const Tile& t1 = tiles[tile1.tile_idx];
		const Tile& t2 = tiles[tile2.tile_idx];

		// Check edge equality.
		for (int i = 0; i < 10; ++i) {
			if (t1.get(t1_x, t1_y) != t2.get(t2_x, t2_y)) {
				return false;
			}

			t1_x += t1_x_dir;
			t1_y += t1_y_dir;
			t2_x += t2_x_dir;
			t2_y += t2_y_dir;
		}

		return true;
	}

	std::vector<Tile> load_input() {
		std::vector<std::string> input = load_strings("2020/2020_day20_input.txt");

		// Parse input.
		std::vector<Tile> tiles;
		for (size_t i = 0; i < input.size(); ++i) {
			if (contains(input[i], "Tile")) {
				tiles.push_back({});
				sscanf_s(input[i].c_str(), "Tile %lld", &tiles.back().id);

				for (int y = 0; y < 10; ++y) {
					for (int x = 0; x < 10; ++x) {
						tiles.back().set(x, y, input[i + y + 1][x]);
					}
				}
				i += 10;
			}
		}

		return tiles;
	}

	Grid stitch_grid(const std::vector<Tile>& tiles) {
		// DFS search through all grids.
		std::deque<Grid> grid_stack;

		// Push all valid first states - each tile, in the first position,
		// in every permutation.
		for (int i = 0; i < kNumTiles; ++i) {
			PlacedTile tile;
			tile.tile_idx = i;
			for (int permutation = 0; permutation < 8; ++permutation) {
				Grid grid;
				grid.tiles[0] = tile;
				grid.tiles_used[i] = true;
				grid_stack.push_front(grid);
				tile = next_permutation(tile);
			}
		}

		while (!grid_stack.empty() && grid_stack.back().tiles_used.count() < kNumTiles) {
			// Pop element, push all valid next states.
			Grid current_grid = grid_stack.back();
			grid_stack.pop_back();

			// Try to place the next tile.
			for (int tile_idx = 0; tile_idx < kNumTiles; ++tile_idx) {
				if (current_grid.tiles_used[tile_idx]) {
					continue;
				}
				size_t num_tiles_placed = current_grid.tiles_used.count();

				PlacedTile next_tile;
				next_tile.tile_idx = tile_idx;

				bool found_valid_tile = false;
				for (int permutation = 0; permutation < 8 && !found_valid_tile; ++permutation) {
					// Check connecting to the right.
					found_valid_tile = true;
					if (num_tiles_placed % kGridEdge != 0) {
						if (!valid_neighbour(tiles, current_grid.tiles[num_tiles_placed - 1], next_tile, false)) {
							found_valid_tile = false;
						}
					}

					// Check connecting below.
					if (num_tiles_placed >= kGridEdge) {
						if (!valid_neighbour(tiles, current_grid.tiles[num_tiles_placed - kGridEdge], next_tile, true)) {
							found_valid_tile = false;
						}
					}

					// Push grid.
					if (found_valid_tile) {
						Grid next_grid = current_grid;
						next_grid.tiles[num_tiles_placed] = next_tile;
						next_grid.tiles_used[tile_idx] = true;
						grid_stack.push_back(next_grid);
						break;
					}

					next_tile = next_permutation(next_tile);
				}
			}
		}

		return grid_stack.back();
	}

	void part1() {
		const auto& tiles = load_input();
		const auto& winning_tiles = stitch_grid(tiles).tiles;
		// Multiply IDs from 4 corners.
		const int64_t result = tiles[winning_tiles[0].tile_idx].id
			* tiles[winning_tiles[kGridEdge - 1].tile_idx].id
			* tiles[winning_tiles[kNumTiles - kGridEdge].tile_idx].id
			* tiles[winning_tiles[kNumTiles - 1].tile_idx].id;
		printf("part 1: %lld\n", result);
	}

	void part2() {
		const auto& tiles = load_input();
		const Grid winning_grid = stitch_grid(tiles);

		std::vector<std::string> image(kGridEdge * 8, std::string(kGridEdge * 8, '.'));
		constexpr int kMaxIndex = kTileEdge - 1;

		// Stitch tiles together into one image.
		for (int ty = 0; ty < kGridEdge; ++ty) {
			int base_row = ty * 8;
			for (int tx = 0; tx < kGridEdge; ++tx) {
				int base_col = tx * 8;

				const PlacedTile& placed_tile = winning_grid.tiles[ty * kGridEdge + tx];
				const Tile& tile = tiles[placed_tile.tile_idx];

				for (int row = 0; row < 8; ++row) {
					for (int col = 0; col < 8; ++col) {
						char& output = image[base_row + row][base_col + col];
						
						int input_x = col + 1;
						int input_y = row + 1;

						// Mirror.
						if (placed_tile.mirrored) {
							input_x = kMaxIndex - input_x;
						}

						// Rotate.
						for (int i = 0; i < placed_tile.rotations; ++i) {
							std::swap(input_x, input_y);
							input_y = kMaxIndex - input_y;
						}

						output = tile.get(input_x, input_y);
					}
				}
			}
		}

		std::vector<std::string> kSeaMonster = {
			"                  # ",
			"#    ##    ##    ###",
			" #  #  #  #  #  #   "
		};
		constexpr int kNumSeaMonsterPixels = 15;

		int sea_monster_pixels = 0;

		// Search image for sea monsters with sliding window.
		int num_sea_monsters = 0;
		while (num_sea_monsters == 0) {
			for (int start_y = 0; start_y < image.size() - kSeaMonster.size() + 1; ++start_y) {
				for (int start_x = 0; start_x < image[0].length() - kSeaMonster[0].length() + 1; ++start_x) {
					bool match = true;
					for (int y = 0; y < kSeaMonster.size(); ++y) {
						for (int x = 0; x < kSeaMonster[0].length(); ++x) {
							if (kSeaMonster[y][x] == '#' && image[start_y + y][start_x + x] != '#') {
								match = false;
							}
						}
					}

					if (match) {
						++num_sea_monsters;
					}
				}
			}

			// Rotate the image (turns out we don't need mirroring on this input).
			std::vector<std::string> new_image = image;
			for (int y = 0; y < image.size(); ++y) {
				for (int x = 0; x < image[0].size(); ++x) {
					new_image[y][x] = image[x][image.size() - y - 1];
				}
			}
			image = new_image;
		}

		//for (auto& row : image) {
		//	printf("%s\n", row.c_str());
		//}

		// Count total # in image, called the 'roughness' in this puzzle.
		int total_roughness = 0;
		for (const auto& row : image) {
			for (char c : row) {
				if (c == '#') {
					++total_roughness;
				}
			}
		}

		printf("part 2: %d\n", total_roughness - num_sea_monsters * kNumSeaMonsterPixels);
	}
}

namespace y2020 {
	void day20() {
		part1();
		part2();
	}
}
