#include <cstdio>
#include <vector>
#include "utils.h"

namespace {
	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day13_input.txt");

		int begin_wait_time = std::stoi(input[0]);
		int min_wait = std::numeric_limits<int>::max();
		int best_bus = 0;
		for (const auto& bus_id_str : split(input[1], ",")) {
			if (bus_id_str != "x") {
				int bus_period = std::stoi(bus_id_str);
				int wait = bus_period - begin_wait_time % bus_period;
				if (wait < min_wait) {
					min_wait = wait;
					best_bus = bus_period;
				}
			}
		}
		printf("part 1: %d\n", best_bus * min_wait);
	}

	void part2() {
		std::vector<std::string> input = load_strings("2020/2020_day13_input.txt");

		int64_t offset = 0;
		int64_t departure_time = 0;
		int64_t step = 1;
		for (auto& bus_str : split(input[1], ",")) {
			if (bus_str != "x") {
				int64_t bus_period = std::stoi(bus_str);
				while ((departure_time + offset) % bus_period != 0)
					departure_time += step;

				// The bus periods are all prime, so can accumulate step with period.
				step *= bus_period;
			}
			++offset;
		}
		printf("part 2: %lld\n", departure_time);
	}
}

namespace y2020 {
	void day13() {
		part1();
		part2();
	}
}
