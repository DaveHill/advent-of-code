#include <fstream>
#include <string>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cassert>

namespace {
	void part1() {
		std::ifstream fs{ "2020/2020_day03_input.txt" };
		int num_trees = 0;
		int position = 0;
		for (std::string line; std::getline(fs, line);) {
			if (line[position % line.length()] == '#')
				++num_trees;
			position += 3;
		}
		printf("part 1: %d\n", num_trees);
	}

	void part2() {
		int64_t product = 1;
		for (int step = 1; step <= 7; step += 2) {
			std::ifstream fs{ "2020/2020_day03_input.txt" };
			int position = 0;
			int num_trees = 0;
			for (std::string line; std::getline(fs, line);) {
				if (line[position % line.length()] == '#')
					++num_trees;
				position += step;
			}
			product *= num_trees;
		}
		{
			std::ifstream fs{ "2020/2020_day03_input.txt" };
			int position = 0;
			int num_trees = 0;
			bool skip_line = false;
			for (std::string line; std::getline(fs, line);) {
				if (!skip_line) {
					if (line[position % line.length()] == '#')
						++num_trees;
					position++;
				}
				skip_line = !skip_line;
			}
			product *= num_trees;
		}
		printf("part 2: %llu\n", product);
	}
}

namespace y2020 {
	void day03() {
		part1();
		part2();
	}
}
