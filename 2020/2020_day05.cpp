#include <cstdio>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

namespace {
	std::vector<std::string> load_input(const char* filename) {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<std::string> data;
		while (std::getline(file_stream, line)) {
			data.push_back(line);
		}
		return data;
	}

	int calculate_seat_id(const std::string& str) {
		int bit = 64;
		int row = 0;
		for (int i = 0; i < 7; ++i) {
			if (str[i] == 'B')
				row += bit;
			bit /= 2;
		}

		std::string col_str = str.substr(7, 3);
		int col = 0;
		if (col_str == "LLL") col = 0;
		if (col_str == "LLR") col = 1;
		if (col_str == "LRL") col = 2;
		if (col_str == "LRR") col = 3;
		if (col_str == "RLL") col = 4;
		if (col_str == "RLR") col = 5;
		if (col_str == "RRL") col = 6;
		if (col_str == "RRR") col = 7;

		return row * 8 + col;
	}

	void part1() {
		std::vector<std::string> input = load_input("2020/2020_day05_input.txt");

		int result = 0;
		for (const std::string& line : input) {
			result = std::max(result, calculate_seat_id(line));
		}

		printf("part 1: %d\n", result);
	}

	void part2() {
		std::vector<std::string> input = load_input("2020/2020_day05_input.txt");

		int result = 0;
		std::vector<int> seat_ids;
		for (const std::string& line : input) {
			seat_ids.push_back(calculate_seat_id(line));
		}

		// Find the missing seat.
		std::sort(seat_ids.begin(), seat_ids.end());

		for (int i = 1; i < seat_ids.size(); ++i) {
			if (seat_ids[i] != seat_ids[i - 1] + 1)
				printf("part 2: %d\n", seat_ids[i] - 1);
		}
	}
}

namespace y2020 {
	void day05() {
		part1();
		part2();
	}
}
