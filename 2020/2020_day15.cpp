#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <bitset>

#include "utils.h"

namespace {
	void part1() {
		std::vector<int64_t> input{ 15,5,1,4,7,0 };

		std::unordered_map<int64_t, int64_t> last_spoken;
		int64_t last_number = input[0];
		for (int64_t i = 1; i < 2020; ++i) {
			int64_t next_number = 0;
			if (i < input.size())
				next_number = input[i];
			else {
				next_number = last_spoken.count(last_number) == 0 ? 0 : i - last_spoken[last_number] - 1;
			}
			last_spoken[last_number] = i - 1;
			last_number = next_number;
		}

		printf("part 1: %llu\n", last_number);
	}

	void part2() {
		std::vector<int> input{ 15,5,1,4,7,0 };

		std::chrono::time_point begin = std::chrono::high_resolution_clock::now();

		std::unordered_map<int, int> last_spoken;
		int last_number = input[0];
		for (int i = 1; i < input.size(); ++i) {
			int next_number = 0;
			next_number = input[i];
			last_spoken[last_number] = i - 1;
			last_number = next_number;
		}

		for (int i = input.size(); i < 30000000; ++i) {
			int next_number = 0;
			auto last_count = last_spoken.find(last_number);
			next_number = last_count == end(last_spoken) ? 0 : i - last_count->second - 1;
			last_spoken[last_number] = i - 1;
			last_number = next_number;
		}

		std::chrono::duration<float> duration = std::chrono::high_resolution_clock::now() - begin;

		printf("part 2: %d (%0.2fs)\n", last_number, duration.count());
	}
}

namespace y2020 {
	void day15() {
		part1();
		part2();
	}
}
