#include <fstream>
#include <string>
#include <cstdio>
#include <algorithm>

namespace {
	void part1() {
		int num_valid = 0;
		std::ifstream fs{ "2020/2020_day02_input.txt" };
		for (std::string line; std::getline(fs, line);) {
			int min;
			int max;
			char char_policy;
			char password[64];
			sscanf_s(line.c_str(), "%d-%d %c: %63s", &min, &max, &char_policy, 1, password, 64);

			auto char_count = std::count(std::begin(password), std::end(password), char_policy);

			if (char_count >= min && char_count <= max) {
				num_valid++;
			}
		}
		printf("part 1: num valid passwords = %d\n", num_valid);
	}

	void part2() {
		int num_valid = 0;
		std::ifstream fs{ "2020/2020_day02_input.txt" };
		for (std::string line; std::getline(fs, line);) {
			int first;
			int second;
			char char_policy;
			char password[64];
			sscanf_s(line.c_str(), "%d-%d %c: %63s", &first, &second, &char_policy, 1, password, 64);

			if ((password[first - 1] == char_policy) != (password[second - 1] == char_policy)) {
				num_valid++;
			}
		}
		printf("part 2: num valid passwords = %d\n", num_valid);
	}
}

namespace y2020 {
	void day02() {
		part1();
		part2();
	}
}
