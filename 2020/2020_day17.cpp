#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <bitset>
#include <array>

#include "utils.h"

namespace {
	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day17_input.txt");
		constexpr size_t kGridSize = 24;
		using Grid = std::array<std::array<std::array<char, kGridSize>, kGridSize>, kGridSize>;
		Grid grid{ '.' };

		for (int z = 0; z < kGridSize; ++z) {
			for (int y = 0; y < kGridSize; ++y) {
				for (int x = 0; x < kGridSize; ++x) {
					grid[z][y][x] = '.';
				}
			}

			// Insert the input data into the middle of the world.
			if (z == kGridSize / 2) {
				int y = kGridSize / 2 - input.size() / 2;
				for (auto& row : input) {
					int x = kGridSize / 2 - row.size() / 2;
					for (auto c : row) {
						grid[z][y][x++] = c;
					}
					++y;
				}
			}
		}

		// Run the simulation.
		for (int i = 0; i < 6; ++i) {
			Grid updated_grid = grid;

			for (int z = 1; z < kGridSize - 1; ++z) {
				for (int y = 1; y < kGridSize - 1; ++y) {
					for (int x = 1; x < kGridSize - 1; ++x) {
						int num_active_neighbours = 0;
						for (int dz = -1; dz <= 1; ++dz) {
							for (int dy = -1; dy <= 1; ++dy) {
								for (int dx = -1; dx <= 1; ++dx) {
									if (dx != 0 || dy != 0 || dz != 0) {
										if (grid[z + dz][y + dy][x + dx] == '#') {
											++num_active_neighbours;
										}
									}
								}
							}
						}

						bool cube_active = grid[z][y][x] == '#';
						if (cube_active && num_active_neighbours != 2 && num_active_neighbours != 3) {
							updated_grid[z][y][x] = '.';
						}

						if (!cube_active && num_active_neighbours == 3) {
							updated_grid[z][y][x] = '#';
						}
					}
				}
			}
			grid = updated_grid;
		}

		int num_active = 0;
		for (const auto& layer : grid) {
			for (const auto& row : layer) {
				for (const char cube : row) {
					if (cube == '#')
						++num_active;
				}
			}
		}

		printf("part 1: %d\n", num_active);
	}

	void part2() {
		std::vector<std::string> input = load_strings("2020/2020_day17_input.txt");
		constexpr size_t kGridSize = 24;
		using Grid = std::array<std::array<std::array<std::array<char, kGridSize>, kGridSize>, kGridSize>, kGridSize>;
		Grid grid{ '.' };

		for (int w = 0; w < kGridSize; ++w) {
			for (int z = 0; z < kGridSize; ++z) {
				for (int y = 0; y < kGridSize; ++y) {
					for (int x = 0; x < kGridSize; ++x) {
						grid[w][z][y][x] = '.';
					}
				}

				// Insert the input data into the middle of the world.
				if (w == kGridSize / 2 && z == kGridSize / 2) {
					int y = kGridSize / 2 - input.size() / 2;
					for (auto& row : input) {
						int x = kGridSize / 2 - row.size() / 2;
						for (auto c : row) {
							grid[w][z][y][x++] = c;
						}
						++y;
					}
				}
			}
		}

		// Run the simulation.
		for (int i = 0; i < 6; ++i) {
			Grid updated_grid = grid;
			for (int w = 1; w < kGridSize - 1; ++w) {
				for (int z = 1; z < kGridSize - 1; ++z) {
					for (int y = 1; y < kGridSize - 1; ++y) {
						for (int x = 1; x < kGridSize - 1; ++x) {
							int num_active_neighbours = 0;
							for (int dw = -1; dw <= 1; ++dw) {
								for (int dz = -1; dz <= 1; ++dz) {
									for (int dy = -1; dy <= 1; ++dy) {
										for (int dx = -1; dx <= 1; ++dx) {
											if (dx != 0 || dy != 0 || dz != 0 || dw != 0) {
												if (grid[w + dw][z + dz][y + dy][x + dx] == '#') {
													++num_active_neighbours;
												}
											}
										}
									}
								}
							}

							bool cube_active = grid[w][z][y][x] == '#';
							if (cube_active && num_active_neighbours != 2 && num_active_neighbours != 3) {
								updated_grid[w][z][y][x] = '.';
							}

							if (!cube_active && num_active_neighbours == 3) {
								updated_grid[w][z][y][x] = '#';
							}
						}
					}
				}
			}
			grid = updated_grid;
		}

		int num_active = 0;
		for (const auto& super_layer : grid) {
			for (const auto& layer : super_layer) {
				for (const auto& row : layer) {
					for (const char cube : row) {
						if (cube == '#')
							++num_active;
					}
				}
			}
		}

		printf("part 2: %d\n", num_active);
	}
}

namespace y2020 {
	void day17() {
		part1();
		part2();
	}
}
