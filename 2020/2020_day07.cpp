#include <cstdio>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>
#include <set>

namespace {
	std::vector<std::string> load_input(const char* filename) {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<std::string> data;
		while (std::getline(file_stream, line)) {
			data.push_back(line);
		}
		return data;
	}

	std::vector<std::string> split(const std::string& str, const std::string& delim) {
		size_t start = 0;
		size_t end = str.find(delim);
		std::vector<std::string> result;
		while (end != std::string::npos) {
			result.push_back(str.substr(start, end - start));
			start = end + delim.length();
			end = str.find(delim, start);
		}
		result.push_back(str.substr(start, end));
		return result;
	}

	void part1() {
		std::vector<std::string> input = load_input("2020/2020_day07_input.txt");
		std::map<std::string, std::vector<std::string>> all_bags;

		// Parse.
		for (const std::string& line : input) {
			auto bags = split(line, " bags contain ");
			std::string outer_bag = bags[0];
			std::string inner_bags = bags[1];

			all_bags[outer_bag];

			for (const auto& inner_bag : split(inner_bags, ", ")) {
				if (inner_bag == "no other bags.") {
					break;
				}

				auto inner_bag_breakdown = split(inner_bag, " ");
				all_bags[outer_bag].push_back(inner_bag_breakdown[1] + " " + inner_bag_breakdown[2]);
			}
		}

		// Search.
		int count = 0;
		for (const auto& pair : all_bags) {
			const auto& root = pair.first;

			std::vector<std::string> next = pair.second;
			std::set<std::string> visited;

			while (!next.empty()) {
				auto curr = next.back();
				next.pop_back();
				visited.emplace(curr);

				if (curr == "shiny gold") {
					++count;
					break;
				}

				for (auto& child : all_bags[curr]) {
					if (visited.find(child) == visited.end()) {
						next.push_back(child);
					}
				}
			}
		}

		printf("part 1: %d\n", count);
	}

	struct ContainedBags {
		int count = 0;
		std::string type;
	};

	int count_contained_bags(const std::map<std::string, std::vector<ContainedBags>>& all_bags,
			const std::string& bag_type) {
		int count = 0;
		for (const ContainedBags& contained_bag : all_bags.at(bag_type)) {
			count += contained_bag.count
				+ contained_bag.count * count_contained_bags(all_bags, contained_bag.type);
		}
		return count;
	}

	void part2() {
		std::vector<std::string> input = load_input("2020/2020_day07_input.txt");
		std::map<std::string, std::vector<ContainedBags>> bags;

		// Parse.
		for (const std::string& line : input) {
			auto bags_str = split(line, " bags contain ");
			std::string outer_bag = bags_str[0];
			std::string inner_bags = bags_str[1];

			bags.emplace(outer_bag, std::vector<ContainedBags>{});

			for (const auto& inner_bag : split(inner_bags, ", ")) {
				if (inner_bag == "no other bags.") {
					break;
				}

				auto tokens = split(inner_bag, " ");
				bags[outer_bag].push_back({ std::stoi(tokens[0]), tokens[1] + " " + tokens[2] });
			}
		}

		// Count.
		int total_bag_count = count_contained_bags(bags, "shiny gold");
		printf("part 2: %d\n", total_bag_count);
	}
}

namespace y2020 {
	void day07() {
		part1();
		part2();
	}
}
