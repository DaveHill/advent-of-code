#include <fstream>
#include <string>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cassert>

namespace {
	void part1() {
		std::ifstream fs{ "2020/2020_day04_input.txt" };
		int valid_passports = 0;
		int valid_fields = 0;
		while (!fs.eof()) {
			std::string line;
			std::getline(fs, line);

			int line_valid_fields = 
				(line.find("byr:") != std::string::npos)
				+ (line.find("iyr:") != std::string::npos)
				+ (line.find("eyr:") != std::string::npos)
				+ (line.find("hgt:") != std::string::npos)
				+ (line.find("hcl:") != std::string::npos)
				+ (line.find("ecl:") != std::string::npos)
				+ (line.find("pid:") != std::string::npos);
			valid_fields += line_valid_fields;

			if (line == "") {
				if (valid_fields == 7) {
					valid_passports++;
				}
				valid_fields = 0;
			}
		}
		printf("part 1: %d\n", valid_passports);
	}

	bool is_valid_field(const std::string& key, const std::string& value) {
		if (key == "cid") return false;

		if (key == "byr") {
			int year = std::stoi(value);
			return year >= 1920 && year <= 2002;
		}

		if (key == "iyr") {
			int year = std::stoi(value);
			return year >= 2010 && year <= 2020;
		}

		if (key == "eyr") {
			int year = std::stoi(value);
			return year >= 2020 && year <= 2030;
		}

		if (key == "hgt") {
			char unit[4];
			int height = 0;
			sscanf_s(value.c_str(), "%d%s", &height, unit, 3);
			if (unit == std::string("cm")) return height >= 150 && height <= 193;
			if (unit == std::string("in")) return height >= 59 && height <= 76;
			return false;
		}

		if (key == "hcl") {
			if (value[0] != '#') return false;
			if (value.length() != 7) return false;
			for (int i = 1; i < value.size(); ++i) {
				char c = value[i];
				if (!(std::isalpha(c) || std::isdigit(c))) {
					return false;
				}
			}
			return true;
		}

		if (key == "ecl") {
			return value == "amb" || value == "blu" || value == "brn" ||
				value == "gry" || value == "grn" || value == "hzl" || value == "oth";
		}

		if (key == "pid") {
			if (value.length() != 9) return false;
			for (auto c : value) {
				if (!std::isdigit(c)) return false;
			}
			return true;
		}

		return false;
	}

	void part2() {
		std::ifstream fs{ "2020/2020_day04_input.txt" };
		int valid_passports = 0;
		int valid_fields = 0;

		bool key_complete = false;
		std::string key;
		std::string value;

		for (char c, c_prev; fs.get(c); c_prev = c) {
			// Detect empty line.
			if (c == '\n' && c_prev == '\n') {
				if (valid_fields == 7) {
					valid_passports++;
				}
				valid_fields = 0;
				continue;
			}

			// Check for end of key.
			if (c == ':') {
				key_complete = true;
				continue;
			}

			// Check for end of value.
			if (c == ' ' || c == '\n') {
				valid_fields += is_valid_field(key, value);
				key = "";
				value = "";
				key_complete = false;
				continue;
			}

			if (!key_complete) {
				key += c;
			}
			else {
				value += c;
			}
		}
		printf("part 2: %d\n", valid_passports);
	}
}

namespace y2020 {
	void day04() {
		part1();
		part2();
	}
}
