#include <cstdio>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <sstream>
#include <set>

namespace {
	std::vector<int64_t> load_input(const char* filename) {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<int64_t> data;
		while (std::getline(file_stream, line)) {
			data.push_back(std::stoull(line));
		}
		return data;
	}

	void part1() {
		std::vector<int64_t> input = load_input("2020/2020_day09_input.txt");

		for (size_t i = 25; i < input.size(); ++i) {
			bool found = false;
			for (size_t j = i - 25; j < i; ++j) {
				for (size_t k = j; k < i; ++k) {
					if (input[i] == input[j] + input[k]) {
						found = true;
					}
				}
			}
			if (!found) {
				printf("part 1: %lld\n", input[i]);
			}
		}
	}

	void part2() {
		std::vector<int64_t> input = load_input("2020/2020_day09_input.txt");

		const int64_t target = 18272118;
		int64_t sum = input[0];

		auto begin = input.begin();
		auto end = input.begin();

		while (sum != target) {
			if (sum < target) {
				end++;
				sum += *end;
			}
			if (sum > target) {
				sum -= *begin;
				begin++;
			}

			if (sum == target) {
				auto minmax = std::minmax_element(begin, end);
				printf("part 2: %lld\n", *minmax.first + *minmax.second);
				break;
			}
		}
	}
}

namespace y2020 {
	void day09() {
		part1();
		part2();
	}
}
