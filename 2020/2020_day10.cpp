#include <algorithm>
#include <cstdio>
#include <vector>

#include "utils.h"

namespace {
	void part1() {
		std::vector<int64_t> input = load_ints("2020/2020_day10_input.txt");
		std::sort(input.begin(), input.end());

		int64_t gap[4] = { 0 };
		gap[input[0]]++;

		for (size_t i = 1; i < input.size(); ++i) {
			gap[input[i] - input[i - 1]]++;
		}

		gap[3]++;

		printf("part 1: %lld\n", gap[1] * gap[3]);
	}

	void part2() {
		std::vector<int64_t> input = load_ints("2020/2020_day10_input.txt");
		std::sort(input.begin(), input.end());
		input.insert(input.begin(), 0);

		std::vector<int64_t> permutations(input.size());
		permutations[0] = 1;

		for (size_t i = 1; i < input.size(); ++i) {
			for (size_t offset = 1; offset <= std::min<size_t>(i, 3); ++offset) {
				if (input[i] - input[i - offset] <= 3) {
					permutations[i] += permutations[i - offset];
				}
			}
		}

		printf("part 2: %lld\n", permutations.back());
	}
}

namespace y2020 {
	void day10() {
		part1();
		part2();
	}
}
