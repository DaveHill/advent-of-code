#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>

#include "utils.h"

namespace {
	void part1() {
		std::vector<std::string> seats = load_strings("2020/2020_day11_input.txt");
		std::vector<std::string> seats_out = seats;

		auto read_seat = [&](size_t row, size_t col) {
			// Assume out-of-bounds is floor.
			if (row >= seats.size() || col > seats[row].size())
				return '.';
			return seats[row][col];
		};

		bool stable = false;
		while (!stable) {
			stable = true;
			for (size_t row = 0; row < seats.size(); ++row) {
				for (size_t col = 0; col < seats[row].size(); ++col) {
					int adjacent_occupied = 0;
					if (read_seat(row - 1, col - 1) == '#') ++adjacent_occupied;
					if (read_seat(row - 1, col + 0) == '#') ++adjacent_occupied;
					if (read_seat(row - 1, col + 1) == '#') ++adjacent_occupied;
					if (read_seat(row + 0, col - 1) == '#') ++adjacent_occupied;
					if (read_seat(row + 0, col + 1) == '#') ++adjacent_occupied;
					if (read_seat(row + 1, col - 1) == '#') ++adjacent_occupied;
					if (read_seat(row + 1, col + 0) == '#') ++adjacent_occupied;
					if (read_seat(row + 1, col + 1) == '#') ++adjacent_occupied;

					if (seats[row][col] == 'L' && adjacent_occupied == 0) {
						seats_out[row][col] = '#';
						stable = false;
					} else if (seats[row][col] == '#' && adjacent_occupied >= 4) {
						seats_out[row][col] = 'L';
						stable = false;
					}
					else {
						seats_out[row][col] = seats[row][col];
					}
				}
			}
			std::swap(seats, seats_out);
		} 

		int occupied = 0;
		for (auto& row : seats) {
			for (auto& seat : row) {
				if (seat == '#')
					++occupied;
			}
		}

		printf("part 1: %d\n", occupied);
	}

	void part2() {
		std::vector<std::string> seats = load_strings("2020/2020_day11_input.txt");
		std::vector<std::string> seats_out = seats;

		auto read_seat = [&](size_t row, size_t col) {
			// Assume out-of-bounds is floor.
			if (row >= seats.size() || col > seats[row].size())
				return '.';
			return seats[row][col];
		};

		auto read_seat_in_direction = [&](size_t row, size_t col, int row_offset, int col_offset) {
			while (row < seats.size() && col < seats[row].size()) {
				row += row_offset;
				col += col_offset;

				const char seat = read_seat(row, col);
				if (seat != '.')
					return seat;
			}
			// Nuthin' but floor.
			return '.';
		};

		bool stable = false;
		while (!stable) {
			stable = true;
			for (size_t row = 0; row < seats.size(); ++row) {
				for (size_t col = 0; col < seats[row].size(); ++col) {
					int adjacent_occupied = 0;
					if (read_seat_in_direction(row, col, -1, -1) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, -1, 0) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, -1, 1) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, 0, -1) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, 0, 1) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, 1, -1) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, 1, 0) == '#') ++adjacent_occupied;
					if (read_seat_in_direction(row, col, 1, 1) == '#') ++adjacent_occupied;

					if (seats[row][col] == 'L' && adjacent_occupied == 0) {
						seats_out[row][col] = '#';
						stable = false;
					}
					else if (seats[row][col] == '#' && adjacent_occupied >= 5) {
						seats_out[row][col] = 'L';
						stable = false;
					}
					else {
						seats_out[row][col] = seats[row][col];
					}
				}
			}
			std::swap(seats, seats_out);
		}

		int occupied = 0;
		for (auto& row : seats) {
			for (auto& seat : row) {
				if (seat == '#')
					++occupied;
			}
		}

		printf("part 2: %d\n", occupied);
	}
}

namespace y2020 {
	void day11() {
		part1();
		part2();
	}
}
