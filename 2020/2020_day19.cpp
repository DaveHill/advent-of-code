#include <algorithm>
#include <cstdio>
#include <vector>
#include <chrono>
#include <thread>
#include <unordered_map>
#include <bitset>
#include <array>
#include <functional>
#include <deque>
#include <set>

#include "utils.h"

namespace {
	struct Rule {
		std::vector<std::vector<int>> sub_rules;
		char c = 0;
	};

	int matches(const std::unordered_map<int, Rule>& all_rules,
		int rule_id, const char* str, int pos) {
		const Rule& rule = all_rules.at(rule_id);

		if (rule.c != 0) {
			return str[pos] == rule.c ? pos + 1 : 0;
		}

		for (const auto& rule_set : rule.sub_rules) {
			bool matches_subset = true;
			int next_pos = pos;
			for (int sub_rule_id : rule_set) {			
				next_pos = matches(all_rules, sub_rule_id, str, next_pos);

				if (next_pos == 0) {
					matches_subset = false;
				}
			}
			if (matches_subset) {
				return next_pos;
			}
		}
		return 0;
	}

	void part1() {
		std::vector<std::string> input = load_strings("2020/2020_day19_input.txt");

		bool building_rules = true;
		int num_matches = 0;
		std::unordered_map<int, Rule> rules;
		for (auto& line : input) {
			if (line.empty()) {
				building_rules = false;
				continue;
			}

			if (building_rules) {
				auto parts = split(line, ":");
				int id = std::stoi(parts[0]);
				auto rule_str = parts[1];

				Rule& rule = rules[id];
				if (contains(rule_str, "\"")) {
					rule.c = rule_str[2];
				}
				else {
					for (auto sub_rule_section : split(rule_str, "|")) {
						rule.sub_rules.push_back({});
						for (auto sub_rule : split(sub_rule_section, " ")) {
							if (!sub_rule.empty()) {
								rule.sub_rules.back().push_back(std::stoi(sub_rule));
							}
						}
					}
				}
			}

			// Count matches.
			if (!building_rules) {
				if (matches(rules, 0, line.c_str(), 0) == line.length()) {
					++num_matches;
				}
			}
		}

		printf("part 1: %d\n", num_matches);
	}

	//std::vector<int> matches2(const std::unordered_map<int, Rule>& all_rules,
	//	int rule_id, const char* str, int pos) {
	//	const Rule& rule = all_rules.at(rule_id);

	//	if (rule.c != 0) {
	//		return str[pos] == rule.c ? std::vector<int>{pos + 1} : std::vector<int>{};
	//	}

	//	std::vector<int> next_positions;
	//	for (const auto& rule_set : rule.sub_rules) {
	//		bool matches_subset = true;

	//		for (int sub_rule_id : rule_set) {
	//			next_pos = matches2(all_rules, sub_rule_id, str, next_pos);

	//			if (next_pos == 0) {
	//				break;
	//			}
	//		}
	//		if (next_pos != 0) {
	//			next_positions.push_back(next_pos);
	//		}
	//	}
	//	return{};
	//}

	void part2() {
		//std::vector<std::string> input = load_strings("2020/2020_day19_input.txt");
		std::vector<std::string> input = {
"42: 9 14 | 10 1",
"9: 14 27 | 1 26",
"10: 23 14 | 28 1",
"1: \"a\"",
"11: 42 31",
"5: 1 14 | 15 1",
"19: 14 1 | 14 14",
"12: 24 14 | 19 1",
"16: 15 1 | 14 14",
"31: 14 17 | 1 13",
"6: 14 14 | 1 14",
"2: 1 24 | 14 4",
"0: 8 11",
"13: 14 3 | 1 12",
"15: 1 | 14",
"17: 14 2 | 1 7",
"23: 25 1 | 22 14",
"28: 16 1",
"4: 1 1",
"20: 14 14 | 1 15",
"3: 5 14 | 16 1",
"27: 1 6 | 14 18",
"14: \"b\"",
"21: 14 1 | 1 14",
"25: 1 1 | 1 14",
"22: 14 14",
"8: 42",
"26: 14 22 | 1 20",
"18: 15 15",
"7: 14 5 | 1 21",
"24: 14 1",
"",
"abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa",
"bbabbbbaabaabba",
"babbbbaabbbbbabbbbbbaabaaabaaa",
"aaabbbbbbaaaabaababaabababbabaaabbababababaaa",
"bbbbbbbaaaabbbbaaabbabaaa",
"bbbababbbbaaaaaaaabbababaaababaabab",
"ababaaaaaabaaab",
"ababaaaaabbbaba",
"baabbaaaabbaaaababbaababb",
"abbbbabbbbaaaababbbbbbaaaababb",
"aaaaabbaabaaaaababaa",
"aaaabbaaaabbaaa",
"aaaabbaabbaaaaaaabbbabbbaaabbaabaaa",
"babaaabbbaaabaababbaabababaaab",
"aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba",
		};

		bool building_rules = true;
		int num_matches = 0;
		std::unordered_map<int, Rule> rules;
		for (auto& line : input) {
			if (line.empty()) {
				building_rules = false;

				// Hack in the  part 2 replacement.
				Rule rule8;
				rule8.sub_rules.push_back({ 42 });
				rule8.sub_rules.push_back({ 42, 42 });
				rule8.sub_rules.push_back({ 42, 42, 42 });
				//rule8.sub_rules.push_back({ 42, 42, 42, 42 });
				//rule8.sub_rules.push_back({ 42, 42, 42, 42, 42 });
				//rule8.sub_rules.push_back({ 42, 42, 42, 42, 42, 42 });
				//rule8.sub_rules.push_back({ 42, 42, 42, 42, 42, 42, 42 });

				Rule rule11;
				rule11.sub_rules.push_back({ 42, 31 });
				rule11.sub_rules.push_back({ 42, 42, 31, 31 });
				rule11.sub_rules.push_back({ 42, 42, 42, 31, 31, 31 });
				rule11.sub_rules.push_back({ 42, 42, 42, 42, 31, 31, 31, 31 });
				//rule11.sub_rules.push_back({ 42, 42, 42, 42, 42, 31, 31, 31, 31, 31 });
				//rule11.sub_rules.push_back({ 42, 42, 42, 42, 42, 42, 31, 31, 31, 31, 31, 31 });
				//rule11.sub_rules.push_back({ 42, 42, 42, 42, 42, 42, 42, 31, 31, 31, 31, 31, 31, 31 });

				rules[8] = rule8;
				rules[11] = rule11;

				continue;
			}

			if (building_rules) {
				auto parts = split(line, ":");
				int id = std::stoi(parts[0]);
				auto rule_str = parts[1];

				Rule& rule = rules[id];
				if (contains(rule_str, "\"")) {
					rule.c = rule_str[2];
				}
				else {
					for (auto sub_rule_section : split(rule_str, "|")) {
						rule.sub_rules.push_back({});
						for (auto sub_rule : split(sub_rule_section, " ")) {
							if (!sub_rule.empty()) {
								rule.sub_rules.back().push_back(std::stoi(sub_rule));
							}
						}
					}
				}
			}

			// Count matches.
			if (!building_rules) {
				int matched_chars = matches(rules, 0, line.c_str(), 0);
				if (matched_chars != 0) {
					++num_matches;
					printf("%s -> match\n", line.c_str());
				}
				else {
					printf("%s -> no match\n", line.c_str());
				}
			}
		}

		printf("part 2: %d\n", num_matches);
	}
}

namespace y2020 {
	void day19() {
		part1();
		part2();
	}
}
