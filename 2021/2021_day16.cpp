#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <utility>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
    string hex_to_bin(string s)
    {
        ostringstream ss;
        for (char c : s) {
            switch (c) {
            case '0':
                ss << "0000";
                break;
            case '1':
                ss << "0001";
                break;
            case '2':
                ss << "0010";
                break;
            case '3':
                ss << "0011";
                break;
            case '4':
                ss << "0100";
                break;
            case '5':
                ss << "0101";
                break;
            case '6':
                ss << "0110";
                break;
            case '7':
                ss << "0111";
                break;
            case '8':
                ss << "1000";
                break;
            case '9':
                ss << "1001";
                break;
            case 'A':
                ss << "1010";
                break;
            case 'B':
                ss << "1011";
                break;
            case 'C':
                ss << "1100";
                break;
            case 'D':
                ss << "1101";
                break;
            case 'E':
                ss << "1110";
                break;
            case 'F':
                ss << "1111";
                break;
            default:
                printf("ERROR: %c", c);
                std::abort();
            }
        }

        return ss.str();
    }

	enum Type {
		Literal = 4,
        Sum = 0,
        Product = 1,
        Min = 2,
        Max = 3,
        Gt = 5,
        Lt = 6,
        Eq = 7,
	};

	struct Packet {
		int version;
		Type type;
		int64_t literal_value; // only if literal
		vector<Packet> sub_packets;
	};

	pair<Packet, int> parse(string str) {
        Packet packet{};
        packet.version = (str[0] - '0') << 2 | (str[1] - '0') << 1 | str[2] - '0';
        (int&)(packet.type) = (str[3] - '0') << 2 | (str[4] - '0') << 1 | str[5] - '0';

        if (packet.type == Type::Literal) {
            int i = 1;
            do {
                i += 5;
                // Parse 4 at a time.
                for (int j = i + 1; j < i + 5; ++j) {
                    packet.literal_value <<= 1;
                    packet.literal_value |= str[j] - '0';
                }
                
            } while (str[i] != '0');

            return { packet, i + 5};
        }
        else {
            // Operator packet.
            if (str[6] == '0') {
                // Next 15 bits represent total length in bits of subpackets.
                int total_length = 0;
                for (int i = 7; i < 7 + 15; ++i) {
                    total_length <<= 1;
                    total_length |= str[i] - '0';
                }

                int next = 7 + 15;
                while (next < 7 + 15 + total_length) {
                    auto [sub_packet, len] = parse(str.substr(next));
                    packet.sub_packets.push_back(sub_packet);
                    next += len;
                }

                return { packet, next };
            }
            else {
                // Next 11 bits represent number of sub-packets.
                int num_sub_packets = 0;
                for (int i = 7; i < 7 + 11; ++i) {
                    num_sub_packets <<= 1;
                    num_sub_packets |= str[i] - '0';
                }

                int next = 7 + 11;
                for (int i = 0; i < num_sub_packets; ++i) {
                    auto [sub_packet, len] = parse(str.substr(next));
                    packet.sub_packets.push_back(sub_packet);
                    next += len;
                }

                return { packet, next };
            }
        }
	}

    int64_t version_sum(Packet& p) {
        int64_t sum = p.version;
        for (auto& sb : p.sub_packets) {
            sum += version_sum(sb);
        }
        return sum;
    }

    int64_t calc(Packet& p) {
        switch (p.type) {
        case Type::Literal:
            return p.literal_value;
        case Type::Sum: {
            int64_t sum = 0;
            for (auto& sp : p.sub_packets) sum += calc(sp);
            return sum;
        }
        case Type::Product: {
            int64_t product = 1;
            for (auto& sp : p.sub_packets) product *= calc(sp);
            return product;
        }
        case Type::Min: {
            int64_t mini = numeric_limits<int64_t>::max();
            for (auto sp : p.sub_packets) mini = min(mini, calc(sp));
            return mini;
        }
        case Type::Max: {
            int64_t maxi = 0;
            for (auto sp : p.sub_packets) maxi = max(maxi, calc(sp));
            return maxi;
        }
        case Type::Lt:
            return calc(p.sub_packets[0]) < calc(p.sub_packets[1]) ? 1 : 0;
        case Type::Gt:
            return calc(p.sub_packets[0]) > calc(p.sub_packets[1]) ? 1 : 0;
        case Type::Eq:
            return calc(p.sub_packets[0]) == calc(p.sub_packets[1]) ? 1 : 0;
        }

        printf("Error: %d", p.type);
        std::abort();
    }

	void part1() {
		vector<string> in = load_strings("2021/2021_day16_input.txt");

		auto [packet, bits] = parse(hex_to_bin(in[0]));

		printf("part 1: %lld\n", version_sum(packet));
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day16_input.txt");
		int64_t result = 0;

        auto [packet, bits] = parse(hex_to_bin(in[0]));

		printf("part 2: %lld\n", calc(packet));
	}
}

namespace y2021 {
	void day16() {
		part1();
		part2();
	}
}
