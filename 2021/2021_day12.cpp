#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> in = load_strings("2021/2021_day12_input.txt");
		map<string, vector<string>> graph;
		for (auto line : in) {
			auto hyphen_pos = line.find('-');
			auto a = line.substr(0, hyphen_pos);
			auto b = line.substr(hyphen_pos + 1);
			graph[a].push_back(b);
			graph[b].push_back(a);
		}

		struct State {
			string cave;
			vector<string> path;
		};

		vector<State> next;
		next.push_back(State{ "start", { "start"} });

		int num_routes = 0;

		while (!next.empty()) {
			auto state = next.back();
			next.pop_back();

			for (auto& next_cave : graph[state.cave]) {
				if (next_cave == "end") {
					++num_routes;
					continue;
				}

				if (isupper(next_cave[0]) || count(state.path.begin(), state.path.end(), next_cave) == 0) {
					auto next_state = state;
					next_state.cave = next_cave;
					next_state.path.push_back(next_cave);
					next.push_back(next_state);
				}
			}
		}

		printf("part 1: %lld\n", num_routes);
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day12_input.txt");
		map<string, vector<string>> graph;
		for (auto line : in) {
			auto hyphen_pos = line.find('-');
			auto a = line.substr(0, hyphen_pos);
			auto b = line.substr(hyphen_pos + 1);
			graph[a].push_back(b);
			graph[b].push_back(a);
		}

		struct State {
			vector<string> path;
			bool used_freebie = false;
		};

		vector<State> next;
		next.push_back(State{ { "start"} });

		int num_paths = 0;

		while (!next.empty()) {
			State state = next.back();
			next.pop_back();

			for (auto& next_cave : graph[state.path.back()]) {
				if (next_cave == "end") {
					++num_paths;
					continue;
				}

				if (next_cave == "start") {
					continue;
				}

				int visit_count = count(state.path.begin(), state.path.end(), next_cave);
				if (isupper(next_cave[0]) || visit_count == 0 || !state.used_freebie) {
					auto next_state = state;
					if (visit_count > 0 && islower(next_cave[0])) {
						next_state.used_freebie = true;
					}
					next_state.path.push_back(next_cave);
					next.push_back(next_state);
				}
			}
		}

		printf("part 2: %lld\n", num_paths);
	}
}

namespace y2021 {
	void day12() {
		part1();
		part2();
	}
}
