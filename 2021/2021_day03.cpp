#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>

#include "2021_utils.h"

namespace {
	void part1() {
		std::vector<std::string> input_str = load_strings("2021/2021_day03_input.txt");
		std::vector<std::bitset<12>> input(input_str.begin(), input_str.end());

		std::bitset<12> result{};
		for (int i = 0; i < input[0].size(); ++i) {
			int one_count = std::count_if(input.begin(), input.end(),
				[&](auto b) { return b[i]; });
			result.set(i, one_count >= input.size() / 2);
		}

		printf("part 1: %d\n", result.to_ulong() * (~result).to_ulong());
	}

	void part2() {
		std::vector<std::string> input_str = load_strings("2021/2021_day03_input.txt");
		std::vector<std::bitset<12>> o2(input_str.begin(), input_str.end());
		std::vector<std::bitset<12>> co2 = o2;

		// Consider MSB first.
		for (int i = o2[0].size() - 1; i >= 0; --i) {
			int one_count = std::count_if(o2.begin(), o2.end(),
				[&](auto b) { return b[i]; });

			bool erase_one = (one_count < o2.size() / 2);
			o2.erase(std::remove_if(o2.begin(), o2.end(),
				[&](auto b) { return b[i] == erase_one; }), o2.end());

			if (o2.size() == 1) break;
		}

		for (int i = co2[0].size() - 1; i >= 0; --i) {
			int one_count = std::count_if(co2.begin(), co2.end(),
				[&](auto b) { return b[i]; });

			bool erase_one = (one_count >= co2.size() / 2);
			co2.erase(std::remove_if(co2.begin(), co2.end(),
				[&](auto b) { return b[i] == erase_one; }), co2.end());

			if (co2.size() == 1) break;
		}

		printf("part 2: %llu\n", o2[0].to_ullong() * co2[0].to_ullong());
	}
}

namespace y2021 {
	void day03() {
		part1();
		part2();
	}
}
