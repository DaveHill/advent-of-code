#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> in = load_strings("2021/2021_day10_input.txt");
		int64_t result = 0;

		for (auto s : in) {
			vector<char> stack;
			
			bool err = false;
			for (auto c : s) {
				if (err) break;
				switch (c)
				{
				case '{':
				case '[':
				case '(':
				case '<':
					stack.push_back(c);
					break;

				case ')':
					if (stack.back() == '(') {
						stack.pop_back();
						continue;
					}
					else {
						result += 3;
						err = true;
					}
					break;
				case ']':
					if (stack.back() == '[') {
						stack.pop_back();
						continue;
					}
					else {
						result += 57;
						err = true;
					}
					break;
				case '}':
					if (stack.back() == '{') {
						stack.pop_back();
						continue;
					}
					else {
						result += 1197;
						err = true;
					}
					break;
				case '>':
					if (stack.back() == '<') {
						stack.pop_back();
						continue;
					}
					else {
						result += 25137;
						err = true;
					}
					break;
						
				}
			}
		}


		printf("part 1: %lld\n", result);
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day10_input.txt");
		int64_t result = 0;

		std::vector<int64_t> results;
		for (auto s : in) {
			vector<char> stack;

			bool err = false;
			for (auto c : s) {
				if (err) break;
				switch (c)
				{
				case '{':
				case '[':
				case '(':
				case '<':
					stack.push_back(c);
					break;

				case ')':
					if (stack.back() == '(') {
						stack.pop_back();
						continue;
					}
					else {
						err = true;
					}
					break;
				case ']':
					if (stack.back() == '[') {
						stack.pop_back();
						continue;
					}
					else {
						err = true;
					}
					break;
				case '}':
					if (stack.back() == '{') {
						stack.pop_back();
						continue;
					}
					else {
						err = true;
					}
					break;
				case '>':
					if (stack.back() == '<') {
						stack.pop_back();
						continue;
					}
					else {
						err = true;
					}
					break;

				}
			}

			int64_t score = 0;
			if (!err) {
				while (!stack.empty()) {
					score *= 5;
					if (stack.back() == '(') {
						score += 1;
					}
					if (stack.back() == '[') {
						score += 2;
					}
					if (stack.back() == '{') {
						score += 3;
					}
					if (stack.back() == '<') {
						score += 4;
					}
					stack.pop_back();
				}
				results.push_back(score);
			}
		}

		std::sort(results.begin(), results.end());

		printf("part 2: %lld\n", results[results.size() / 2]);
	}
}

namespace y2021 {
	void day10() {
		part1();
		part2();
	}
}
