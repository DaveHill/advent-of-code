#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <array>
#include <utility>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
    struct Coord {
        int row, col;
    };

    bool operator<(const Coord& l, const Coord& r) {
        return tie(l.row, l.col) < tie(r.row, r.col);
    }

    int enhance(int num_enhances) {
        vector<string> input = load_strings("2021/2021_day20_input.txt");

        set<Coord> px;
        int min_col = 999;
        int max_col = -999;
        for (int r = 2; r < input.size(); ++r) {
            for (int c = 0; c < input[r].size(); ++c) {
                if (input[r][c] == '#') {
                    px.insert({ r - 2, c });
                    min_col = min(min_col, c);
                    max_col = max(max_col, c);
                }
            }
        }

        for (int i = 0; i < num_enhances; ++i) {
            set<Coord> new_px;
            int new_min_col = 999;
            int new_max_col = -999;
            for (int r = px.begin()->row - 1; r <= px.rbegin()->row + 1; ++r) {
                for (int c = min_col - 1; c <= max_col + 1; ++c) {
                    int idx = 0;
                    for (int rr = r - 1; rr <= r + 1; ++rr) {
                        for (int cc = c - 1; cc <= c + 1; ++cc) {
                            idx <<= 1;

                            bool outside_is_light = (i % 2);
                            bool out_of_bounds = (rr < px.begin()->row 
                                || rr > px.rbegin()->row 
                                || cc < min_col 
                                || cc > max_col);

                            if (px.count({ rr, cc }) || (out_of_bounds && outside_is_light)) {
                                idx |= 1;
                            }
                        }
                    }

                    if (input[0][idx] == '#') {
                        new_px.insert({ r, c });
                        new_min_col = min(new_min_col, c);
                        new_max_col = max(new_max_col, c);
                    }
                }
            }
            swap(px, new_px);
            min_col = new_min_col;
            max_col = new_max_col;
        }

        return px.size();
    }

    void part1() {
        printf("part 2: %lld\n", enhance(2));
    }

    void part2() {
        printf("part 2: %lld\n", enhance(50));
    }
}

namespace y2021 {
    void day20() {
        part1();
        part2();
    }
}
