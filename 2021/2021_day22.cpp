#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <array>
#include <utility>
#include <sstream>
#include <functional>

#include "2021_utils.h"

using namespace ::std;

namespace {
	struct V3 { int64_t x, y, z; };
	struct Box {
		V3 min, max;
		int64_t volume() const {
			if (max.x < min.x || max.y < min.y || max.z < min.z) {
				return -1;
			}

			return (max.x - min.x + 1) *
				(max.y - min.y + 1) *
				(max.z - min.z + 1);
		}
	};

	void part1() {
		vector<bool> grid;
		grid.resize(101 * 101 * 101);
		for (auto& line : load_strings("2021/2021_day22_input.txt")) {
			V3 vmin, vmax;
			char state[4]{0};
			sscanf_s(line.c_str(), "%s x=%lld..%lld,y=%lld..%lld,z=%lld..%lld", state, 4,
				&vmin.x, &vmax.x, &vmin.y, &vmax.y, &vmin.z, &vmax.z);

			bool on = (state == string("on"));

			if (vmax.x < -50 || vmin.x > 50 ||
				vmax.y < -50 || vmin.y > 50 ||
				vmax.z < -50 || vmin.z > 50) {
				continue;

			}
			for (int64_t z = max(-50ll, vmin.z); z <= min(50ll, vmax.z); ++z) {
				for (int64_t y = max(-50ll, vmin.y); y <= min(50ll, vmax.y); ++y) {
					for (int64_t x = max(-50ll, vmin.x); x <= min(50ll, vmax.x); ++x) {
						int64_t idx = (z + 50) * 101 * 101 + (y + 50) * 101 + (x + 50);
						grid[idx] = on;
					}
				}
			}
		}


		printf("part 1: %lld\n", count(grid.begin(), grid.end(), true));
	}

	void part2() {
		vector<Box> boxes;
		auto input = load_strings("2021/2021_day22_input.txt");
		for (auto& line : input) {
			Box box_in;
			char state[4]{ 0 };
			sscanf_s(line.c_str(), "%s x=%lld..%lld,y=%lld..%lld,z=%lld..%lld", state, 4,
				&box_in.min.x, &box_in.max.x,
				&box_in.min.y, &box_in.max.y,
				&box_in.min.z, &box_in.max.z);
			const bool on = (state == string("on"));

			vector<Box> new_boxes;
			for (const auto& box : boxes) {
				// Early out if no intersection with main box.
				if (box.max.x < box_in.min.x ||
					box.min.x > box_in.max.x ||
					box.max.y < box_in.min.y ||
					box.min.y > box_in.max.y ||
					box.max.z < box_in.min.z ||
					box.min.z > box_in.max.z) {
					new_boxes.push_back(box);
					continue;
				}

				// Generate 27 new boxes by intersecting pre/in/post each face.
				vector<function<void(Box& b)>> x_fns = {
					[&](Box& b) { b.max.x = min(b.max.x, box_in.min.x - 1); },
					[&](Box& b) { b.min.x = max(b.min.x, box_in.max.x + 1); },
					[&](Box& b) {
						b.min.x = max(b.min.x, box_in.min.x);
						b.max.x = min(b.max.x, box_in.max.x);
				} };
				vector<function<void(Box& b)>> y_fns = {
					[&](Box& b) { b.max.y = min(b.max.y, box_in.min.y - 1); },
					[&](Box& b) { b.min.y = max(b.min.y, box_in.max.y + 1); },
					[&](Box& b) {
						b.min.y = max(b.min.y, box_in.min.y);
						b.max.y = min(b.max.y, box_in.max.y);
				} };
				vector<function<void(Box& b)>> z_fns = {
					[&](Box& b) { b.max.z = min(b.max.z, box_in.min.z - 1); },
					[&](Box& b) { b.min.z = max(b.min.z, box_in.max.z + 1); },
					[&](Box& b) {
					b.min.z = max(b.min.z, box_in.min.z);
					b.max.z = min(b.max.z, box_in.max.z);
				} };

				for (auto& x_fn : x_fns) {
					for (auto& y_fn : y_fns) {
						for (auto& z_fn : z_fns) {
							Box new_box = box;
							x_fn(new_box);
							y_fn(new_box);
							z_fn(new_box);

							// Filter degenerate boxes.
							if (new_box.volume() <= 0)
								continue;

							// Keep non-intersecting boxes
							if (new_box.max.x < box_in.min.x ||
								new_box.max.y < box_in.min.y ||
								new_box.max.z < box_in.min.z ||
								new_box.min.x > box_in.max.x ||
								new_box.min.y > box_in.max.y ||
								new_box.min.z > box_in.max.z) {
								new_boxes.push_back(new_box);
							}
						}
					}
				}
			}

			// Update boxes.
			boxes = move(new_boxes);

			// Insert the new box.
			if (on) {
				boxes.push_back(box_in);
			}
		}

		uint64_t volume_sum = 0;
		for (auto& box : boxes) {
			volume_sum += box.volume();
		}
		printf("part 2: %lld\n", volume_sum);
	}
}

namespace y2021 {
	void day22() {
		part1();
		part2();
	}
}
