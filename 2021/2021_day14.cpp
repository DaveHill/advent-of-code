#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> in = load_strings("2021/2021_day14_input.txt");
		int64_t result = 0;

		map<string, string> rules;
		for (auto& line : in) {
			char from[16];
			char to;
			if (sscanf_s(line.c_str(), "%s -> %c", from, 16, &to, 1)) {
				rules[from] = to;
			}
		}

		string pattern = in[0];

		for (int i = 0; i < 10; ++i) {
			vector<pair<char, int>> insert;
			for (int p = 0; p < pattern.size() - 1; ++p) {
				for (auto& [from, to] : rules) {
					if (pattern.substr(p, 2) == from) {
						insert.push_back({ to[0], p + 1 });
					}
				}
			}

			int offset = 0;
			for (auto& [c, pos] : insert) {
				pattern.insert(pattern.begin() + pos + offset++, c);
			}

			result++;
		}

		map<char, int64_t> histogram;
		for (auto c : pattern) {
			histogram[c]++;
		}

		int64_t mini = 99999999, maxi = 0;
		for (auto [c, count] : histogram) {
			mini = min(mini, count);
			maxi = max(maxi, count);
		}

		printf("part 1: %lld\n", maxi - mini);
	}

	void part2() {
		int64_t result = 0;
		{
			vector<string> in = load_strings("2021/2021_day14_input.txt");
			Profile p("2021 day 14 part 2");

			// Parse rules.
			unordered_map<string, char> rules;
			for (auto& line : in) {
				char from[16];
				char to;
				if (sscanf_s(line.c_str(), "%s -> %c", from, 16, &to, 1)) {
					rules[from] = to;
				}
			}

			string pattern = in[0];
			unordered_map<string, int64_t> pairs;

			// Initialize pair counts.
			for (int i = 0; i < pattern.size() - 1; ++i) {
				pairs[pattern.substr(i, 2)]++;
			}

			// Run the simulation.
			for (int i = 0; i < 40; ++i) {
				unordered_map<string, int64_t> pairs2;
				for (auto& [pair, count] : pairs) {
					for (auto& [from, to] : rules) {
						if (pair == from) {
							pairs2[string{} + from[0] + to] += count;
							pairs2[string{} + to + from[1]] += count;
							break;
						}
					}
				}

				std::swap(pairs, pairs2);
			}

			// Add 'em up.
			unordered_map<char, int64_t> histogram;
			for (auto& [pair, count] : pairs) {
				histogram[pair[0]] += count;
				histogram[pair[1]] += count;
			}

			// Account for first and last character only appearing once per-pair.
			histogram[pattern.front()]++;
			histogram[pattern.back()]++;

			int64_t mini = std::numeric_limits<int64_t>::max(), maxi = 0;
			for (auto [c, count] : histogram) {
				mini = min(mini, count);
				maxi = max(maxi, count);
			}
			result = (maxi - mini) / 2;
		}
		printf("part 2: %lld\n", result);
	}
}

namespace y2021 {
	void day14() {
		part1();
		part2();
	}
}
