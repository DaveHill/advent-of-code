#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> in = load_strings("2021/2021_day13_input.txt");
		int64_t result = 0;

		vector<vector<uint8_t>> grid;

		for (auto& line : in) {
			{
				int x, y;
				if (sscanf_s(line.c_str(), "%d, %d", &x, &y) == 2) {
					if (y >= grid.size()) {
						grid.resize(y + 1);
					}

					for (auto& line : grid) {
						line.resize(std::max((int)line.size(), x + 1));
					}

					grid[y][x] = 1;
				}
			}

			{
				char axis;
				int coord;
				if (sscanf_s(line.c_str(), "fold along %c=%d", &axis, 1, &coord) == 2) {
					if (axis == 'x') {
						int num_pixels = min((size_t)coord, grid[0].size() - coord);
						for (int y = 0; y < grid.size(); ++y) {
							for (int i = 1; i <= num_pixels; ++i) {
								grid[y][coord - i] |= grid[y][coord + i];
							}

							grid[y].resize(coord);
						}
					}

					if (axis == 'y') {
						int num_pixels = min(coord, (int)grid.size() - coord);
						for (int i = 1; i <= num_pixels; ++i) {
							for (int x = 0; x < grid[0].size(); ++x) {
								grid[coord - i][x] |= grid[coord + i][x];
							}
						}

						grid.resize(coord);
					}

					break;
				}
			}
		}


		for (auto line : grid) {
			for (auto pixel : line) {
				if (pixel)
					++result;
			}
		}

		printf("part 1: %lld\n", result);
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day13_input.txt");
		vector<vector<uint8_t>> grid;

		for (auto& line : in) {
			{
				int x, y;
				if (sscanf_s(line.c_str(), "%d, %d", &x, &y) == 2) {
					if (y >= grid.size()) {
						grid.resize(y + 1);
					}

					for (auto& line : grid) {
						line.resize(std::max((int)line.size(), x + 1));
					}

					grid[y][x] = 1;
				}
			}

			{
				char axis;
				int coord;
				if (sscanf_s(line.c_str(), "fold along %c=%d", &axis, 1, &coord) == 2) {
					if (axis == 'x') {
						int num_pixels = min((size_t)coord, grid[0].size() - coord);
						for (int y = 0; y < grid.size(); ++y) {
							for (int i = 1; i <= num_pixels; ++i) {
								grid[y][coord - i] |= grid[y][coord + i];
							}

							grid[y].resize(coord);
						}
					}

					if (axis == 'y') {
						int num_pixels = min(coord, (int)grid.size() - coord);
						for (int i = 1; i <= num_pixels; ++i) {
							for (int x = 0; x < grid[0].size(); ++x) {
								grid[coord - i][x] |= grid[coord + i][x];
							}
						}

						grid.resize(coord);
					}
				}
			}
		}

		for (int y = 0; y < grid.size(); ++y) {
			for (int x = 0; x < grid[0].size(); ++x) {
				printf("%c", grid[y][x] ? '#' : ' ');
			}
			printf("\n");
		}
	}
}

namespace y2021 {
	void day13() {
		part1();
		part2();
	}
}
