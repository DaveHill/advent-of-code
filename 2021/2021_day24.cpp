#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <array>
#include <utility>
#include <functional>
#include <cassert>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void program(int64_t* input, int64_t z_out[14]) {
		int64_t w = 0, x = 0, y = 0, z = 0;
		int i = 0;
#define add(x,y) x = x + y;
#define mul(x,y) x = x * y;
#define div(x,y) x = x / y;
#define mod(x,y) x = x % y;
#define eql(x,y) x = (x == y ? 1 : 0);
#define inp(x) { x = input[i]; z_out[i++] = z; }
#include "2021_day24_input.txt"

		// Write out the final z value.
		z_out[i] = z;
	}

	struct Run {
		vector<int64_t> input = vector<int64_t>(14, 0);
		vector<int64_t> output = vector<int64_t>(15, 0);
	};

	void print(Run& r, int digits) {
		printf("---\ninput: ");
		for (int i = 0; i <= digits; ++i) {
			printf("%d ", r.input[i]);
		}
		printf("\noutput: ");
		for (int i = 0; i <= digits + 1; ++i) {
			printf("%d ", r.output[i]);
		}
		printf("\n");
	}

	template<typename Comp>
	vector<int64_t> process(Comp comp) {
		vector<Run> potential_runs(1);

		for (int digit = 0; digit < 14; ++digit) {
			vector<Run> current_runs;
			for (const auto& potential_run : potential_runs) {
				for (int64_t a = 1; a <= 9; ++a) {
					auto input = potential_run.input;
					input[digit] = a;

					Run r{ input };
					program(r.input.data(), r.output.data());
					current_runs.push_back(move(r));
				}
			}

			constexpr size_t MAX_RUNS = 1'000'000;

			// Sort first by lowest most-recent Z value, then by input.
			sort(current_runs.begin(), current_runs.end(), [&](const Run& r1, const Run& r2) {
				if (r1.output[digit + 1] == r2.output[digit + 1]) {
					return comp(r1.input, r2.input);
				}

				return r1.output[digit + 1] < r2.output[digit + 1];
				});

			potential_runs.assign(current_runs.begin(), current_runs.begin() + min(current_runs.size(), MAX_RUNS));

			print(potential_runs[0], digit);
		}

		return potential_runs[0].input;
	}

	void part1() {
		auto ans = process([](auto& l, auto& r) { return l > r; });

		printf("part 1: \n");
		for (auto d : ans) {
			printf("%lld", d);
		}
		printf("\n");
	}

	void part2() {
		auto ans = process([](auto& l, auto& r) { return l < r; });

		printf("part 2: \n");
		for (auto d : ans) {
			printf("%lld", d);
		}
		printf("\n");
	}
}

namespace y2021 {
	void day24() {
		part1();
		part2();
	}
}
