#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>

#include "2021_utils.h"

namespace {
	void part1() {
		std::vector<std::string> input_str = load_strings("2021/2021_day08_input.txt");
		int64_t result = 0;
		
		for (auto& line : input_str) {
			auto parts = split(line, "|");
			auto digs = split(parts[1], " ");

			for (auto d : digs) {
				if (d.length() == 2 || d.length() == 4 || d.length() == 3 || d.length() == 7)
					++result;
			}
		}

		printf("part 1: %lld\n", result);
	}

	void part2() {
		auto input_str = load_strings("2021/2021_day08_input.txt");
		int64_t result = 0;

		for (auto& line : input_str) {
			auto parts = split(line, "|");
			auto patterns = split(parts[0], " ");
			auto code = split(parts[1], " ");

			std::map<std::string, int> digit;
			std::map<int, std::string> pattern;

			// sort all
			for (auto& pattern : patterns) {
				std::sort(pattern.begin(), pattern.end());
			}
			for (auto& c : code) {
				std::sort(c.begin(), c.end());
			}

			// find  1, 4, 7, 8.
			for (auto& p : patterns) {
				if (p.length() == 2) {
					digit[p] = 1;
					pattern[1] = p;
				}
				if (p.length() == 4) {
					digit[p] = 4;
					pattern[4] = p;
				}
				if (p.length() == 3) {
					digit[p] = 7;
					pattern[7] = p;
				}
				if (p.length() == 7) {
					digit[p] = 8;
					pattern[8] = p;
				}
			}

			// 9 has 6 segments and contains all of 4's segments.
			for (auto& p : patterns) {
				if (p.length() == 6) {
					std::vector<char> out;
					std::set_intersection(p.begin(), p.end(),
						pattern[4].begin(), pattern[4].end(),
						std::back_inserter(out));

					if (out.size() == 4) {
						digit[p] = 9;
						pattern[9] = p;
					}
				}
			}

			// 0 has 6 segments, contains all of 1's segments, and isn't 9.
			for (auto& p : patterns) {
				if (p.length() == 6) {
					std::vector<char> out;
					std::set_intersection(p.begin(), p.end(),
						pattern[1].begin(), pattern[1].end(),
						std::back_inserter(out));

					if (out.size() == 2 && p != pattern[9]) {
						digit[p] = 0;
						pattern[0] = p;
					}
				}
			}

			// 6 has 6 segments and isn't 0 or 9.
			for (auto& p : patterns) {
				if (p.length() == 6 && p != pattern[0] && p != pattern[9]) {
					digit[p] = 6;
					pattern[6] = p;
				}
			}

			// 5 shares all 5 segments with 6.
			for (auto& p : patterns) {
				if (p.length() == 5) {
					std::vector<char> out;
					std::set_intersection(p.begin(), p.end(),
						pattern[6].begin(), pattern[6].end(),
						std::back_inserter(out));

					if (out.size() == 5) {
						digit[p] = 5;
						pattern[5] = p;
					}
				}
			}

			// 3 shares 2 segments with 1.
			for (auto& p : patterns) {
				if (p.length() == 5) {
					std::vector<char> out;
					std::set_intersection(p.begin(), p.end(),
						pattern[1].begin(), pattern[1].end(),
						std::back_inserter(out));

					if (out.size() == 2) {
						digit[p] = 3;
						pattern[3] = p;
					}
				}
			}

			// 2 has 5 segments and isn't 3 or 5.
			for (auto& p : patterns) {
				if (p.length() == 5 && p != pattern[3] && p != pattern[5]) {
					digit[p] = 2;
					pattern[2] = p;
				}
			}

			// Add 'em up.
			int column = 1000;
			for (int i = 0; i < 4; ++i) {
				result += column * digit[code[i]];
				column /= 10;
			}
		}

		printf("part 2: %lld\n", result);
	}
}

namespace y2021 {
	void day08() {
		part1();
		part2();
	}
}
