#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <array>
#include <utility>
#include <functional>
#include <cassert>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> grid = load_strings("2021/2021_day25_input.txt");

		int height = grid.size();
		int width = grid[0].length();

		vector<string> blank(height, string(width, '.'));

		int steps_taken = 0;
		while (++steps_taken) {
			auto init_grid = grid;

			// Move east.
			for (auto r = 0; r < height; ++r) {
				for (auto c = 0; c < width; ++c) {
					if (init_grid[r][c] == '>' && init_grid[r][(c + 1) % width] == '.') {
						// Do move.
						grid[r][c] = '.';
						grid[r][(c + 1) % width] = '>';
					}
				}
			}

			auto half_grid = grid;

			// Move south.
			for (auto r = 0; r < height; ++r) {
				for (auto c = 0; c < width; ++c) {
					if (half_grid[r][c] == 'v' && half_grid[(r + 1) % height][c] == '.') {
						grid[r][c] = '.';
						grid[(r + 1) % height][c] = 'v';
					}
				}
			}

			if (init_grid == grid)
				break;
		}

		printf("part 1: %d\n", steps_taken);
	}

	void part2() {

	}
}

namespace y2021 {
	void day25() {
		part1();
		part2();
	}
}
