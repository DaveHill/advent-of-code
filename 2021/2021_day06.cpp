#include <vector>
#include <string>
#include <array>
#include <cstdio>
#include <algorithm>
#include <set>

#include "2021_utils.h"

namespace {
	void part1() {
		auto fish_str = split(load_strings("2021/2021_day06_input.txt")[0], ",");
		std::vector<int> fish(fish_str.size());
		std::transform(fish_str.begin(), fish_str.end(), fish.begin(),
			[](auto& s) { return std::atoi(s.c_str()); });

		constexpr int DAYS = 80;
		for (int d = 0; d < DAYS; ++d) {
			for (int i = 0; i < fish.size(); ++i) {
				if (fish[i] == 0) {
					fish[i] = 7;
					fish.push_back(9);
				}
				--fish[i];
			}
		}

		printf("part 1: %d\n", fish.size());
	}

	void part2() {
		auto fish_str = split(load_strings("2021/2021_day06_input.txt")[0], ",");
		std::array<size_t, 11> fish_age_count{};

		for (auto& str : fish_str)
			fish_age_count[std::atoi(str.c_str())]++;

		constexpr int DAYS = 256;
		for (int d = 0; d < DAYS; ++d) {
			fish_age_count[9] = fish_age_count[0];
			fish_age_count[7] += fish_age_count[0];
			for (int i = 0; i < 10; ++i) {
				fish_age_count[i] = fish_age_count[i + 1];
			}
		}

		int64_t count = 0;
		for (auto num : fish_age_count)
			count += num;
		printf("part 2: %lld\n", count);
	}
}

namespace y2021 {
	void day06() {
		part1();
		part2();
	}
}
