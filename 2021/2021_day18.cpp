#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <utility>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
    struct SN {
        int regular = -1;
        unique_ptr<SN> left;
        unique_ptr<SN> right;
        SN* parent = nullptr;

        bool is_regular() { return regular >= 0; }
        bool has_pair() { return left && left->is_regular()
                             && right && right->is_regular(); }
        int level() { return parent ? parent->level() + 1 : 1; }
    };

    unique_ptr<SN> parse(string s) {
        unique_ptr<SN> root = make_unique<SN>();
        vector<SN*> stack;
        stack.push_back(root.get());

        for (auto c : s) {
            if (c >= '0' && c <= '9') {
                stack.back()->regular = c - '0';
            }

            if (c == '[') {
                stack.back()->left = make_unique<SN>();
                stack.back()->left->parent = stack.back();
                stack.push_back(stack.back()->left.get());
            }

            if (c == ',') {
                stack.pop_back();
                stack.back()->right = make_unique<SN>();
                stack.back()->right->parent = stack.back();
                stack.push_back(stack.back()->right.get());
            }

            if (c == ']') {
                stack.pop_back();
            }
        }

        return root;
    }

    string to_str(unique_ptr<SN>& n) {
        if (n->regular >= 0) return to_string(n->regular);
        return std::string() + '[' + to_str(n->left) + ',' + to_str(n->right) + ']';
    }

    unique_ptr<SN> add(unique_ptr<SN> l, unique_ptr<SN> r) {
        auto n = make_unique<SN>();
        n->left = move(l);
        n->left->parent = n.get();
        n->right = move(r);
        n->right->parent = n.get();
        return n;
    }

    bool try_explode(unique_ptr<SN>& n) {
        int* prev_regular = nullptr;
        int add_to_next_regular = -1;

        vector<SN*> stack;
        stack.push_back(n.get());

        int depth = 1;
        while (!stack.empty()) {
            SN* n = stack.back();
            stack.pop_back();

            if (add_to_next_regular < 0 && n->has_pair() && n->level() > 4) {
                if (prev_regular) {
                    *prev_regular += n->left->regular;
                }
                add_to_next_regular = n->right->regular;

                n->left.reset();
                n->right.reset();
                n->regular = 0;
                continue;
            }
            
            if (n->is_regular()) {
                if (add_to_next_regular >= 0) {
                    // Explode finished.
                    n->regular += add_to_next_regular;
                    return true;
                }

                prev_regular = &n->regular;
            }

            if (n->right) stack.push_back(n->right.get());
            if (n->left) stack.push_back(n->left.get());
        }

        return add_to_next_regular >= 0;
    }

    bool try_split(unique_ptr<SN>& n) {
        if (n->is_regular() && n->regular > 9) {
            n->left = make_unique<SN>();
            n->left->regular = n->regular / 2;
            n->left->parent = n.get();

            n->right = make_unique<SN>();
            n->right->regular = (n->regular + 1) / 2;
            n->right->parent = n.get();

            n->regular = -1;
            return true;
        }

        return (n->left && try_split(n->left)) || (n->right && try_split(n->right));
    }

    void reduce(unique_ptr<SN>& n) {
        while (try_explode(n) || try_split(n)) {
            //printf("%s\n", to_str(n).c_str());
        }
    }

    int64_t magnitude(unique_ptr<SN>& n) {
        if (n->is_regular()) return n->regular;
        
        int64_t left = n->left ? magnitude(n->left) * 3 : 0;
        int64_t right = n->right ? magnitude(n->right) * 2 : 0;
        return left + right;
    }

    void part1() {
        vector<string> in = load_strings("2021/2021_day18_input.txt");
        auto n = parse(in[0]);
        for (int i = 1; i < in.size(); ++i) {
            n = add(move(n), parse(in[i]));
            reduce(n);
        }

        printf("part 1: %lld\n", magnitude(n));
    }

    void part2() {
        vector<string> in = load_strings("2021/2021_day18_input.txt");
        
        int64_t maxi;
        for (int i = 0; i < in.size(); ++i) {
            for (int j = 0; j < in.size(); ++j) {
                if (i == j) continue;
                auto ni = parse(in[i]);
                auto nj = parse(in[j]);
                auto result = add(move(ni), move(nj));
                reduce(result);
                maxi = max(maxi, magnitude(result));
            }
        }

        printf("part 2: %lld\n", maxi);
    }
}

namespace y2021 {
    void day18() {
        part1();
        part2();
    }
}
