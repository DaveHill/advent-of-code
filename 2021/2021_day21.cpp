#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <array>
#include <utility>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {

    void part1() {       
        int64_t p0 = 9;
        int64_t p1 = 3;
        int64_t p0_score = 0;
        int64_t p1_score = 0;
        int64_t next_dice_roll = 1;
        int64_t num_dice_rolls = 0;

        auto roll = [&] {
            auto result = next_dice_roll;
            if (++next_dice_roll == 101) next_dice_roll = 1;
            ++num_dice_rolls;
            return result;
        };

        while (p0_score < 1000 && p1_score < 1000) {
            p0 = (p0 + roll() + roll() + roll()) % 10;
            if (p0 == 0) p0 = 10;
            p0_score += p0;
            if (p0_score >= 1000) continue;

            p1 = (p1 + roll() + roll() + roll()) % 10;
            if (p1 == 0) p1 = 10;
            p1_score += p1;
        }

        auto loser_score = p0_score > p1_score ? p1_score : p0_score;

        printf("part 1: %lld\n", loser_score * num_dice_rolls);
    }

    struct State {
        int p0_score;
        int p1_score;
        int p0;
        int p1;
    };

    bool operator<(const State& l, const State& r) {
        return tie(l.p0_score, l.p1_score, l.p0, l.p1) < tie(r.p0_score, r.p1_score, r.p0, r.p1);
    }

    void part2() {
        map<State, int64_t> states;
        State initial_state{};
        initial_state.p0 = 9;
        initial_state.p1 = 3;
        states[initial_state] = 1;

        vector<int64_t> rolls;
        for (int64_t x = 1; x <= 3; ++x) {
            for (int64_t y = 1; y <= 3; ++y) {
                for (int64_t z = 1; z <= 3; ++z) {
                    rolls.push_back(x + y + z);
                }
            }
        }

        while (true) {
            // Player 1's turn.
            auto new_states = states;
			for (auto& [old_state, old_count] : states) {
                if (old_count == 0 || old_state.p0_score >= 21 || old_state.p1_score >= 21) {
                    continue;
                }

				for (auto roll : rolls) {
                    State new_state = old_state;
                    new_state.p0 += roll;
                    if (new_state.p0 > 10) new_state.p0 -= 10;
                    new_state.p0_score += new_state.p0;
                    new_states[new_state] += old_count;
                }
                new_states[old_state] -= old_count;
            }
            states = new_states;

            // Player 2's turn.
			for (auto& [old_state, old_count] : states) {
                if (old_count == 0 || old_state.p0_score >= 21 || old_state.p1_score >= 21) {
                    continue;
                }

                for (auto roll : rolls) {
                    State new_state = old_state;
                    new_state.p1 += roll;
                    if (new_state.p1 > 10) new_state.p1 -= 10;
                    new_state.p1_score += new_state.p1;
                    new_states[new_state] += old_count;
                }
                new_states[old_state] -= old_count;
            }
            std::swap(new_states, states);

            if (all_of(states.begin(), states.end(), [](auto& s) {
                return s.second == 0 || s.first.p0_score >= 21 || s.first.p1_score >= 21; })) {
                break;
            }
        }

        // All games finished. Count the winners.
        int64_t p0_wins = 0;
        int64_t p1_wins = 0;
        for (auto& [state, count] : states) {
            if (state.p0_score >= 21) p0_wins += count;
            else p1_wins += count;
        }

        printf("part 2: %lld\n", max(p0_wins, p1_wins));
    }
}

namespace y2021 {
    void day21() {
        part1();
        part2();
    }
}
