#include <vector>
#include <string>
#include <cstdio>

#include "2021_utils.h"

namespace {
	void part1() {
		auto input = load_strings("2021/2021_day02_input.txt");
		int horiz = 0, depth = 0;
		for (auto& line : input) {
			auto instruction = split(line, " ");
			int x = std::atoi(instruction[1].c_str());
			if (instruction[0] == "forward") horiz += x;
			if (instruction[0] == "down") depth -= x;
			if (instruction[0] == "up") depth += x;
		}

		printf("part 1: %d\n", std::abs(horiz * depth));
	}

	void part2() {
		auto input = load_strings("2021/2021_day02_input.txt");
		int aim = 0, horiz = 0, depth = 0;
		for (auto& line : input) {
			auto instruction = split(line, " ");
			int x = std::atoi(instruction[1].c_str());
			if (instruction[0] == "forward") {
				horiz += x;
				depth += x * aim;
			}
			if (instruction[0] == "down") aim += x;
			if (instruction[0] == "up") aim -= x;
		}

		printf("part 2: %d\n", std::abs(horiz * depth));
	}
}

namespace y2021 {
	void day02() {
		part1();
		part2();
	}
}
