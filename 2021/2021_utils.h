#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <chrono>

namespace y2021 {
	inline std::string load_string(const char* filename) {
		std::ifstream t("file.txt");
		std::stringstream buffer;
		buffer << t.rdbuf();
		return buffer.str();
	}

	inline std::vector<int64_t> load_ints(const char* filename, const char delim = '\n') {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<int64_t> data;
		while (std::getline(file_stream, line, delim)) {
			data.push_back(std::stoull(line));
		}
		return data;
	}

	inline std::vector<std::string> load_strings(const char* filename) {
		std::ifstream file_stream{ filename };
		std::string line;
		std::vector<std::string> data;
		while (std::getline(file_stream, line)) {
			data.push_back(line);
		}
		return data;
	}

	inline std::vector<std::string> split(const std::string& str, const std::string& delim) {
		size_t start = 0;
		size_t end = str.find(delim);
		std::vector<std::string> result;
		while (end != std::string::npos) {
			auto part = str.substr(start, end - start);
			if (!part.empty())
				result.push_back(part);
			start = end + delim.length();
			end = str.find(delim, start);
		}
		auto last = str.substr(start, end);
		if (!last.empty())
			result.push_back(last);
		return result;
	}

	inline bool starts_with(const std::string& haystack, const std::string& needle) {
		return haystack.find(needle) == 0;
	}

	inline bool contains(const std::string& haystack, const std::string& needle) {
		return haystack.find(needle) != std::string::npos;
	}

	class Profile {
	public:
		explicit Profile(std::string name) : name_(name) {
			begin_ = std::chrono::steady_clock::now();
		}

		~Profile() {
			auto end = std::chrono::steady_clock::now();
			printf("%s: %lldus\n", name_.c_str(), (end - begin_).count() / 1'000);
		}

	private:
		std::string name_;
		std::chrono::time_point<std::chrono::steady_clock> begin_;
	};
}

using namespace y2021;
