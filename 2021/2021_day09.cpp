#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <utility>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> in = load_strings("2021/2021_day09_input.txt");
		int64_t result = 0;

		int width = in[0].length();
		int height = in.size();

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int v = in[y][x];
				if ((x == 0 || v < in[y][x - 1]) &&
					(x == width - 1 || v < in[y][x + 1]) &&
					(y == 0 || v < in[y - 1][x]) &&
					(y == height - 1 || v < in[y + 1][x])) {
					result += in[y][x] + 1 - '0';
				}
			}
		}

		printf("part 1: %lld\n", result);
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day09_input.txt");
		int64_t result = 0;

		vector<int> basins;

		int width = in[0].length();
		int height = in.size();

		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				int v = in[y][x];
				if ((x == 0 || v < in[y][x - 1]) &&
					(x == width - 1 || v < in[y][x + 1]) &&
					(y == 0 || v < in[y - 1][x]) &&
					(y == height - 1 || v < in[y + 1][x])) {
					
					// Find size of basin.
					int basin_size = 0;
					vector<tuple<int, int>> q;
					vector<tuple<int, int>> done;
					q.push_back({ x, y });

					while (!q.empty()) {
						auto next = q.back();
						q.pop_back();

						if (std::count(done.begin(), done.end(), next))
							continue;

						++basin_size;
						auto [x_, y_] = next;
						done.push_back({ x_, y_ });

						if (x_ > 0 && in[y_][x_ - 1] > in[y_][x_] && in[y_][x_ - 1] != '9') {
							q.push_back({ x_ - 1, y_ });
						}
						if (x_ < width - 1 && in[y_][x_ + 1] > in[y_][x_] && in[y_][x_ + 1] != '9') {
							q.push_back({ x_ + 1, y_ });
						}
						if (y_ > 0 && in[y_ - 1][x_] > in[y_][x_] && in[y_ - 1][x_] != '9') {
							q.push_back({ x_, y_ - 1 });
						}
						if (y_ < height - 1 && in[y_ + 1][x_] > in[y_][x_] && in[y_ + 1][x_] != '9') {
							q.push_back({ x_,  y_ + 1 });
						}
					}

					basins.push_back(basin_size);
				}
			}
		}

		std::sort(basins.rbegin(), basins.rend());

		printf("part 2: %lld\n", basins[0] * basins[1] * basins[2]);
	}
}

namespace y2021 {
	void day09() {
		part1();
		part2();
	}
}
