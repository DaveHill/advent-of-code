#include <vector>
#include <string>
#include <array>
#include <cstdio>
#include <algorithm>
#include <set>

#include "2021_utils.h"

namespace {
	void part1() {
		auto input = load_ints("2021/2021_day07_input.txt", ',');
		auto [min, max] = std::minmax_element(input.begin(), input.end());
		int best = 99999999;
		for (int i = *min; i < *max; ++i) {
			int cost = 0;
			for (auto pos : input)
				cost += std::abs(pos - i);
			best = std::min(best, cost);
		}
		printf("part 1: %d\n", best);
	}

	void part2() {
		auto input = load_ints("2021/2021_day07_input.txt", ',');
		auto [min, max] = std::minmax_element(input.begin(), input.end());
		int best = 99999999;
		for (int i = *min; i < *max; ++i) {
			int cost = 0;
			for (auto pos : input) {
				auto dist = std::abs(pos - i);
				cost += (dist * (dist + 1)) / 2;
			}
			best = std::min(best, cost);
		}
		printf("part 2: %d\n", best);
	}
}

namespace y2021 {
	void day07() {
		part1();
		part2();
	}
}
