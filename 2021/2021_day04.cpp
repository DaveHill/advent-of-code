#include <vector>
#include <string>
#include <cstdio>
#include <set>

#include "2021_utils.h"

namespace {
	class Board {
	public:
		explicit Board(std::string* rows) {
			for (int r = 0; r < 5; ++r) {
				std::string& row = rows[r];
				for (int c = 0; c < 5; ++c) {
					int num = std::atoi(row.substr(c * 3, 2).c_str());
					numbers[r][c] = num;
					marked[r][c] = false;
				}
			}
		}

		void mark(int number) {
			for (int r = 0; r < 5; ++r) {
				for (int c = 0; c < 5; ++c) {
					if (numbers[r][c] == number) {
						marked[r][c] = true;
						return;
					}
				}
			}
		}

		bool check() const {
			// Check rows.
			for (int r = 0; r < 5; ++r) {
				if (marked[r][0] && marked[r][1] && marked[r][2] 
					&& marked[r][3] && marked[r][4])
					return true;
			}

			// Check cols.
			for (int c = 0; c < 5; ++c) {
				if (marked[0][c] && marked[1][c] && marked[2][c] &&
					marked[3][c] && marked[4][c])
					return true;
			}

			return false;
		}

		int score() const {
			int sum = 0;
			for (int r = 0; r < 5; ++r) {
				for (int c = 0; c < 5; ++c) {
					if (!marked[r][c])
						sum += numbers[r][c];
				}
			}
			return sum;
		}

	private:
		int numbers[5][5];
		bool marked[5][5];
	};

	void part1() {
		auto input = load_strings("2021/2021_day04_input.txt");
		auto calls = split(input[0], ",");

		std::vector<Board> boards;
		for (int i = 2; i < input.size(); i += 6) {
			boards.emplace_back(&input[i]);
		}

		int result = 0;
		for (auto& call_str : calls) {
			int call = std::atoi(call_str.c_str());
			for (auto& board : boards) {
				board.mark(call);
				if (board.check()) {
					result = call * board.score();
				}
			}

			if (result) break;
		}

		printf("part 1: %d\n", result);
	}

	void part2() {
		auto input = load_strings("2021/2021_day04_input.txt");
		auto calls = split(input[0], ",");

		std::vector<Board> boards;
		for (int i = 2; i < input.size(); i += 6) {
			boards.emplace_back(&input[i]);
		}

		int result = 0;
		for (auto& call_str : calls) {
			int call = std::atoi(call_str.c_str());
			for (auto& board : boards) {
				board.mark(call);

				if (board.check()) {
					bool all_complete = true;
					for (auto& board_inner : boards) {
						all_complete &= board_inner.check();
					}

					if (all_complete && !result) {
						result = call * board.score();
					}
				}
			}

			if (result) break;
		}

		printf("part 2: %d\n", result);
	}
}

namespace y2021 {
	void day04() {
		part1();
		part2();
	}
}
