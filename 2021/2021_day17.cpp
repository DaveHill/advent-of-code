#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <utility>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
    void part1() {
        vector<string> in = load_strings("2021/2021_day17_input.txt");       
        int64_t minx, maxx, miny, maxy;
        sscanf_s(in[0].c_str(), "target area: x=%lld\.\.%lld, y=%lld\.\.%lld", &minx, &maxx, &miny, &maxy);

        int64_t max_y_hit_target = 0;
        for (int64_t init_vx = 1; init_vx < 500; ++init_vx) {
            for (int64_t init_vy = -500; init_vy < 500; ++init_vy) {
                // Simulate.
                int64_t x = 0, y = 0;
                int64_t vx = init_vx;
                int64_t vy = init_vy;
                int64_t max_y_reached = 0;
                while (x <= maxx && y >= miny) {
                    x += vx;
                    y += vy;
                    max_y_reached = max(y, max_y_reached);
                    vx = max(vx - 1, 0ll);
                    vy -= 1;

                    if (x >= minx && x <= maxx && y >= miny && y <= maxy) {
                        max_y_hit_target = max(max_y_hit_target, max_y_reached);
                    }
                }
            }
        }

        printf("part 1: %lld\n", max_y_hit_target);
    }

    void part2() {
        vector<string> in = load_strings("2021/2021_day17_input.txt");
        int64_t minx, maxx, miny, maxy;
        sscanf_s(in[0].c_str(), "target area: x=%lld\.\.%lld, y=%lld\.\.%lld", &minx, &maxx, &miny, &maxy);

        int64_t win = 0;
        for (int64_t init_vx = 1; init_vx < 500; ++init_vx) {
            for (int64_t init_vy = -500; init_vy < 500; ++init_vy) {
                // Simulate.
                int64_t x = 0, y = 0;
                int64_t vx = init_vx;
                int64_t vy = init_vy;
                while (x <= maxx && y >= miny) {
                    x += vx;
                    y += vy;
                    vx = max(vx - 1, 0ll);
                    vy -= 1;

                    if (x >= minx && x <= maxx && y >= miny && y <= maxy) {
                        ++win;
                        break;
                    }
                }
            }
        }

        printf("part 2: %lld\n", win);
    }
}

namespace y2021 {
    void day17() {
        part1();
        part2();
    }
}
