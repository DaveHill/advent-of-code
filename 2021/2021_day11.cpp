#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void flash(vector<string>& in, int64_t& flash_count, int x, int y) {
		++flash_count;
		for (int yy = max(y - 1, 0); yy <= min(y + 1, (int)(in.size() - 1)); ++yy) {
			for (int xx = max(x - 1, 0); xx <= min(x + 1, (int)(in[0].length() - 1)); ++xx) {
				if (x == xx && y == yy) continue;

				if (++in[yy][xx] == '9' + 1) {
					flash(in, flash_count, xx, yy);
				}
			}
		}
	}

	void part1() {
		vector<string> in = load_strings("2021/2021_day11_input.txt");
		int64_t result = 0;

		for (int step = 0; step < 100; ++step) {
			for (int y = 0; y < in.size(); ++y) {
				for (int x = 0; x < in[0].length(); ++x) {
					++in[y][x];

					if (in[y][x] == '9' + 1) {
						flash(in, result, x, y);
					}
				}
			}

			for (int y = 0; y < in.size(); ++y) {
				for (int x = 0; x < in[0].length(); ++x) {
					if (in[y][x] > '9') in[y][x] = '0';
				}
			}
		}

		printf("part 1: %lld\n", result);
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day11_input.txt");
		int64_t result = 0;

		for (int step = 0; step < 1000; ++step) {
			for (int y = 0; y < in.size(); ++y) {
				for (int x = 0; x < in[0].length(); ++x) {
					++in[y][x];

					if (in[y][x] == '9' + 1) {
						flash(in, result, x, y);
					}
				}
			}

			bool all = true;
			for (int y = 0; y < in.size(); ++y) {
				for (int x = 0; x < in[0].length(); ++x) {
					if (in[y][x] > '9') {
						in[y][x] = '0';
					}
					else {
						all = false;
					}
				}
			}

			if (all) {
				printf("part 2: %d\n", step + 1);
				return;
			}
		}
	}
}

namespace y2021 {
	void day11() {
		part1();
		part2();
	}
}
