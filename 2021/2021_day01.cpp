#include <vector>
#include <string>
#include <cstdio>

#include "2021_utils.h"

namespace {
	void part1() {
		auto input = load_ints("2021/2021_day01_input.txt");
		int count = 0;
		for (int i = 1; i < input.size(); ++i) {
			if (input[i] > input[i - 1]) {
				++count;
			}
		}
		printf("part 1: %d\n", count);
	}

	void part2() {
		auto input = load_ints("2021/2021_day01_input.txt");
		int count = 0;
		for (int i = 3; i < input.size(); ++i) {
			if (input[i] > input[i - 3]) {
				++count;
			}
		}
		printf("part 2: %d\n", count);
	}
}

namespace y2021 {
	void day01() {
		part1();
		part2();
	}
}
