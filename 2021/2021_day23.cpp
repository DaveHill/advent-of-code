#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <array>
#include <utility>
#include <sstream>

#include "2021_utils.h"

using namespace ::std;

namespace {
	vector<int> room_rows{ 2, 3 };
	array<int, 4> room_cols{ 3, 5, 7, 9 };
	const int hallway_row = 1;
	array<int, 7> hallway_cols{ 1, 2, 4, 6, 8, 10, 11 };
	constexpr char names[4] = { 'A', 'B', 'C', 'D' };
	constexpr int costs[4] = { 1, 10, 100, 1000 };

	struct Amphipod {
		char name = 'X';
		int8_t r = -1, c = -1;
		bool moved_to_hallway = false;

		bool in_room() const {
			return find(room_rows.begin(), room_rows.end(), r) != room_rows.end();
		}

		bool in_hallway() const {
			return r == hallway_row;
		}

		bool in_position() const {
			return in_room() && (room_cols[name - 'A'] == c);
		}
	};

	struct State {
		vector<Amphipod> amphipods;
		int cost = 0;
	};

	struct SHash {
		size_t operator()(const State& s) const noexcept {
			size_t out = std::hash<int>{}(s.cost);

			static_assert(sizeof(Amphipod) == sizeof(int));

			for (const auto& a : s.amphipods) {
				out ^= std::hash<char>{}(reinterpret_cast<const int&>(a));
			}
			
			return out;
		}
	};

	bool operator==(const Amphipod& a1, const Amphipod& a2) {
		return memcmp(&a1, &a2, sizeof(decltype(a1))) == 0;
	}

	bool operator==(const State& s1, const State& s2) {
		return s1.cost == s2.cost && equal(s1.amphipods.begin(), s1.amphipods.end(), s2.amphipods.begin());
	}

	bool path_clear(const State& state, int start_r, int start_c, int end_r, int end_c) {
		// Check horizontal movement.
		for (int c = min(start_c, end_c); c <= max(start_c, end_c); ++c) {
			for (auto& a : state.amphipods) {
				// Ignore self.
				if (a.r == start_r && a.c == start_c) {
					continue;
				}

				if (a.r == hallway_row && a.c == c) {
					return false;
				}
			}
		}

		// Check vertical movement.
		int c = (start_r > end_r) ? start_c : end_c;
		for (int r = min(start_r, end_r); r <= max(start_r, end_r); ++r) {
			for (auto& a : state.amphipods) {
				if (a.r == start_r && a.c == start_c) {
					continue;
				}

				if (a.r == r && a.c == c) {
					return false;
				}
			}
		}

		return true;
	}

	bool room_ok(const State& state, char name, int r, int c) {
		// Can only move to correct room.
		if (room_cols[name - 'A'] != c) {
			return false;
		}

		for (auto& a : state.amphipods) {
			// Can't move to a room with a different type.
			if (a.c == c && a.name != name) {
				return false;
			}
		}

		// Can't move if a lower row is empty.
		for (int row = r + 1; row < room_rows.back(); ++row) {
			bool empty = true;
			for (auto& a : state.amphipods) {
				if (a.c == c && a.r == row) {
					empty = false;
				}
			}
			if (empty) return false;
		}

		return true;
	}

	int manhattan(int start_r, int start_c, int end_r, int end_c) {
		return abs(start_r - end_r) + abs(start_c - end_c);
	}

	void print(const State& state) {
		printf("cost: %d\n", state.cost);
		vector<string> lines = {
			"#############",
			"#...........#",
			"###.#.#.#.###",
			"  #.#.#.#.#  ",
			"  #########  ",
		};

		for (auto& a : state.amphipods) {
			lines[a.r][a.c] = a.name;
		}

		for (auto& line : lines) {
			printf("%s\n", line.c_str());
		}
		printf("\n\n");
	}

	bool stay_in_place(const State& state, int i) {
		const auto& a = state.amphipods;
		if (!a[i].in_room() || !a[i].in_position()) return false;
		//if (a[i].r == 3) return true;

		for (auto& am : a) {
			if (am.name != a[i].name && am.r > a[i].r && am.c == a[i].c)
				return false;
		}
		return true;
	}

	int score(const State& state) {
		int score = 0;
		for (int i = 0; i < state.amphipods.size(); ++i) {
			if (stay_in_place(state, i))
				score += 2;
			else if (state.amphipods[i].in_room())
				score--;
		}

		return score;
	}

	int find_cost(const State& state, int cost) {
		vector<State> next_states;
		next_states.push_back(state);

		int min_cost = std::numeric_limits<int>::max();
		unordered_map<State, int, SHash> cost_cache;

		uint64_t states_evaluated = 0;

		while (!next_states.empty()) {
			// todo: Best-first.
			auto next = max_element(next_states.begin(), next_states.end(),
				[](const auto& s1, const auto& s2) { return score(s1) < score(s2); });
			State state = *next;
			next_states.erase(next);

			// If we're already costing more than our best, continue.
			if (state.cost >= min_cost) {
				continue;
			}

			// If we've been here before at a lower cost, continue.
			if (auto it = cost_cache.find(state); it != cost_cache.end() && state.cost >= it->second) {
				continue;
			}
			++states_evaluated;
			cost_cache[state] = state.cost;

			//print(state);

			auto& a = state.amphipods;
			if (score(state) == state.amphipods.size() * 2) {
				min_cost = min(state.cost, min_cost);
				printf("min_cost = %d after %lld\n", min_cost, states_evaluated);
			}

			// Generate possible next states.
			for (int i = 0; i < a.size(); ++i) {
				if (stay_in_place(state, i)) {
					continue;
				}

				int cost_per_move = costs[a[i].name - 'A'];
				if (a[i].in_room()) {
					if (!a[i].moved_to_hallway) {
						// Find moves from room to hallway.
						for (int c : hallway_cols) {
							if (path_clear(state, a[i].r, a[i].c, hallway_row, c)) {
								State next_state = state;
								next_state.amphipods[i].r = hallway_row;
								next_state.amphipods[i].c = c;
								next_state.amphipods[i].moved_to_hallway = true;
								next_state.cost += cost_per_move * manhattan(a[i].r, a[i].c, hallway_row, c);
								next_states.push_back(next_state);
							}
						}
					}
				}
				else {
					// Find moves from hallway back to room.
					for (auto r : room_rows) {
						for (auto c : room_cols) {
							if (room_ok(state, a[i].name, r, c) &&
								path_clear(state, a[i].r, a[i].c, r, c)) {
								State next_state = state;
								next_state.amphipods[i].r = r;
								next_state.amphipods[i].c = c;
								next_state.cost += cost_per_move * manhattan(a[i].r, a[i].c, r, c);
								next_states.push_back(next_state);
							}
						}
					}
				}
			}
		}

		printf("%lld states evaluated\n", states_evaluated);

		return min_cost;
	}

	State parse(vector<string> input) {
		// Create first state.
		State initial;

		// Parse.
		for (int i = 0; i < (room_rows.size() * 4); ++i) {
			char find_name = names[i / room_rows.size()];

			bool found = false;
			for (int ri = 0; ri <= room_rows.size() && !found; ++ri) {
				int r = room_rows[ri];
				for (int c = 0; c < input[r].length() && !found; ++c) {
					if (input[r][c] == find_name) {
						initial.amphipods.emplace_back();
						initial.amphipods.back().name = find_name;
						initial.amphipods.back().r = r;
						initial.amphipods.back().c = c;
						input[r][c] = 'X';
						found = true;
					}
				}
			}
		}

		return initial;
	}

	void part1() {
		vector<string> input = load_strings("2021/2021_day23_input.txt");
		int cost = find_cost(parse(input), 0);

		printf("part 1: %d\n", cost);
	}

	void part2() {
		vector<string> input = load_strings("2021/2021_day23_input.txt");
		input.insert(input.begin() + 3, "  #D#C#B#A#");
		input.insert(input.begin() + 4, "  #D#B#A#C#");
		room_rows.assign({ 2, 3, 4, 5 });
		int cost = find_cost(parse(input), 0);

		printf("part 2: %lld\n", cost);
	}
}

namespace y2021 {
	void day23() {
		part1();
		part2();
	}
}
