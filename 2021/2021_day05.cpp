#include <vector>
#include <string>
#include <cstdio>
#include <algorithm>
#include <set>

#include "2021_utils.h"

namespace {
	struct Line {
		int x1, y1, x2, y2;
	};

	Line parse(const std::string& str) {
		Line line;
		sscanf_s(str.c_str(), "%d,%d -> %d,%d", &line.x1, &line.y1, &line.x2, &line.y2);
		return line;
	}

	void part1() {
		auto input = load_strings("2021/2021_day05_input.txt");
		std::vector<Line> data;
		std::transform(input.begin(), input.end(), std::back_inserter(data), parse);

		// Render the lines.
		constexpr int D = 1000;
		auto grid = new int[D][D]{ 0 };
		for (const Line& line : data) {
			if (line.x1 == line.x2) {
				for (int y = std::min(line.y1, line.y2); y <= std::max(line.y1, line.y2); ++y) {
					++grid[y][line.x1];
				}
			}

			if (line.y1 == line.y2) {
				for (int x = std::min(line.x1, line.x2); x <= std::max(line.x1, line.x2); ++x) {
					++grid[line.y1][x];
				}
			}
		}

		int count = 0;
		for (int y = 0; y < D; ++y)
			for (int x = 0; x < D; ++x)
				if (grid[y][x] >= 2)
					++count;

		delete[] grid;

		printf("part 1: %d\n", count);
	}

	void part2() {
		auto input = load_strings("2021/2021_day05_input.txt");
		std::vector<Line> data;
		std::transform(input.begin(), input.end(), std::back_inserter(data), parse);

		// Rasterise the lines.
		constexpr int D = 1000;
		auto grid = new int[D][D]{ 0 };
		for (Line& line : data) {
			int dx = line.x1 < line.x2 ? 1 : (line.x1 > line.x2 ? -1 : 0);
			int dy = line.y1 < line.y2 ? 1 : (line.y1 > line.y2 ? -1 : 0);

			int minx = std::min(line.x1, line.x2);
			int maxx = std::max(line.x1, line.x2);
			int miny = std::min(line.y1, line.y2);
			int maxy = std::max(line.y1, line.y2);

			for (int x = line.x1, y = line.y1;
				x >= minx && x <= maxx && y >= miny && y <= maxy;
				x += dx, y += dy) {
				++grid[y][x];
			}
		}

		int count = 0;
		for (int y = 0; y < D; ++y)
			for (int x = 0; x < D; ++x)
				if (grid[y][x] >= 2)
					++count;

		delete[] grid;

		printf("part 2: %d\n", count);
	}
}

namespace y2021 {
	void day05() {
		part1();
		part2();
	}
}
