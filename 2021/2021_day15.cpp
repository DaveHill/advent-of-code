#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
#include <utility>

#include "2021_utils.h"

using namespace ::std;

namespace {
	void part1() {
		vector<string> in = load_strings("2021/2021_day15_input.txt");

		struct State { int x, y, cost; };
		vector<State> stack;
		stack.push_back(State{0, 0, 0});

		map<pair<int, int>, int> cheapest_cost;
		while (!stack.empty()) {
			// Evaluate cheapest first.
			sort(stack.rbegin(), stack.rend(), [](auto& s1, auto& s2) { return s1.cost < s2.cost; });
			State state = stack.back();
			stack.pop_back();

			vector<State> next_states{
				{ state.x + 1, state.y },
				{ state.x - 1, state.y},
				{ state.x, state.y + 1},
				{ state.x, state.y - 1},
			};
			for (auto& next : next_states) {
				if (next.x >= 0 && next.x < in[0].size()
					&& next.y >= 0 && next.y < in.size()) {
					next.cost = state.cost + in[next.y][next.x] - '0';

					// Continue path if first or cheapest to this point.
					if (auto cc = cheapest_cost.find({ next.x, next.y });
						cc == cheapest_cost.end() || next.cost < cc->second) {
						stack.push_back(next);
						cheapest_cost[{next.x, next.y}] = next.cost;
					}
				}
			}
		}

		printf("part 1: %d\n", cheapest_cost.at({ in.size() - 1, in.size() - 1 }));
	}

	void part2() {
		vector<string> in = load_strings("2021/2021_day15_input.txt");

		struct State { int x, y, cost; };
		vector<State> stack;
		stack.push_back(State{ 0, 0, 0 });

		map<pair<int, int>, int> cheapest_cost;
		while (!stack.empty()) {
			// Evaluate cheapest first.
			sort(stack.rbegin(), stack.rend(), [](auto& s1, auto& s2) { return s1.cost < s2.cost; });
			State state = stack.back();
			stack.pop_back();

			vector<State> next_states{
				{ state.x + 1, state.y },
				{ state.x - 1, state.y},
				{ state.x, state.y + 1},
				{ state.x, state.y - 1},
			};
			for (auto& next : next_states) {
				if (next.x >= 0 && next.x < in[0].size() * 5
					&& next.y >= 0 && next.y < in.size() * 5) {

					// Calculate cost of next move.
					int next_cost = in[next.y % in.size()][next.x % in.size()] - '0';
					next_cost += next.x / in.size() + next.y / in.size();
					if (next_cost > 9) {
						next_cost = next_cost % 9;
					}

					next.cost = state.cost + next_cost;

					// Continue path if first or cheapest to this point.
					if (auto cc = cheapest_cost.find({ next.x, next.y });
						cc == cheapest_cost.end() || next.cost < cc->second) {
						stack.push_back(next);
						cheapest_cost[{next.x, next.y}] = next.cost;
					}
				}
			}
		}

		printf("part 2: %d\n", cheapest_cost.at({ in.size() * 5 - 1, in.size() * 5 - 1 }));
	}
}

namespace y2021 {
	void day15() {
		part1();
		part2();
	}
}
