#include <algorithm>
#include <bitset>
#include <cstdio>
#include <string>
#include <vector>
#include <map>
#include <cassert>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <sstream>
#include <set>
#include <numeric>

#include "2021_utils.h"

using namespace ::std;

namespace {
	struct V3 { int x, y, z; };

	struct M33 {
		int m00, m01, m02,
			m10, m11, m12,
			m20, m21, m22;
	};

	V3 transform(M33 m, V3 v) {
		return {
			m.m00 * v.x + m.m01 * v.y + m.m02 * v.z,
			m.m10 * v.x + m.m11 * v.y + m.m12 * v.z,
			m.m20 * v.x + m.m21 * v.y + m.m22 * v.z
		};
	}

	V3 operator+(const V3& v0, const V3& v1) {
		return { v1.x + v0.x, v1.y + v0.y, v1.z + v0.z };
	}

	V3 operator-(const V3& v0, const V3& v1) {
		return { v1.x - v0.x, v1.y - v0.y, v1.z - v0.z };
	}

	struct VHash {
		size_t operator()(const V3& v) const noexcept {
			return std::hash<int>{}(v.x * v.y * 1024 * v.z * 1024 * 1024);
		}
	};

	bool operator==(const V3& v0, const V3& v1) {
		return v0.x == v1.x && v0.y == v1.y && v0.z == v1.z;
	}

	// Left-handed, row major (shouldn't matter as long as consistent).
	const vector<M33> rotations{
		// X-right
		{ 1, 0, 0,      0, 1, 0,     0, 0, 1 },
		{ 1, 0, 0,      0, 0, 1,     0, -1, 0 },
		{ 1, 0, 0,      0, -1, 0,    0, 0, -1 },
		{ 1, 0, 0,      0, 0, -1,    0, 1, 0 },

		// X-left (flip X and Y cols from above)
		{ -1, 0, 0,      0, -1, 0,   0, 0, 1 },
		{ -1, 0, 0,      0, 0, -1,   0, -1, 0 },
		{ -1, 0, 0,      0, 1, 0,    0, 0, -1 },
		{ -1, 0, 0,      0, 0, 1,    0, 1, 0 },

		// X-up
		{ 0, 1, 0,     -1, 0, 0,     0, 0, 1 },
		{ 0, 1, 0,     0, 0, 1,      1, 0, 0 },
		{ 0, 1, 0,     1, 0, 0,      0, 0, -1 },
		{ 0, 1, 0,     0, 0, -1,     -1, 0, 0 },

		// X-down (flip X and Y from above)
		{ 0, -1, 0,     1, 0, 0,     0, 0, 1 },
		{ 0, -1, 0,     0, 0, -1,    1, 0, 0 },
		{ 0, -1, 0,     -1, 0, 0,    0, 0, -1 },
		{ 0, -1, 0,     0, 0, 1,    -1, 0, 0 },

		// X-back
		{ 0, 0, 1,      1, 0, 0,     0, 1, 0 },
		{ 0, 0, 1,      0, 1, 0,    -1, 0, 0 },
		{ 0, 0, 1,      -1, 0, 0,    0, -1, 0 },
		{ 0, 0, 1,      0, -1, 0,    1, 0, 0 },

		// X-front (flip X and Y)
		{ 0, 0, -1,     -1, 0, 0,    0, 1, 0 },
		{ 0, 0, -1,      0, -1, 0,  -1, 0, 0 },
		{ 0, 0, -1,      1, 0, 0,    0, -1, 0 },
		{ 0, 0, -1,      0, 1, 0,    1, 0, 0 },
	};

	struct Scanner {
		vector<V3> beacons;
		bool matched = false;
		V3 offset_from_scanner0;
	};

	vector<Scanner> get_localized_scanners() {
		vector<string> in = load_strings("2021/2021_day19_input.txt");
		int scanner_id = 0;
		vector<Scanner> scanners;
		for (auto& line : in) {
			if (sscanf_s(line.c_str(), "--- scanner %d ---", &scanner_id) == 1) {
				scanners.emplace_back();
				continue;
			}

			V3 v;
			if (sscanf_s(line.c_str(), "%d,%d,%d", &v.x, &v.y, &v.z) == 3) {
				scanners[scanner_id].beacons.push_back(v);
			}
		}

		Profile p("get_localized_scanners");

		// Initialize.
		const int min_matches = 12;
		int unique_beacons = scanners[0].beacons.size();
		int completed_scanners = 1;
		scanners[0].matched = true;

		vector<int> newly_matched{ 0 };

		// For a matched scanner A...
		while (!newly_matched.empty()) {
			Scanner& scanner_a = scanners[newly_matched.back()];
			newly_matched.pop_back();
			if (!scanner_a.matched) {
				continue;
			}

			// Try to match an unmatched scanner B to it...
			for (int b = 0; b < scanners.size(); ++b) {
				Scanner& scanner_b = scanners[b];
				if (scanner_b.matched) {
					continue;
				}

				// For every potential orientation of B...
				for (const M33& rotation : rotations) {
					// Transform B's positions.
					vector<V3> transformed_beacons_b(scanner_b.beacons.size());
					std::transform(scanner_b.beacons.begin(), scanner_b.beacons.end(), transformed_beacons_b.begin(),
						[&](V3& b) { return transform(rotation, b);  });

					// Try to match the beacons, count offsets.
					unordered_map<V3, int, VHash> offset_counts;
					int max_match_count = 0;
					for (const V3& ba : scanner_a.beacons) {
						for (const V3& bb : transformed_beacons_b) {
							++offset_counts[bb - ba];
							max_match_count = max(max_match_count, offset_counts[bb - ba]);
						}
					}

					// If we got the required offsets...
					for (auto [offset, match_count] : offset_counts) {
						if (match_count >= min_matches) {
							// Mark scanner B as matched.
							scanner_b.matched = true;
							scanner_b.offset_from_scanner0 = offset;
							++completed_scanners;
							unique_beacons += scanner_b.beacons.size() - match_count;
							newly_matched.push_back(b);

							// Store B's beacon positions in the canonical coordinate space.
							for (int i = 0; i < transformed_beacons_b.size(); ++i) {
								scanner_b.beacons[i] = transformed_beacons_b[i] + offset;
							}

							break;
						}
					}

					if (scanner_b.matched)
						break;
				}
			}
		}
		assert(completed_scanners == scanners.size());

		return scanners;
	}

	void part1() {
		auto scanners = get_localized_scanners();

		// Count unique.
		unordered_set<V3, VHash> all_beacons;
		for (auto& scanner : scanners) {
			for (auto& beacon : scanner.beacons) {
				all_beacons.insert(beacon);
			}
		}

		printf("part 1: %lld\n", all_beacons.size());
	}

	void part2() {
		auto scanners = get_localized_scanners();

		int max_manhattan = 0;
		for (auto& scanner_a : scanners) {
			for (auto& scanner_b : scanners) {
				auto a = scanner_a.offset_from_scanner0;
				auto b = scanner_b.offset_from_scanner0;
				int manhattan = abs(a.x - b.x) + abs(a.y - b.y) + abs(a.z - b.z);
				max_manhattan = max(manhattan, max_manhattan);
			}
		}

		printf("part 2: %d\n", max_manhattan);
	}
}

namespace y2021 {
	void day19() {
		part1();
		part2();
	}
}
