#include <numeric>

#include "pch.h"

namespace {

void part1() {
  auto input = load_strings("day04_input.txt");
  int64_t result = 0;

  for (string card : input) {
    auto parts = split(split(card, ":")[1], "|");
    auto winning = split(parts[0], " ");
    auto my = split(parts[1], " ");

    int score = 0;
    for (auto& num : my) {
      if (ccontains(winning, num)) {
        if (score == 0) {
          score = 1;
        } else {
          score *= 2;
        }
      }
    }
    result += score;
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day04_input.txt");
  int64_t result = 0;

  vector<int64_t> num_cards(input.size(), 1);
  for (int64_t card_id = 0; card_id < input.size(); ++card_id) {
    auto& card = input[card_id];
    auto parts = split(split(card, ":")[1], "|");
    auto winning = split(parts[0], " ");
    auto my = split(parts[1], " ");

    int matched = 0;
    for (auto& num : my) {
      if (ccontains(winning, num)) {
        ++matched;
      }
    }

    for (int i = 0; i < matched; ++i) {
      num_cards[card_id + i + 1] += num_cards[card_id];
    }
  }

  result = std::accumulate(num_cards.begin(), num_cards.end(), 0);

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day04() {
  part1();
  part2();
}
