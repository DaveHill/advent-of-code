#include <numeric>

#include "pch.h"

namespace {

void part1() {
  auto input = load_strings("day05_input.txt");
  i64 result = 0;

  const vector<string> seeds_str = split(split(input[0], ":")[1], " ");
  set<i64> seeds;
  for (auto& str : seeds_str) {
    seeds.insert(stoull(str));
  }

  set<i64> seeds_todo = seeds;
  set<i64> seeds_next;
  seeds.clear();

  for (string& line : input) {
    auto line_split = split(line, " ");
    if (line_split.size() == 3) {
      i64 dst = stoull(line_split[0]);
      i64 src = stoull(line_split[1]);
      i64 len = stoull(line_split[2]);

      // Transform each seed.
      for (auto seed : seeds) {
        if (seed >= src && seed <= src + len) {
          seeds_next.insert(seed - src + dst);
          seeds_todo.erase(seed);
        }
      }
    }

    if (line.contains("-")) {
      seeds = seeds_next;
      seeds.insert(seeds_todo.begin(), seeds_todo.end());
      seeds_todo = seeds;
      seeds_next.clear();
    }
  }
  seeds = seeds_next;
  seeds.insert(seeds_todo.begin(), seeds_todo.end());

  cout << std::format("part 1 result = {}\n",
                      *min_element(seeds.begin(), seeds.end()));
}

void part2() {
  auto input = load_strings("day05_input.txt");
  i64 result = 0;

  const vector<string> seeds_str = split(split(input[0], ":")[1], " ");
  set<pair<i64, i64>> seeds;
  for (size_t i = 0; i < seeds_str.size(); i += 2) {
    i64 start = stoull(seeds_str[i]);
    i64 len = stoull(seeds_str[i + 1]);
    seeds.emplace(start, start + len - 1);
  }

  set<pair<i64, i64>> seeds_todo = seeds;
  set<pair<i64, i64>> seeds_next;
  seeds.clear();

  for (string& line : input) {
    auto line_split = split(line, " ");
    if (line_split.size() == 3) {
      i64 dst = stoull(line_split[0]);
      i64 src = stoull(line_split[1]);
      i64 len = stoull(line_split[2]);

      // Transform each seed.
      for (auto seed : seeds) {
        i64 range_lo = seed.first;
        i64 range_hi = seed.second;

        // Part below input range.
        if (range_lo < src) {
          seeds_next.emplace(range_lo, src - 1);
          seeds_todo.erase(seed);
          seeds_todo.emplace();
        }
        // Part above input range.
        if (range_hi > src + len) {
          seeds_next.emplace(src + len + 1, range_hi);
        }
        // Intersecting range.
        if (!(range_hi < src || range_lo > src + len)) {
          // TODO: Re-map.
          seeds_next.emplace(max(range_lo, src), min(range_hi, src + len));
        }
      }
    }

    if (line.contains("-")) {
      seeds = seeds_next;
      seeds.insert(seeds_todo.begin(), seeds_todo.end());
      seeds_todo = seeds;
      seeds_next.clear();
    }
  }
  seeds = seeds_next;
  seeds.insert(seeds_todo.begin(), seeds_todo.end());

  cout << std::format("part 2 result = {}\n",
                      min_element(seeds.begin(), seeds.end())->first);
}

}  // namespace

void day05() {
  part1();
  part2();
}
