#include "pch.h"

namespace {

void part1() {
  auto input = load_strings("day01_input.txt");
  int64_t result = 0;
  for (auto v : input) {
    vector<int> digits;
    for (auto c : v) {
      if (c >= '0' && c <= '9') {
        digits.push_back(c - '0');
      }
    }
    result += digits.front() * 10 + digits.back();
  }
  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  vector<string> names{
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
  };

  auto input = load_strings("day01_input.txt");
  int64_t result = 0;
  for (auto v : input) {
    map<int, int> first_digits;
    map<int, int> last_digits;
    
    // Find words.
    for (int n = 0; n < names.size(); ++n) {
      auto& name = names[n];
      auto first = v.find(name);
      auto last = v.rfind(name);

      if (first != string::npos) {
        first_digits[first] = n;
      }
      if (last != string::npos) {
        last_digits[last] = n;
      }
    }

    // Find digits.
    for (int i = 0; i < 10; ++i) {
      auto first = v.find_first_of('0' + i);
      auto last = v.find_last_of('0' + i);
      if (first != string::npos) {
        first_digits[first] = i;
      }
      if (last != string::npos) {
        last_digits[last] = i;
      }
    }

    result += first_digits.begin()->second * 10 + last_digits.rbegin()->second;
  }
  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day01() {
  part1();
  part2();
}
