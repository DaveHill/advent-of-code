#include "pch.h"

namespace {

void part1() {
  auto input = load_strings("day03_input.txt");
  int64_t result = 0;

  for (int64_t line_idx = 0; line_idx < input.size(); ++line_idx) {
    const std::string& line = input[line_idx];
    for (int64_t i = 0; i < line.size(); ++i) {
      if (isdigit(line[i])) {
        int64_t end = line.find_first_not_of("0123456789", i);
        if (end == string::npos) end = line.size();

        // Search around for a symbol.
        int64_t min_y = max<int64_t>(0, line_idx - 1);
        int64_t max_y = min<int64_t>(input.size() - 1, line_idx + 1);
        int64_t min_x = max<int64_t>(0, i - 1);
        int64_t max_x = min<int64_t>(line.size() - 1, end);

        int64_t value = stoi(line.substr(i, end));

        bool found_symbol = false;
        for (int64_t y = min_y; y <= max_y; ++y) {
          for (int64_t x = min_x; x <= max_x; ++x) {
            if (input[y][x] != '.' && !isdigit(input[y][x])) {
              found_symbol = true;
            }
          }
        }

        if (found_symbol) {
          result += value;
        }

        i = end;
      }
    }
  }

  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day03_input.txt");
  int64_t result = 0;

  map<int64_t, vector<int>> gears;

  for (int64_t line_idx = 0; line_idx < input.size(); ++line_idx) {
    const std::string& line = input[line_idx];
    for (int64_t i = 0; i < line.size(); ++i) {
      if (isdigit(line[i])) {
        int64_t end = line.find_first_not_of("0123456789", i);
        if (end == string::npos) end = line.size();

        // Search around for a symbol.
        int64_t min_y = max<int64_t>(0, line_idx - 1);
        int64_t max_y = min<int64_t>(input.size() - 1, line_idx + 1);
        int64_t min_x = max<int64_t>(0, i - 1);
        int64_t max_x = min<int64_t>(line.size() - 1, end);

        int64_t value = stoi(line.substr(i, end));

        for (int64_t y = min_y; y <= max_y; ++y) {
          for (int64_t x = min_x; x <= max_x; ++x) {
            if (input[y][x] == '*') {
              gears[y * line.size() + x].push_back(value);
            }
          }
        }

        i = end;
      }
    }
  }

  for (auto& [idx, gear] : gears) {
    if (gear.size() == 2) {
      result += gear[0] * gear[1];
    }
  }

  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day03() {
  part1();
  part2();
}
