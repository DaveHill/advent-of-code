#include "pch.h"

void day01();
void day02();
void day03();
void day04();
void day05();
void day06();
void day06();
void day07();
void day08();
void day09();
void day10();
void day11();
void day12();
void day13();
void day14();
void day15();
void day16();
void day17();
void day18();
void day19();
void day20();
void day21();
void day22();
void day23();
void day24();
void day25();

int main() {
  auto days = std::vector{
      &day01,
      &day02,
      &day03,
      &day04,
      &day05,
  };

  bool all = false;
  size_t start_day = (all ? 1 : days.size());
  for (size_t day = start_day; day <= days.size(); ++day) {
    cout << format("-- day {} --\n", day);
    days[day - 1]();
  }
}
