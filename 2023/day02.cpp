#include "pch.h"

namespace {

void part1() {
  auto input = load_strings("day02_input.txt");
  map<string, int> available_cubes{{"red", 12}, {"green", 13}, {"blue", 14}};
  int64_t result = 0;
  for (int game_idx = 0; game_idx < input.size(); ++game_idx) {
    auto game_cubes = split(input[game_idx], ":")[1];
    vector<string> reveals = split(game_cubes, ";");
    bool ok = true;
    for (const auto& reveal : reveals) {
      vector<string> list_cubes = split(reveal, ",");
      for (const auto& cube_count : list_cubes) {
        vector<string> cube_count_v = split(cube_count, " ");
        int64_t count = stoi(cube_count_v[0]);
        std::string color = cube_count_v[1];
        if (available_cubes[color] < count) {
          ok = false;
          goto not_ok;
        }
      }
    }
    if (ok) {
      result += game_idx + 1;
    }
  not_ok:
    continue;
  }
  cout << std::format("part 1 result = {}\n", result);
}

void part2() {
  auto input = load_strings("day02_input.txt");
  map<string, int> available_cubes{{"red", 12}, {"green", 13}, {"blue", 14}};
  int64_t result = 0;
  for (int game_idx = 0; game_idx < input.size(); ++game_idx) {
    map<string, int64_t> min_cubes_needed;
    auto game_cubes = split(input[game_idx], ":")[1];
    vector<string> reveals = split(game_cubes, ";");
    bool ok = true;
    for (const auto& reveal : reveals) {
      vector<string> list_cubes = split(reveal, ",");
      for (const auto& cube_count : list_cubes) {
        vector<string> cube_count_v = split(cube_count, " ");
        int64_t count = stoi(cube_count_v[0]);
        std::string color = cube_count_v[1];
        min_cubes_needed[color] = max(min_cubes_needed[color], count);
      }
    }
    int64_t power = 1;
    for (auto& [color, count] : min_cubes_needed) {
      power *= count;
    }
    result += power;
  }
  cout << std::format("part 2 result = {}\n", result);
}

}  // namespace

void day02() {
  part1();
  part2();
}
